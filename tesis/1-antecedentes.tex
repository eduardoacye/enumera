\chapter{Antecedentes}
\label{chap:ant}

\section{Distribución universal}

En 1997 se publica en la revista de divulgación \emph{The Mathematical Intelligencer} un artículo de Kirchherr, Li y Vitányi~\cite{MiraculousUD} donde plantean que el razonamiento inductivo contempla hacer predicciones sobre el comportamiento futuro basado en observaciones previas y cómo este razonamiento es una herramienta fundamental en el planteamiento de hipótesis en el quehacer científico.

El problema central que describen es cómo determinar métodos apropiados para formular estas hipótesis, y posteriormente, cómo determinar qué hipótesis es adecuada cuando las observaciones tienen mas de una explicación.

A lo largo del artículo presentan una perspectiva algorítmica particular, transversal a conceptos de probabilidad, teoría de la información y computabilidad, introduciendo a los lectores lo que ellos llaman \emph{la milagrosa distribución universal}.

En la década de los sesenta, Ray Solomonoff sentó las bases del área de \emph{probabilidad algorítmica}~\cite{InductiveInf1,InductiveInf2}, donde se estudia la distribución conocida como distribución universal. Esta distribución describe la probabilidad de que una cadena de símbolos $x$ sea producida por algún programa computacional.

Sea $U$ una máquina universal, $p$ un programa y $|p|$ el tamaño del programa, se define la distribución universal $D_U$ como
\[ D_U(x) = \sum_{U(p)=x} 2^{-|p|} \]

De forma independiente, Andrey Kolmogorov desarrolló la llamada \emph{complejidad algorítmica} al estudiar la naturaleza de las secuencias aleatorias~\cite{KolmogorovOriginal}. La complejidad algorítmica de una cadena de símbolos $x$ es la longitud de un programa más corto, que al ser ejecutado produce $x$. De acuerdo con este enfoque, entre mayor complejidad tenga una cadena, más aleatoria es y entre menor complejidad, mayor su regularidad.

Considerando la notación utilizada en la definición de la distribución universal, se define la complejidad de Kolmogorov $K_U$ como
\[ K_U(x) = \min \left\{ |p| {}:{} U(p) = x \right\} \]

A pesar de que el énfasis de Solomonoff fuera la inferencia inductiva y el de Kolmogorov fueran las propiedades matemáticas de la complejidad, ambas nociones se relacionan en el sentido que los programas que influyen en mayor medida la probabilidad algorítmica son los programas que tienen menor complejidad algorítmica, tal como lo explica Solomonoff en la conferencia impartida en 2003 al ser galardonado con la medalla Kolmogorov~\cite{SolomonoffLecture}.

En el contexto de razonamiento inductivo, una observación es una cadena de símbolos, mientras que las predicciones de qué hipótesis explica una observación son programas que generan dichas cadenas. La probabilidad algorítmica permite estudiar qué tan probable es que esta cadena haya sido generada por un mecanismo computacional, mientras que la complejidad algorítmica permite estudiar qué tan aleatoria es la cadena. La suposición implícita en ambos planteamientos es que los procesos detrás las observaciones realizadas pueden ser descritos con mecanismos computacionales.

Tanto la probabilidad como la complejidad algorítmica son incomputables, es decir, no existe un método efectivo para calcular estas medidas. Sin embargo, se han realizado esfuerzos para aproximarlas, en particular utilizando algoritmos de compresión. En una reciente publicación de Zenil~\cite{ZenilRespuesta} se presenta un panorama de los distintos métodos, enfoques y esfuerzos dedicados a esta tarea.

El presente trabajo de tesis, así como los trabajos de Luis Benítez Lluis~\cite{lluis} y Victor Zamora Gutiérrez~\cite{victor} forman parte del proyecto de investigación de Vladimir Lemus Yáñez~\cite{vlad} bajo la asesoría de Francisco Hernández Quiroz y Héctor Zenil. El proyecto busca aproximar la distribución universal utilizando técnicas alternativas a la compresión, como el \emph{Coding Theorem Method}~\cite{CTMShortStrings,CTMFreqDist} y el \emph{Block Decomposition Method}~\cite{BDMIntro}.

En contraste con esfuerzos similares que utilizan modelos computacionales basados en máquinas de Turing~\cite{CTMShortStrings} o sistemas dinámicos discretos~\cite{GenerativeModels} como autómatas celulares o redes complejas, este proyecto propone el uso de un lenguaje de programación imperativo de alto nivel con una sintaxis y semántica simple.

Como primera aproximación a solucionar el problema en cuestión, se propone generar programas de forma sistemática y ejecutarlos para determinar su salida, de tal manera que al medir el tamaño de los programas generados se puede almacenar su aporte a la distribución universal.

El trabajo descrito en esta tesis y la de Benítez Lluis~\cite{lluis} aborda la primer parte de esta propuesta, conceptualizando la generación de programas como un problema de enumeración. La segunda parte de la propuesta se aborda en el trabajo de tesis de Zamora Gutiérrez~\cite{victor}, donde se describe la implementación de un intérprete de programas diseñado para suspender su ejecución en caso que se determine probable que el programa no se vaya a detener.

% Este trabajo consiste en el desarrollo de métodos computacionales para la enumeración exhaustiva de programas del lenguaje de alto nivel, con un énfasis en las técnicas empleadas en los algoritmos, la representación del lenguaje y la enumeración, así como sus propiedades cuantitativas y cualitativas.

\section{Lenguaje de programación IMP}
\label{sec:stx-sem}

El lenguaje de alto nivel utilizado en el proyecto de investigación es llamado IMP. Los programas de este lenguaje se construyen a partir de expresiones y estructuras presentes en una gran cantidad de lenguajes imperativos: condicionales, iteraciones, asignaciones y concatenación de programas.

Hay una amplia tradición de capturar la esencia de programas de alto nivel con estas mismas unidades básicas. Desde la década de los sesenta se han utilizado estos lenguajes para estudiar formalmente propiedades de programas~\cite{Hoare} y para estudiar conceptos fundamentales de diseño de lenguajes de programación~\cite{Tennent}, en particular para la especificación de la semántica~\cite{Plotkin}. Recientemente se han utilizado lenguajes como IMP como herramienta didáctica para el estudio de técnicas de verificación formal y asistentes de prueba~\cite{SF1,SF2}.

El lenguaje IMP se describe formalmente utilizando una gramática libre de contexto para su sintaxis y reglas de semántica operacional estructurada para su semántica. Las referencias~\cite{hopcroft:ullman}~y~\cite{Plotkin} pueden ser útiles para familiarizarse con estos formalismos. El apéndice \ref{ap:stxsem} contiene una descripción completa y condensada de la definición de IMP presente en esta sección.

Las categorías sintácticas principales del lenguaje son los números naturales \V{N}, las localidades de memoria \V{X}, las expresiones aritméticas \V{A}, las expresiones booleanas \V{B} y los programas \V{P}.

Para toda categoría sintáctica \V{C}, se denota $\alpha \in \V{C}$ si la cadena de símbolos $\alpha$ puede ser derivada a partir de \V{C}. En particular, se dice que una cadena de símbolos $\alpha$ es un programa sintácticamente válido de IMP cuando $\alpha\in\V{P}$.

La ejecución de programas IMP ocurre en el contexto de un estado de memoria denotado $\sigma$. La memoria es modelada como una función que asocia localidades a números naturales. La semántica del lenguaje es definida en términos de relaciones de transición de la forma
\[ \langle \alpha, \sigma \rangle \rightarrow x \]
donde $\alpha$ es una cadena de símbolos, $\sigma$ un estado de memoria y $x$ el resultado de evaluar operacionalmente la expresión $\alpha$ en el estado $\sigma$.

%\subsection*{Números naturales}

Los números naturales son uno de los tipos de datos fundamentales de IMP ya que son almacenados en la memoria, son utilizados para referirse a localidades de memoria y forman parte de los cálculos realizados mediante expresiones aritméticas. La categoría sintáctica de números naturales \V{N} tiene asociadas las producciones \eqref{eq:stx-nat}.
\begin{equation}
  \label{eq:stx-nat}
  \begin{aligned}
    \V{N} &\Prod \V{D_0}
           \Pror \V{D_1}\V{D_0}^+ \\
    \V{D_0} &\Prod \T{0} \Pror \V{D_1} \\
    \V{D_1} &\Prod \T{1} \Pror \T{2} \Pror \T{3} \Pror \T{4} \Pror \T{5}
             \Pror \T{6} \Pror \T{7} \Pror \T{8} \Pror \T{9}
  \end{aligned}
\end{equation}

De acuerdo a estas producciones, la estructura sintáctica de naturales \V{N} consiste de cadenas de dígitos del cero al nueve tal que las cadenas con más de un dígito no comienzan con \T{0}.

Sea $n \in \V{N}$, se denota como $\Num{\T{n}}$ al valor numérico de $n$ del conjunto de naturales~$\mathbb{N}$. La evaluación operacional de la cadena $n$ se especifica con la relación de transición~\eqref{eq:sem-nat}.
\begin{equation}
  \label{eq:sem-nat}
  \begin{aligned}
    \langle \T{n}, \sigma \rangle &\rightarrow_{\V{N}} \Num{\T{n}}
  \end{aligned}
\end{equation}

%\subsection*{Localidades de memoria}

Las localidades de memoria son expresiones que nos permiten referirnos a un valor almacenado en un estado de memoria. Sintácticamente se describen con la producción \eqref{eq:stx-loc}.
\begin{equation}
  \label{eq:stx-loc}
  \begin{aligned}
    \V{X} &\Prod \T{x_{\V{N}}}
  \end{aligned}
\end{equation}

Se denota $\sigma(\T{n_n})$ al natural en $\mathbb{N}$ almacenado en el estado de memoria $\sigma$ para la localidad \T{x_n}. La evaluación operacional de \T{x_n} modela la lectura de su valor almacenado en memoria y se especifica con la relación de transición \eqref{eq:sem-loc}.
\begin{equation}
  \label{eq:sem-loc}
  \begin{aligned}
    \langle \T{x_n}, \sigma \rangle &\rightarrow_{\V{X}} \sigma(\T{x_n})
  \end{aligned}
\end{equation}

%\subsection*{Expresiones aritméticas}

El cálculo de números naturales se realiza a partir de expresiones aritméticas~\V{A}, las cuáles se construyen a partir de naturales \V{N}, localidades \V{X}, o bien operaciones de suma, resta y multiplicación de otras expresiones aritméticas. La estructura sintáctica de estas expresiones se describe con las producciones \eqref{eq:stx-arith}.
\begin{equation}
  \label{eq:stx-arith}
  \begin{aligned}
    \V{A} &\Prod \V{N}
           \Pror \V{X}
           \Pror (\V{A}\Sep\T{+}\Sep\V{A})
           \Pror (\V{A}\Sep\T{-}\Sep\V{A})
           \Pror (\V{A}\Sep\T{\times}\Sep\V{A})
  \end{aligned}
\end{equation}

El resultado de evaluar operacionalmente una expresión aritmética es un número natural en $\mathbb{N}$. Cuando la cadena a ser evaluada es derivada de \V{N} o \V{X} se aplican las reglas de inferencia \eqref{eq:sem-arith-natloc}.
\begin{equation}
  \label{eq:sem-arith-natloc}
  \begin{aligned}
    \infer{
      \langle \T{n}, \sigma \rangle \rightarrow_{\V{A}} n
    }{
      \langle \T{n}, \sigma \rangle \rightarrow_{\V{N}} n
    }\qquad%
    \infer{
      \langle \T{x}_i, \sigma \rangle \rightarrow_{\V{A}} n
    }{
      \langle \T{x}_i, \sigma \rangle \rightarrow_{\V{X}} n
    }
  \end{aligned}
\end{equation}

Para las operaciones de suma, resta y multiplicación, se denota como \T{\oplus} al símbolo términal correspondiente a la operación y $\oplus_{\mathbb{N}}$ al operador binario correspondiente para la operación con naturales $\mathbb{N}$. Las tres evaluaciones se describen con la regla de inferencia \eqref{eq:sem-arith-binop}.
\begin{equation}
  \label{eq:sem-arith-binop}
  \infer{
    \langle (a_1\Sep{}\T{\oplus}\Sep{}a_2), \sigma \rangle \rightarrow_{\V{A}} n
  }{
    \langle a_1, \sigma \rangle \rightarrow_{\V{A}} n_1 &
    \langle a_2, \sigma \rangle \rightarrow_{\V{A}} n_2 &
    n_1 \oplus_{\mathbb{N}} n_2 = n
  }
\end{equation}

La suma y multiplicación de naturales se interpretan de forma usual, pero en el caso de la resta de dos cadenas \T{n_1} y \T{n_2} de \V{N}, cuando $\Num{\T{n_2}} > \Num{\T{n_1}}$ se considera que $\Num{\T{n_1}} -_{\mathbb{N}} \Num{\T{n_2}} = 0$.

%\subsection*{Expresiones booleanas}

A partir de valores de verdad \T{true} (verdadero) y \T{false} (falso), el lenguaje IMP permite construir expresiones booleanas \V{B} más complejas utilizando las operaciones de conjunción, disyunción y negación, así como comparaciones entre expresiones aritméticas.

Ya que las expresiones aritméticas se pueden conformar por valores almacenados en memoria, el resultado de una expresión booleana también puede depender del valor asociado a las localidades de memoria.

La estructura sintáctica de las expresiones booleanas \V{B} se describe con las producciones \eqref{eq:stx-bool}.
\begin{equation}
  \label{eq:stx-bool}
  \begin{aligned}
    \V{B} &\Prod \T{true}
           \Pror \T{false}
           \Pror (\V{A}\Sep\T{=}\Sep\V{A})
           \Pror (\V{A}\Sep\T{<}\Sep\V{A})
           \Pror \neg\Sep\V{B}
           \Pror (\V{B}\Sep\T{\lor}\Sep\V{B})
           \Pror (\V{B}\Sep\T{\land}\Sep\V{B})
  \end{aligned}
\end{equation}

El resultado de evaluar operacionalmente una expresión booleana es un valor de verdad \T{true} o \T{false}. Estas dos expresiones son evaluadas a si mismas como se describe en las reglas de inferencia \eqref{eq:sem-bool-tv}.
\begin{equation}
  \label{eq:sem-bool-tv}
  \begin{aligned}
    \langle \T{true}, \sigma \rangle \rightarrow_{\V{B}} \T{true} \qquad
    \langle \T{false}, \sigma \rangle \rightarrow_{\V{B}} \T{false}
  \end{aligned}
\end{equation}

Dos expresiones aritméticas pueden ser comparadas para calcular si corresponden al mismo valor numérico con la operación de \emph{igual que} de acuerdo a las reglas de inferencia \eqref{eq:sem-bool-eq}.
\begin{equation}
  \label{eq:sem-bool-eq}
  \begin{aligned}
    \infer{
      \langle (a_1\Sep{}\T{=}\Sep{}a_2), \sigma \rangle \rightarrow_{\V{B}} \T{true}
    }{
      \langle a_1, \sigma \rangle \rightarrow_{\V{A}} n_1 &
      \langle a_2, \sigma \rangle \rightarrow_{\V{A}} n_2 &
      n_1 = n_2
    } \\
    \infer{
      \langle (a_1\Sep{}\T{=}\Sep{}a_2), \sigma \rangle \rightarrow_{\V{B}} \T{false}
    }{
      \langle a_1, \sigma \rangle \rightarrow_{\V{A}} n_1 &
      \langle a_2, \sigma \rangle \rightarrow_{\V{A}} n_2 &
      n_1 \not= n_2
    }
  \end{aligned}
\end{equation}

De forma similar, se calcula si una expresión aritmética es numéricamente menor a otra con la operación de \emph{menor que} de acuerdo a las reglas de inferencia~\eqref{eq:sem-bool-lt}.
\begin{equation}
  \label{eq:sem-bool-lt}
  \begin{aligned}
    \infer{
      \langle (a_1\Sep{}\T{<}\Sep{}a_2), \sigma \rangle \rightarrow_{\V{B}} \T{true}
    }{
      \langle a_1, \sigma \rangle \rightarrow_{\V{A}} n_1 &
      \langle a_2, \sigma \rangle \rightarrow_{\V{A}} n_2 &
      n_1 < n_2
    } \\
    \infer{
      \langle (a_1\Sep{}\T{<}\Sep{}a_2), \sigma \rangle \rightarrow_{\V{B}} \T{false}
    }{
      \langle a_1, \sigma \rangle \rightarrow_{\V{A}} n_1 &
      \langle a_2, \sigma \rangle \rightarrow_{\V{A}} n_2 &
      n_1 \geq n_2
    }
  \end{aligned}
\end{equation}

La negación de una expresión booleana es operacionalmente evaluada al valor de verdad opuesto de acuerdo a las reglas de inferencia \eqref{eq:sem-bool-neg}.
\begin{equation}
  \label{eq:sem-bool-neg}
  \begin{aligned}
    \infer{
      \langle \T{\neg}\Sep{}b, \sigma \rangle \rightarrow_{\V{B}} \T{true}
    }{
      \langle b, \sigma \rangle \rightarrow_{\V{B}} \T{false}
    }\qquad%
    \infer{
      \langle \T{\neg}\Sep{}b, \sigma \rangle \rightarrow_{\V{B}} \T{false}
    }{
      \langle b, \sigma \rangle \rightarrow_{\V{B}} \T{true}
    }
  \end{aligned}
\end{equation}

Para las conjunciones y disyunciones se describe su evaluación operacional con la regla de inferencia \eqref{eq:sem-bool-binop} donde \T{\oplus} se refiere al símbolo terminal de la operación y $\oplus_{\mathbb{B}}$ se refiere a la operación correspondiente descrita en la tabla \ref{tbl:bool-binops}.
\begin{equation}
  \label{eq:sem-bool-binop}
  \begin{aligned}
    \infer{
      \langle (b_1\Sep{}\T{\oplus}\Sep{}b_2), \sigma \rangle \rightarrow_{\V{B}} v
    }{
      \langle b_1, \sigma \rangle \rightarrow_{\V{B}} v_1 &
      \langle b_2, \sigma \rangle \rightarrow_{\V{B}} v_2 &
      v_1 \oplus_{\mathbb{B}} v_2 = v
    }
  \end{aligned}
\end{equation}

\begin{table}[htb]
  \centering
  \begin{tabular}{@{}cccc@{}}
    \toprule
    $v_1$ & $v_2$ & $v_1 \lor v_2$ & $v_1 \land v_2$ \\
    \midrule
    \T{false}  & \T{false}  & \T{false} & \T{false} \\
    \T{false}  & \T{true}   & \T{true}  & \T{false} \\
    \T{true}   & \T{false}  & \T{true}  & \T{false} \\
    \T{true}   & \T{true}   & \T{true}  & \T{true} \\
    \bottomrule
  \end{tabular}
  \caption{Operaciones booleanas binarias.}
  \label{tbl:bool-binops}
\end{table}

%\subsection*{Programas}

Los programas del lenguaje IMP son expresiones que permiten modificar el estado de la memoria y controlar el flujo de ejecución. La estructura sintáctica de un programa \V{P} se describe con las producciones \eqref{eq:stx-prog}.
\begin{equation}
  \label{eq:stx-prog}
  \begin{aligned}
    \V{P} &\Prod \T{skip}
           \Pror \V{X}\Sep\TSet\Sep\V{A}
           \Pror (\T{while}\Sep\V{B}\Sep\T{do}\Sep\V{P})
           \Pror (\V{P}\Sep\TCon\Sep\V{P})
           \Pror (\T{if}\Sep\V{B}\Sep\T{then}\Sep\V{P}\Sep\T{then}\Sep\V{P})
  \end{aligned}
\end{equation}

El resultado de evaluar operacionalmente un programa es un estado de memoria, es decir, los programas IMP transforman un estado de memoria a otro.

El programa sintácticamente más simple es \T{skip}, su ejecución no realiza cambios en el estado de la memoria. Su evaluación operacional se describe con la relación de transición \eqref{eq:sem-prog-skip}.
\begin{equation}
  \label{eq:sem-prog-skip}
  \begin{aligned}
    \langle \T{skip}, \sigma \rangle \rightarrow_{\V{P}} \sigma
  \end{aligned}
\end{equation}

Las asignaciones de memoria consisten en cambiar un natural almacenado en memoria por otro. Ya que los estados de memoria son modelados como funciones, se denota como $\sigma[n/\T{x_i}]$ al estado de memoria que asocia \T{x_i} al natural $n$ y cualquier otra localidad al natural asociado en $\sigma$. Formalmente se define con la ecuación~\eqref{eq:setmem}.
\begin{equation}\label{eq:setmem}
  \sigma[n/\T{x}_i](\T{x}_j) = \begin{cases}
    n &i=j \\
    \sigma(\T{x}_j) &i\not=j
  \end{cases}
\end{equation}

La evaluación operacional de las asignaciones de memoria se describen con la regla de inferencia~\eqref{eq:sem-prog-set}.
\begin{equation}
  \label{eq:sem-prog-set}
  \begin{aligned}
    \infer{
      \langle \T{x}_i\Sep{}\TSet\Sep{}a, \sigma \rangle \rightarrow_{\V{P}} \sigma[n/\T{x}_i]
    }{
      \langle a, \sigma \rangle \rightarrow_{\V{A}} n
    }
  \end{aligned}
\end{equation}

Para efectuar una secuencia de transformaciones al estado de memoria, el lenguaje IMP contiene expresiones para concatenar programas cuya evaluación operacional consiste en la ejecución secuencial de un programa después de otro con la regla de inferencia \eqref{eq:sem-prog-conc}.
\begin{equation}
  \label{eq:sem-prog-conc}
  \begin{aligned}
    \infer{
      \langle (p_1\Sep{}\TCon\Sep{}p_2), \sigma \rangle \rightarrow_{\V{P}} \sigma_2
    }{
      \langle p_1, \sigma \rangle \rightarrow_{\V{P}} \sigma_1 &
      \langle p_2, \sigma_1 \rangle \rightarrow_{\V{P}} \sigma_2
    }
  \end{aligned}
\end{equation}

Al igual que otros lenguajes de programación imperativos, IMP utiliza expresiones \T{while} para describir la repetida ejecución de un programa mientras una expresión booleana sea evaluada a \T{true}.
Para definir la evaluación operacional de estas expresiones se considera primero la regla de inferencia \eqref{eq:sem-prog-whilef} donde la expresión booleana asociada tiene valor \T{false}.
\begin{equation}
  \label{eq:sem-prog-whilef}
  \begin{aligned}
    \infer{
      \langle (\T{while}\Sep{}b\Sep{}\T{do}\Sep{}p), \sigma \rangle \rightarrow_{\V{P}} \sigma
    }{
      \langle b, \sigma \rangle \rightarrow_{\V{B}} \T{false}
    }
  \end{aligned}
\end{equation}

Cuando la expresión booleana es evaluada a \T{true}, se evalúa el cuerpo de la iteración para obtener un nuevo estado de memoria $\sigma_1$. Posteriormente se evalúa la expresión \T{while} con $\sigma_1$ para obtener el estado de memoria final $\sigma_2$, como se muestra en la regla de inferencia \eqref{eq:sem-prog-whilet}.
\begin{equation}
  \label{eq:sem-prog-whilet}
  \begin{aligned}
    \infer{
      \langle (\T{while}\Sep{}b\Sep{}\T{do}\Sep{}p), \sigma \rangle \rightarrow_{\V{P}} \sigma_2
    }{
      \langle b, \sigma \rangle \rightarrow_{\V{B}} \T{true} &
      \langle p, \sigma \rangle \rightarrow_{\V{P}} \sigma_1 &
      \langle (\T{while}\Sep{}b\Sep{}\T{do}\Sep{}p), \sigma_1 \rangle \rightarrow_{\V{P}} \sigma_2
    }
  \end{aligned}
\end{equation}

El último tipo de programa corresponde a las estructuras de control condicionales \T{if} compuestas de una expresión booleana y dos programas, el primero a ser ejecutado cuando la expresión booleana es evaluada a \T{true} y el segundo cuando es evaluada a \T{false} como se muestra en las reglas de inferencia \eqref{eq:sem-prog-if}.
\begin{equation}
  \label{eq:sem-prog-if}
  \begin{aligned}
    \infer{
      \langle (\T{if}\Sep{}b\Sep{}\T{then}\Sep{}p_1\Sep{}\T{else}\Sep{}p_2), \sigma \rangle \rightarrow_{\V{P}} \sigma'
    }{
      \langle b, \sigma \rangle \rightarrow_{\V{B}} \T{true} &
      \langle p_1, \sigma \rangle \rightarrow_{\V{P}} \sigma'
    }\qquad%
    \infer{
      \langle (\T{if}\Sep{}b\Sep{}\T{then}\Sep{}p_1\Sep{}\T{else}\Sep{}p_2), \sigma \rangle \rightarrow_{\V{P}} \sigma'
    }{
      \langle b, \sigma \rangle \rightarrow_{\V{B}} \T{false} &
      \langle p_2, \sigma \rangle \rightarrow_{\V{P}} \sigma'
    }
  \end{aligned}
\end{equation}

Para determinar cuál es la \emph{salida} de un programa se considera una función de interpretación de memoria $f$ la cual a partir de un estado cualquiera $\sigma$ calcula una cadena binaria $\alpha$. También se considera un estado de memoria vacío $\sigma_0$ el cual asocia a todas las localidades con el natural cero. Formalmente, para cualquier programa $p$, si $\langle p, \sigma_0 \rangle \rightarrow_{\V{P}} \sigma$ y $f(\sigma)=\alpha$, entonces $\alpha$ es la salida del programa $p$.

El trabajo de tesis~\cite{victor} presenta varias alternativas para la función de interpretación de memoria.

\section{Enumeraciones}

El concepto de \emph{enumeración} es utilizado para referirse a distintos procesos u objetos dependiendo del contexto. Desde la perspectiva de teoría de conjuntos, una enumeración es una relación biyectiva entre un conjunto de elementos y los números naturales.

A pesar de proveer una definición clara y precisa, la perspectiva de teoría de conjuntos dice poco sobre cómo trabajar con enumeraciones desde un enfoque algorítmico: ¿Cómo se representa una enumeración en la computadora? ¿Qué procesos computacionales pueden manipular estas representaciones? ¿Qué podemos aprender de la enumeración a partir de métodos que la producen?.

En las áreas de computabilidad y complejidad computacional~\cite{Arora,Cooper,Sipser}, el término enumeración es asociado a una amplia variedad de formalismos, mientras que el \emph{acto} de enumerar se utiliza en estos contextos para describir el proceso mediante el cual se recorre sobre todos los elementos de un conjunto de forma ordenada hasta encontrar uno que satisface alguna propiedad deseada.

En este trabajo se exploran las enumeraciones desde una perspectiva similar al área de combinatoria. Donald Knuth menciona en~\cite{KnuthAOCP4} que algunos autores se refieren al proceso de recorrer todas las posibilidades de algún universo combinatorio como ``enumerar'' o ``listar'' todas las posibilidades, sin embargo se opone a esta terminología argumentando que enumerar es usualmente asociada al acto de \emph{contar} las posibilidades y que listar implica imprimir o almacenar todas las posibilidades.

Kreher y Stinson plantean una distinción similar en~\cite{CompAlgosKreherStinson} y al igual que Knuth proponen como alternativa el término de \emph{generación} de objetos combinatorios, complementando este concepto con el de \emph{visitarlos}, es decir, procesar un objeto sin tener que generarlos todos.

En el trabajo preliminar titulado ``Combinatorial Generation''~\cite{CombGenRuskey}, Frank Ruskey plantea la exploración de cuatro diferentes problemas de enumeración:
\begin{enumerate}
\item Generar objetos a partir de un primer elemento y un operador sucesor.
\item Seleccionar de forma aleatoria un elemento de un conjunto de posibilidades.
\item Calcular la posición ocupada por un objeto en un ordenamiento.
\item Calcular el objeto que ocupa una posición en un ordenamiento.
\end{enumerate}

Los últimos dos problemas corresponden a procesos inversos conocidos como \emph{ranking} y \emph{unranking} respectivamente. El \emph{rank} (o rango) de un objeto es la posición que ocupa, al contar las posiciones a partir del cero, se puede interpretar también como la cantidad de objetos que lo preceden en el ordenamiento. En este trabajo se utilizan operaciones $\rank$ y $\unrank$ como los mecanismos principales para trabajar con enumeraciones.

El enfoque de los algoritmos descritos en~\cite{KnuthAOCP4,CompAlgosKreherStinson,CombGenRuskey} consiste en partir de un universo finito de objetos combinatorios elementales como permutaciones, combinaciones, particiones, tuplas o árboles, para luego describir algoritmos que producen distintos ordenamientos, analizar sus diferencias y presentar las ventajas o desventajas dependiendo del contexto.

Las ideas presentes en estos algoritmos de enumeraciones finitas influyeron los métodos para las enumeraciones descritas en este trabajo, las cuáles son en su mayoría enumeraciones de una infinidad de objetos cuyas estructuras son similares a árboles de sintaxis.

La generalización de los algoritmos de enumeración de IMP en el capítulo \ref{chap:comb} está inspirada en técnicas para reconocimiento sintáctico, en particular el conceptualizar las enumeraciones como objetos a ser combinados se basa en la técnica de combinadores de parsers~\cite{Wadler,Hutton} comúnmente empleada en programación funcional.

% El primero consiste en generar objetos combinatorios en orden a partir de un primer elemento y un operador sucesor. Otro consiste en seleccionar de forma aleatoria un elemento entre todas las posibilidades. Finalmente, los últimos dos problemas corresponden a dos procesos complementarios, el de \emph{ranking} y \emph{unranking}, los cuáles se abordan en este trabajo como los mecanismos principales para trabajar con enumeraciones.

%A partir de un ordenamiento de objetos, el \emph{rank} (o rango) de un objeto es la posición que ocupa en el ordenamiento. Al contar a partir del cero, esta posición se puede interpretar como la cantidad de objetos que preceden al objeto en el ordenamiento. El proceso que describe cómo encontrar el rank de un objeto es llamado \emph{ranking} y el proceso inverso que permite encontrar el objeto que ocupa cierta posición, es llamado \emph{unranking}.

%Frank Ruskey menciona que uno de los usos principales de los algoritmos de ranking es como funciones hash perfectas, mientras que los de unranking pueden ser utilizados para implementar procedimientos paralelos para generar objetos combinatorios cuando se incorpora el operador sucesor. Finalmente menciona que en general, el ranking y el unranking solo son posibles con algunos objetos combinatorios elementales \cite{CombGenRuskey}.
