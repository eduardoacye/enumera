;;; orglpx-ppc-lisp.el --- Pretty print LaTeX + Lisp -*- lexical-binding: t -*-

(font-lock-add-keywords
 'lisp-mode
 `(("(\\(defun\\*\\_>\\)[ \t']*\\(\\(?:\\sw\\|\\s_\\|\\\\.\\)+\\)"
    (1 font-lock-keyword-face)
    (2 font-lock-function-name-face))
   ("'\\(\\(?:\\sw\\|\\s_\\|\\\\.\\)+\\)"
    (1 font-lock-constant-face))))

(defvar lisp-macro-names
  '("do-all-symbols" "lambda" "incf" "loop-finish"
    "call-method" "call-next-method" "pushnew" "define-symbol-macro" "defclass"
    "defun" "multiple-value-bind" "nth-value" "defsetf"
    "untrace" "dolist" "with-hash-table-iterator" "assert"
    "with-compilation-unit" "defmethod" "pop" "setf"
    "defconstant" "define-method-combination" "decf" "typecase"
    "prog" "loop" "with-input-from-string" "time"
    "step" "trace" "with-open-stream" "ccase"
    "pprint-logical-block" "push" "formatter" "with-slots"
    "psetq" "handler-bind" "restart-case" "with-package-iterator"
    "defstruct" "do-symbols" "prog*" "with-accessors"
    "prog2" "do*" "cond" "defvar" "type" "ftype"
    "do-external-symbols" "check-type" "defgeneric" "declaim"
    "deftype" "defmacro" "ecase" "print-unreadable-object"
    "defparameter" "defpackage" "multiple-value-list" "handler-case"
    "unless" "remf" "rotatef" "pprint-exit-if-list-exhausted"
    "restart-bind" "or" "ctypecase" "pprint-pop"
    "with-condition-restarts" "case" "define-compiler-macro" "etypecase"
    "prog1" "multiple-value-setq" "with-open-file" "when"
    "with-standard-io-syntax" "do" "in-package" "ignore-errors"
    "define-modify-macro" "with-output-to-string" "with-simple-restart" "define-condition"
    "and" "return" "define-setf-expander" "shiftf"
    "psetf" "destructuring-bind" "dotimes"))

(defvar lisp-function-names
  '("simple-array" "integer" ">" "char>=" "<=" "list*" "1+" "char>"
    "string>" "*" "char=" "char<=" "<" "/" "string="
    "/=" "1-" "string/=" ">=" "char<" "-" "string>=" "="
    "string<" "char/=" "+" "string<="
    "merge" "rationalize" "break"
    "rest" "make-condition" "byte-size" "upgraded-complex-part-type"
    "use-value" "no-next-method" "cdadar"
    "ceiling" "adjustable-array-p" "mask-field"
    "character" "both-case-p" "ed" "logior"
    "slot-missing" "byte" "seventh"
    "logical-pathname" "make-random-state" "lisp-implementation-type" "integerp"
    "tenth" "stringp" "file-string-length" "alphanumericp"
    "quote" "rationalp" "lognand" "nsubst-if"
    "union" "fill" "hash-table-size"
    "type-error-datum" "make-load-form" "deposit-field" "logandc2"
    "hash-table-rehash-size" "cdaadr" "char-downcase"
    "float-digits" "count" "functionp" "list-length"
    "apply" "read-char-no-hang" "asin" "make-array"
    "member-if-not" "bit-orc2" "parse-namestring" "pathnamep"
    "find-if-not" "notevery" "member-if" "arithmetic-error-operation"
    "set-syntax-from-char" "read-line" "special-operator-p" "nstring-upcase"
    "finish-output" "sinh" "constantly" "cis"
    "remhash" "svref" "bit-xor" "inspect"
    "complexp" "multiple-value-call" "unexport" "bit-not"
    "describe" "slot-exists-p" "nreverse"
    "force-output" "write-line" "delete-if-not" "unuse-package"
    "lisp-implementation-version" "integer-length" "remprop"
    "pprint" "restart-name" "array-in-bounds-p" "write-sequence"
    "minusp" "complex" "position-if-not" "two-way-stream-output-stream"
    "listen" "write" "merge-pathnames" "evenp"
    "boole" "fceiling" "random-state-p" "get-internal-real-time"
    "make-instances-obsolete" "bit" "user-homedir-pathname"
    "write-byte" "use-package" "member" "signum"
    "cddar" "pathname-version" "complement" "ninth"
    "gcd" "delete-package" "pathname-directory" "phase"
    "tan" "hash-table-count" "get-internal-run-time"
    "decode-universal-time" "getf" "yes-or-no-p" "substitute"
    "file-write-date" "machine-instance" "lognor" "numberp"
    "equal" "get-output-stream-string" "eighth"
    "delete-if" "long-site-name" "load" "package-used-by-list"
    "y-or-n-p" "simple-condition-format-control" "string-lessp" "get-properties"
    "file-error-pathname" "stable-sort" "package-name" "set-dispatch-macro-character"
    "round" "compiler-macro-function" "subst" "truename"
    "find-if" "min" "cadddr"
    "rplacd" "documentation" "identity"
    "string-trim" "stream-element-type" "every" "rassoc-if"
    "find-all-symbols" "string-capitalize" "length" "symbol-package"
    "concatenated-stream-streams" "arrayp" "make-load-form-saving-slots" "write-to-string"
    "lcm" "load-time-value" "values" "substitute-if"
    "encode-universal-time" "subseq"
    "simple-condition-format-arguments" "enough-namestring" "rational"
    "acos" "import" "vector-push-extend" "vector-pop"
    "logbitp" "logeqv"
    "sixth" "method-qualifiers" "invoke-restart" "slot-boundp"
    "cdr" "nintersection" "char-code" "string-not-lessp"
    "endp" "graphic-char-p" "equalp"
    "write-char" "mapc" "logtest" "nstring-downcase"
    "compile-file" "butlast" "constantp" "rassoc-if-not"
    "char-name" "tree-equal" "subst-if-not"
    "read-byte" "find-symbol" "make-list" "packagep"
    "copy-readtable" "sxhash" "delete" "scale-float"
    "make-string-input-stream" "integer-decode-float"
    "format" "abs" "standard-char-p" "assoc"
    "count-if" "some" "compile-file-pathname"
    "nsubstitute-if-not" "char-not-lessp" "get"
    "echo-stream-output-stream" "typep" "find" "get-setf-expansion"
    "translate-logical-pathname" "pathname-host" "caadr" "isqrt"
    "slot-makunbound" "ldb" "make-echo-stream" "read-char"
    "cdaaar" "ldiff" "car"
    "array-displacement" "package-nicknames" "make-symbol" "subsetp"
    "set-macro-character" "host-namestring" "read-sequence" "sort"
    "array-dimensions" "string-greaterp" "two-way-stream-input-stream"
    "concatenate" "get-dispatch-macro-character" "compute-applicable-methods" "coerce"
    "maphash" "sleep" "read-from-string"
    "string-equal" "acosh" "fourth"
    "software-version" "adjust-array" "char" "make-pathname"
    "caar" "unbound-slot-instance" "cos" "mapcan"
    "caaaar" "method-combination-error" "count-if-not" "string-not-greaterp"
    "fdefinition" "cadadr" "float-radix" "string-upcase"
    "pprint-linear" "export" "cddddr" "random"
    "princ" "logand" "translate-pathname" "reinitialize-instance"
    "pprint-tabular" "acons" "make-string-output-stream" "open-stream-p"
    "rename-package" "make-synonym-stream" "pathname-type" "eql"
    "copy-tree" "max" "package-error-package" "assoc-if"
    "map" "remove" "list-all-packages" "plusp"
    "arithmetic-error-operands" "pairlis" "nreconc" "fmakunbound"
    "tailp" "symbol-plist" "logandc1"
    "rplaca" "logorc2" "array-row-major-index" "name-char"
    "read-preserving-whitespace" "broadcast-stream-streams" "find-restart" "type-error-expected-type"
    "search" "pathname" "cdadr"
    "array-total-size" "shared-initialize" "char-not-equal" "copy-list"
    "simple-vector-p" "revappend" "symbol-value" "prin1"
    "namestring" "bit-nor" "lognot" "append"
    "copy-seq" "bit-vector-p" "imagpart" "update-instance-for-redefined-class"
    "stream-error-stream" "class-of" "apropos"
    "symbolp" "boundp" "substitute-if-not"
    "mod" "compute-restarts" "subtypep" "row-major-aref"
    "add-method" "zerop" "sublis" "bit-nand"
    "delete-file" "logical-pathname-translations" "directory" "array-rank"
    "maplist" "allocate-instance" "ash" "position"
    "string-left-trim" "remove-duplicates" "initialize-instance" "replace"
    "logxor" "char-equal" "char-lessp" "ensure-generic-function"
    "fresh-line" "file-namestring" "find-package" "disassemble"
    "cosh" "vector-push" "make-concatenated-stream"
    "oddp" "compiled-function-p" "code-char" "characterp"
    "invalid-method-error" "readtablep" "pprint-tab" "copy-structure"
    "sin" "close" "copy-alist" "open"
    "notany" "tanh" "lower-case-p"
    "decode-float" "elt" "second"
    "nstring-capitalize" "atan" "floor" "array-element-type"
    "ldb-test" "atanh" "pathname-match-p" "read-delimited-list"
    "no-applicable-method" "realp" "cell-error-name" "class-name"
    "shadowing-import" "char-not-greaterp" "remove-if" "file-position"
    "find-method" "interactive-stream-p" "function-keywords" "file-length"
    "set" "upper-case-p" "hash-table-rehash-threshold" "unintern"
    "get-decoded-time" "echo-stream-input-stream" "ftruncate" "muffle-warning"
    "room" "atom" "get-universal-time" "caaddr"
    "numerator" "eval" "cadaar"
    "aref" "hash-table-p" "first" "logorc1"
    "cddaar" "reduce" "cadar" "dribble"
    "cddadr" "software-type" "rename-file"
    "bit-ior" "type-of" "input-stream-p" "digit-char"
    "set-exclusive-or" "realpart" "machine-type"
    "nsublis"
    "update-instance-for-different-class" "subst-if" "cons"
    "clear-output" "bit-orc1" "floatp" "caaar"
    "mapcar" "string" "pprint-newline" "package-use-list"
    "reverse" "make-package" "float-sign" "stream-external-format"
    "simple-string-p" "bit-eqv" "nsubstitute" "dpb"
    "digit-char-p" "denominator" "print" "set-pprint-dispatch"
    "bit-andc2" "assoc-if-not" "log" "cdaar"
    "cadr" "delete-duplicates" "cddr" "byte-position"
    "intersection" "string-downcase" "float-precision"
    "make-instance" "mapl" "file-author" "write-string"
    "char-int" "pathname-device" "apropos-list" "nsubst-if-not"
    "print-object" "caddar" "cdaddr" "nunion"
    "compile" "clear-input" "terpri" "fifth"
    "values-list" "expt" "readtable-case" "ffloor"
    "shadow" "rem" "null" "wild-pathname-p"
    "fboundp" "remove-method" "nth"
    "list" "synonym-stream-symbol" "make-two-way-stream"
    "schar" "directory-namestring" "makunbound"
    "logcount" "fill-pointer" "store-value" "nthcdr"
    "invoke-restart-interactively" "function-lambda-expression" "char-upcase" "copy-symbol"
    "exp" "nset-difference" "print-not-readable-object"
    "setq" "sbit" "cdddr"
    "vector" "vectorp" "describe-object" "array-dimension"
    "machine-version" "unread-char" "package-shadowing-symbols" "string-not-equal"
    "output-stream-p" "nconc" "make-sequence" "symbol-name"
    "nset-exclusive-or" "probe-file" "pprint-indent" "cdddar"
    "remove-if-not" "find-class" "asinh" "rassoc"
    "string-right-trim" "consp" "conjugate" "gethash"
    "change-class" "gensym" "hash-table-test" "invoke-debugger"
    "array-has-fill-pointer-p" "mismatch" "upgraded-array-element-type"
    "cdar" "short-site-name" "keywordp" "streamp"
    "position-if" "simple-bit-vector-p" "make-string"
    "float" "eq" "prin1-to-string" "slot-value"
    "macroexpand" "set-difference" "parse-integer" "function"
    "ensure-directories-exist" "make-hash-table" "truncate" "alpha-char-p"
    "adjoin" "sqrt" "intern" "mapcon"
    "read" "get-macro-character"
    "map-into" "nbutlast" "bit-andc1" "funcall"
    "clrhash" "princ-to-string" "caddr" "caaadr"
    "pathname-name" "caadar" "nsubstitute-if"
    "third" "pprint-dispatch" "fround" "not"
    "slot-unbound" "macroexpand-1" "macro-function" "peek-char"
    "make-broadcast-stream" "pprint-fill" "load-logical-pathname-translations" "last"
    "make-dispatch-macro-character" "char-greaterp" "listp" "bit-and"
    "symbol-function" "continue" "gentemp" "copy-pprint-dispatch"
    "nsubst"))

(add-hook 'lisp-mode-hook
          (lambda ()
            (setq orglpx-ppc-phantom-space-alist
                  '((?\( . "$($")
                    (?\[ . "$[$")))
            (setq orglpx-ppc-indent-chars
                  '(?\[ ?\( ?\#))
            (setq orglpx-ppc-space-analog-alist
                  '((init-space . "\\phantom{ii}")
                    (inner-space . "\\ ")))
            (setq orglpx-ppc-char-analog-alist
                  '((?* . "$\\ast$")
                    (?$ . "\\$")
                    (?# . "{\\upshape\\#}")
                    (?& . "\\&")
                    (?% . "\\%")
                    (?_ . "\\_")
                    (?^ . "\\^{}")
                    (?~ . "\\~{}")
                    (?\( . "$($")
                    (?\) . "$)$")
                    (?{ . "$\\{$")
                    (?} . "$\\}$")
                    (?\\ . "$\\backslash$")
                    (?| . "$\\vert$")
                    (?' . "{\\rmfamily\\mdseries\\upshape\\textquotesingle}")
                    (?` . "{\\rmfamily\\mdseries\\upshape\\textasciigrave}")
                    (?\" . "{\\rmfamily\\mdseries\\upshape\\textquotedbl}")))))

(defvar lisp-extra-function-names
  nil)

(defvar lisp-extra-macro-names
  nil)

(defvar lisp-types
  nil)

(let ((funcnames (cl-union lisp-function-names lisp-extra-function-names))
      (macronames (cl-union lisp-macro-names lisp-extra-macro-names)))
  (font-lock-add-keywords
   'lisp-mode
   `((,(concat "\\_<\\(" (regexp-opt '("for" "with" "from" "below" "upto" "downto" "above" "then"
                                       "collect" "collecting" "sum" "summing" "maximize" "maximizing"
                                       "minimize" "minimizing" "append" "appending" "by"
                                       "do" "finally" "into" "across" "when" "until" "unless" "while"
                                       "return" "repeat" "count" "counting" "on" "and"
                                       "if" "else")) "\\)\\_>") 1 font-lock-function-name-face)
     (,(concat "(\\_<\\(" (regexp-opt funcnames) "\\)\\_>") 1 font-lock-function-name-face)
     (,(concat "(\\_<\\(" (regexp-opt macronames) "\\)\\_>") 1 font-lock-keyword-face)
     (,(concat "\\_<\\(" (regexp-opt lisp-types) "\\)\\_>") 1 font-lock-type-face)
     ("\\(#\\\\\\(?:\\(?:\\w\\|\\s_\\)+\\|.\\)\\)" 1 font-lock-constant-face)
     ("\\<\\(t\\|nil\\)\\>" 1 font-lock-constant-face)
     (,(concat "\\("
               (rx (and (or (and symbol-start
                                 (or (and (? (any "-+")) (+ digit)
                                          (? (or (and (any "eE") (? (any "-+")) (+ digit))
                                                 (and "." (? (and (+ digit)
                                                                  (? (and (any "eE") (? (any "-+")) (+ digit))))))
                                                 (and "/" (+ digit)))))
                                     (and "." (+ digit) (? (and (any "eE") (? (any "-+")) (+ digit))))))
                            (and "#" symbol-start
                                 (or (and (any "bB") (? (any "-+")) (+ (any "01")))
                                     (and (any "oO") (? (any "-+")) (+ (any "0-7")))
                                     (and (any "xX") (? (any "-+")) (+ hex-digit)))))
                        symbol-end))
               "\\)")
      1 font-lock-constant-face))
   t))

(provide 'orglpx-ppc-lisp)
