;;; orglpx-editor.el --- Org+ orglpx editing -*- lexical-binding: t -*-

(add-to-list 'load-path (expand-file-name "../el/"))
(require 'orglpx-config)
(require 'xref)

(defun blink-region (beg end)
  (interactive "r")
  (pulse-momentary-highlight-region beg end))

(defun orglpx-goto-region (beg end)
  (goto-char beg)
  (org-show-entry)
  (blink-region beg end))

(defvar orglpx-editor-buffer-name nil)
(defvar orglpx-editor-buffer-tick nil)

(defun orglpx-editor-parse-buffer (&optional forcep)
  (interactive "P")
  (let ((bufname (buffer-name))
        (modtick (buffer-modified-tick)))
    (unless (and (equal orglpx-editor-buffer-name bufname)
                 (equal orglpx-editor-buffer-tick modtick)
                 (not forcep))
      (setq orglpx-editor-buffer-name bufname)
      (setq orglpx-editor-buffer-tick modtick)
      (let ((data (org-element-parse-buffer)))
        (orglpx-src-set-ids data)
        (orglpx-src-parse data)
        (orglpx-knuth-parse-headlines data)))))

(defun orglpx-src-at-point ()
  (let ((element (org-element-context)))
    (when (member (org-element-type element) '(src-block inline-src-block))
      (gethash (org-element-property :begin element)
               orglpx-src-data-beg-index))))

(defun orglpx-src-at-point/lookup (pos)
  (let ((srcs orglpx-src-data))
    (while (and srcs
                (not (<= (plist-get (car srcs) :beg)
                         pos
                         (plist-get (car srcs) :end))))
      (pop srcs))
    (car srcs)))

(defun orglpx-src-refered-from (src)
  (seq-map (lambda (id)
             (gethash id orglpx-src-data-index))
           (gethash (plist-get src :id) orglpx-src-refered-by-index)))

(defun orglpx-src-ref-apropos (ref)
  (let* ((siblings (gethash ref orglpx-src-sibling-index))
         (refd-from (orglpx-src-refered-from (car siblings))))
    (append siblings refd-from)))

(defun orglpx-src-less-than (src1 src2)
  (< (plist-get src1 :id) (plist-get src2 :id)))

(defun orglpx-src-after-point (point &optional src-list)
  (seq-find (lambda (src) (> (plist-get src :beg) point))
            (or src-list orglpx-src-data)))

(defun orglpx-src-before-point (point &optional src-list)
  (seq-find (lambda (src) (< (plist-get src :end) point))
            (reverse (or src-list orglpx-src-data))))

(defun orglpx-guess-file (src)
  (let ((file (plist-get src :file)))
    (if file src
      (let ((parents (orglpx-src-refered-from src))
            (candidate nil)
            (next-parents nil))
        (while (and parents (not candidate))
          (let ((maybe (seq-find (lambda (src*) (plist-get src* :file)) parents)))
            (if maybe (setq candidate maybe)
              (dolist (parent parents)
                (setq next-parents
                      (append (orglpx-src-refered-from parent)
                              next-parents)))
              (delete-dups (nreverse next-parents))
              (setq parents next-parents)
              (setq next-parents nil))))
        candidate))))

;;; XREF SUPPORT
(defun orglpx-xref-backend () 'orglpx)

(defvar orglpx-xref-summary-format
  (let ((fmt "[%s] %s %s"))
    (put-text-property 1 3 'face 'font-lock-keyword-face fmt)
    (put-text-property 5 7 'face 'font-lock-function-name-face fmt)
    (put-text-property 8 10 'face 'font-lock-comment-face fmt)
    fmt))

(defun orglpx-xref-src-code-preview (src)
  (let ((code (plist-get src :code)))
    (concat (substring code 0 (string-match-p "\n" code))
            (if (equal code "") "" "..."))))

(defun orglp-xref-src-xref (src)
  (xref-make (format orglpx-xref-summary-format
                     (plist-get src :lang)
                     (plist-get src :ref)
                     (orglpx-xref-src-code-preview src))
             (xref-make-buffer-location
              (get-buffer orglpx-editor-buffer-name)
              (plist-get src :beg))))

(cl-defmethod xref-backend-identifier-completion-table ((_backend (eql orglpx)))
  (when (derived-mode-p 'org-mode)
    (orglpx-editor-parse-buffer)
    orglpx-src-references))

(cl-defmethod xref-backend-identifier-at-point ((_backend (eql orglpx)))
  (when (derived-mode-p 'org-mode)
    (orglpx-editor-parse-buffer)
    (let ((thing (thing-at-point 'filename t)))
      (if (member thing orglpx-src-references)
          thing
        (let ((src (orglpx-src-at-point)))
          (cond ((not src) thing)
                ((plist-get src :file))
                ((plist-get src :ref))
                (t thing)))))))

(cl-defmethod xref-backend-definitions ((_backend (eql orglpx)) ref)
  (when (derived-mode-p 'org-mode)
    (orglpx-editor-parse-buffer)
    (seq-map 'orglp-xref-src-xref
             (gethash ref orglpx-src-sibling-index))))

(cl-defmethod xref-backend-references ((_backend (eql orglpx)) ref)
  (when (derived-mode-p 'org-mode)
    (orglpx-editor-parse-buffer)
    (seq-map 'orglp-xref-src-xref
             (let ((repr (car (gethash ref orglpx-src-sibling-index))))
               (orglpx-src-refered-from repr)))))

(cl-defmethod xref-backend-apropos ((_backend (eql orglpx)) regexp)
  (when (derived-mode-p 'org-mode)
    (orglpx-editor-parse-buffer)
    (seq-map 'orglp-xref-src-xref
             (sort (apply 'append
                          (seq-map 'orglpx-src-ref-apropos
                                   (seq-filter (lambda (ref)
                                                 (string-match-p regexp ref))
                                               orglpx-src-references)))
                   'orglpx-src-less-than))))

(add-hook 'xref-backend-functions #'orglpx-xref-backend nil)

;;; Completion
(defun orglpx-complete-ref (&optional initial)
  (let ((refs orglpx-src-references))
    (completing-read "Source code reference: "
                     refs nil t
                     (when (and initial (member initial refs))
                       initial))))

(defun orglpx-complete-file (&optional initial)
  (let ((files (map-keys-apply 'identity orglpx-src-file-index)))
    (completing-read "Source code file: "
                     files nil t
                     (when (and initial (member initial files))
                       initial))))

;;; Section links
(setq orglpx-link-follow
      (lambda (ref)
        (orglpx-editor-parse-buffer)
        (let ((src (car (gethash ref orglpx-src-sibling-index))))
          (when src
            (orglpx-goto-region (plist-get src :beg) (plist-get src :end))))))

(setq orglpx-link-complete
      (lambda (&optional arg)
        (orglpx-editor-parse-buffer)
        (format "orglpx:%s" (orglpx-complete-ref))))

(let ((orglpx-error-face
       `((t (:inherit org-link :foreground ,(face-attribute 'org-warning :foreground))))))
  (setq orglpx-link-face
        (lambda (ref)
          (orglpx-editor-parse-buffer)
          (if (member ref orglpx-src-references)
              'org-link
            orglpx-error-face))))

(setq orglpx-link-help-echo
      (lambda (_win _obj pos)
        (orglpx-editor-parse-buffer)
        (save-match-data
          (save-excursion
            (goto-char pos)
            (let ((ref (org-element-property :path (org-element-context))))
              (if (member ref orglpx-src-references)
                  "Jump to first definition"
                "Unknown section reference"))))))

;;; User interaction

(defun orglpx-insert-link-or-ref ()
  (interactive)
  (orglpx-editor-parse-buffer)
  (let ((src (orglpx-src-at-point)))
    (if src
        (insert (concat "<<" (orglpx-complete-ref) ">>"))
      (org-insert-link))))

(define-key org-mode-map (kbd "C-c C-l") 'orglpx-insert-link-or-ref)

(defun orglpx-goto-next ()
  (interactive)
  (orglpx-editor-parse-buffer)
  (let* ((src (orglpx-src-at-point))
         (next-src (if src (gethash (1+ (plist-get src :id)) orglpx-src-data-index)
                     (orglpx-src-after-point (point)))))
    (when next-src
      (orglpx-goto-region (plist-get next-src :beg)
                          (plist-get next-src :end)))))

(define-key org-mode-map (kbd "<C-right>") 'orglpx-goto-next)

(defun orglpx-goto-prev ()
  (interactive)
  (orglpx-editor-parse-buffer)
  (let* ((src (orglpx-src-at-point))
         (prev-src (if src (gethash (1- (plist-get src :id)) orglpx-src-data-index)
                     (orglpx-src-before-point (point)))))
    (when prev-src
      (orglpx-goto-region (plist-get prev-src :beg)
                          (plist-get prev-src :end)))))

(define-key org-mode-map (kbd "<C-left>") 'orglpx-goto-prev)

(defun orglpx-goto-next-sibling ()
  (interactive)
  (orglpx-editor-parse-buffer)
  (let* ((src (orglpx-src-at-point))
         (ref (plist-get src :ref)))
    (when (and src ref)
      (let ((next-src (orglpx-src-after-point (point) (gethash ref orglpx-src-sibling-index))))
        (when next-src
          (orglpx-goto-region (plist-get next-src :beg)
                              (plist-get next-src :end)))))))

(define-key org-mode-map (kbd "<C-down>") 'orglpx-goto-next-sibling)

(defun orglpx-goto-prev-sibling ()
  (interactive)
  (orglpx-editor-parse-buffer)
  (let* ((src (orglpx-src-at-point))
         (ref (plist-get src :ref)))
    (when (and src ref)
      (let ((prev-src (orglpx-src-before-point (point) (gethash ref orglpx-src-sibling-index))))
        (when prev-src
          (orglpx-goto-region (plist-get prev-src :beg)
                              (plist-get prev-src :end)))))))

(define-key org-mode-map (kbd "<C-up>") 'orglpx-goto-prev-sibling)

(defun orglpx-editor-tangle-file (&optional file)
  (interactive)
  (orglpx-editor-parse-buffer)
  (orglpx-tangle-file (orglpx-complete-file file)))

(defun orglpx-editor-tangle-file-dwim (&optional arg)
  (interactive "P")
  (orglpx-editor-parse-buffer)
  (if (not arg)
      (let ((src (orglpx-src-at-point)))
        (orglpx-editor-tangle-file (and src (plist-get (orglpx-guess-file src) :file))))
    (org-babel-tangle)))

(define-key org-mode-map (kbd "C-c o t") 'orglpx-editor-tangle-file-dwim)

(defvar orglpx-tangle-loader-alist nil)

(defun orglpx-editor-tangle-load-file (&optional arg file)
  (interactive)
  (orglpx-editor-parse-buffer)
  (let ((src (orglpx-editor-tangle-file file)))
    (when src
      (let ((loader (cdr (assoc (plist-get src :lang) orglpx-tangle-loader-alist))))
        (when loader
          (funcall loader arg src))))))

(defun orglpx-editor-tangle-load-file-dwim (&optional arg)
  (interactive "P")
  (orglpx-editor-parse-buffer)
  (let ((src (orglpx-src-at-point)))
    (orglpx-editor-tangle-load-file
     arg (and src (plist-get (orglpx-guess-file src) :file)))))

(define-key org-mode-map (kbd "C-c o l") 'orglpx-editor-tangle-load-file-dwim)

(provide 'orglpx-editor)
