;;; orglpx-editor-lisp.el -*- lexical-binding: t -*-

(require 'sly)
(require 'orglpx-editor)

(defun org-babel-edit-prep:lisp (info)
  (let* ((src (orglpx-src-at-point/lookup (nth 5 info)))
         (pkg (or (plist-get src :package)
                  (plist-get (orglpx-guess-file src) :package))))
    (when pkg
      (setq sly-buffer-package pkg))))

(defun lisp-loader (arg src)
  (let ((file (plist-get src :file)))
    (when (file-exists-p file)
      (find-file file)
      (add-hook 'sly-compilation-finished-hook
                'lisp-loader--after-hook
                100 t)
      (sly-compile-and-load-file arg))))

(defun lisp-loader--after-hook (successp notes buffer _loadp)
  (when (or successp (not notes))
    (kill-buffer buffer))
  (remove-hook 'sly-compilation-finished-hook
               'lisp-loader--after-hook t))

(add-to-list 'orglpx-tangle-loader-alist (cons "lisp" 'lisp-loader))

(provide 'orglpx-editor-lisp)
