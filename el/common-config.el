(add-to-list 'load-path (expand-file-name "~/.emacs.d/site-lisp/"))
(add-to-list 'load-path (expand-file-name "~/.emacs.d/elpa/sly-20210303.1148"))

(require 'orglp)


(provide 'common-config)
