;;; orglpx-config.el --- enumera orglpx config -*- lexical-binding: t -*-

(require 'orglpx)
(require 'orglpx-src)
(require 'orglpx-knuth)
(require 'orglpx-ppc)
;; (require 'orglpx-ppc-lisp)
;; (require 'orglpx-ppc-coq)

(setq-default org-export-use-babel nil)
(setq-default org-latex-caption-above nil)
(setq-default org-latex-prefer-user-labels t)

(setq orglpx-latex-packages '(("" "orglpxmac" nil nil)))

(defun parse-litprog (data backend info)
  (orglpx-src-set-ids data)
  (orglpx-src-parse data)
  (orglpx-knuth-parse-headlines data)
  data)

(setq orglpx-parse-function 'parse-litprog)

(setq orglpx-ppc-code-xref
      (lambda (ref label caption)
        (if label
            (format "\\lpxref{orglp-sec:%s}{%s}{%s}"
                    label label caption)
          (warn "Undefined code reference %S" ref)
          (format "\\lpsimpref{%s}" caption))))

(defun transcode-inline-src-block (block contents info)
  (let* ((id (org-element-property :src-id block))
         (entry (gethash id orglpx-src-data-index))
         (orglpx-ppc-inline-p t)
         (code (orglpx-ppc-code entry)))
    (concat "\\begin{lpinline}"
            code
            "\\end{lpinline}")))

(setq orglpx-latex-inline-src-block-transcoder 'transcode-inline-src-block)

(defun transcode-src-block (block contents info)
  (unless (equal "none" (cdr (assoc :exports (elt (org-babel-get-src-block-info 'light block) 2))))
    ;; (message "EXPORTS: %S" (org-element-property :exports block))
    ;; (message "!!!! BLOCK:\n%S" (org-babel-get-src-block-info 'light block))
    ;; (message "--------------------------------------------------")
    ;; (message "!!!! INFO:\n%S" info)
    (let* ((id (org-element-property :src-id block))
           (entry (gethash id orglpx-src-data-index))
           (code (orglpx-ppc-code entry))
           (sibling-ref-msg (info-on-same-ref entry))
           (usages-ref-msg (info-on-referers entry))
           (ref-msg (combine-info-refs sibling-ref-msg usages-ref-msg)))
      (concat "\\begin{lpcodedisplay}\n"
              (transcode-src-block-def-ref entry)
              "\\begin{lpwithcode}\n"
              "\\begin{lptabs}%\n"
              code
              "\\end{lptabs}\n"
              "\\end{lpwithcode}\n"
              ref-msg
              "\\end{lpcodedisplay}\n"))))

(defun combine-info-refs (siblings-ref-msg usages-ref-msg)
  (cond ((and siblings-ref-msg usages-ref-msg)
         (format "\\begin{lpbotmsg}\\noindent %s \\newline\n\\noindent %s\\end{lpbotmsg}\n"
                 siblings-ref-msg usages-ref-msg))
        (siblings-ref-msg
         (format "\\begin{lpbotmsg}\\noindent %s\\end{lpbotmsg}\n" siblings-ref-msg))
        (usages-ref-msg
         (format "\\begin{lpbotmsg}\\noindent %s\\end{lpbotmsg}\n" usages-ref-msg))
        (t "")))

(defun info-on-same-ref (entry)
  (let ((labels (src-block-definitions entry))
        (thislabel (plist-get entry :label)))
    (when (and labels (eq thislabel (car labels)))
      (let ((n (1- (length labels)))
            (others (cdr labels)))
        (cond ((= n 0) nil)
              ((= n 1)
               (format "Ver también la sección %s." (numref/label (car others))))
              (t
               (format "Ver también las secciones %s y %s."
                       (mapconcat 'numref/label (butlast others) ", ")
                       (numref/label (car (last others))))))))))

(defun info-on-referers (entry)
  (let ((ref (plist-get entry :ref)))
    (when ref
      (let ((siblings (gethash ref orglpx-src-sibling-index)))
        (when (eq entry (car siblings))
          (let ((labels (src-block-referrers entry))
                (thislabel (plist-get entry :label)))
            (when labels
              (let ((n (length labels)))
                (cond ((= n 0) nil)
                      ((= n 1)
                       (format "Este código se utiliza en la sección %s." (numref/label (car labels))))
                      (t
                       (format "Este código se utiliza en las secciones %s y %s."
                               (mapconcat 'numref/label (butlast labels) ", ")
                               (numref/label (car (last labels))))))))))))))

(defun src-block-definitions (entry)
  (let ((ref (plist-get entry :ref)))
    (when ref
      (let ((label (plist-get entry :label))
            (siblings (gethash ref orglpx-src-sibling-index)))
        (mapcar (lambda (entry*) (plist-get entry* :label)) siblings)))))

(defun src-block-referrers (entry)
  (let ((ref (plist-get entry :ref)))
    (when ref
      (let ((refd-by (mapcar (lambda (id) (gethash id orglpx-src-data-index))
                             (gethash (plist-get entry :id)
                                      orglpx-src-refered-by-index))))
        (remove nil (mapcar (lambda (entry*) (plist-get entry* :label)) refd-by))))))

(defun numref/label (label)
  (format "\\hyperlink{orglp-sec:%s}{%s}" label label))

(defun transcode-src-block-def-ref (entry)
  (let ((ref (plist-get entry :ref)))
    (if (not ref) "\\vspace{0.5\\baselineskip}\n"
      (let* ((label (plist-get entry :label))
             (representative (car (gethash ref orglpx-src-sibling-index)))
             (flabel (plist-get representative :label))
             (fcaption (plist-get representative :caption)))
        (format "\\noindent\\%s{orglp-sec:%s}{%s}{%s}\n"
                (if (eql label flabel) "lpxdefi" "lpxdef")
                flabel flabel
                (if fcaption
                    (org-export-data-with-backend
                     fcaption 'orglpx-latex orglpx-latex-typeset-info)
                  (format "{\\ttfamily %s}"
                          (org-export-data-with-backend
                           ref 'orglpx-latex orglpx-latex-typeset-info))))))))

(setq orglpx-latex-src-block-transcoder 'transcode-src-block)

(defun transcode-headline (element contents info)
  (let* ((id (org-element-property :hdl-id element))
         (entry (gethash id orglpx-knuth-headline-index))
         (level (plist-get entry :level))
         (title (org-export-data (plist-get entry :title) info)))
    (when (string= title "")
      (setq title nil))
    (concat (if title
                (format "\\lpssec{orglp-sec:%s}{%s}{%s}{%s}"
                        id level id title)
              (format "\\lpsec{orglp-sec:%s}{%s}"
                      id id))
            contents)))

(setq orglpx-latex-headline-transcoder 'transcode-headline)

(defun content-filter (content)
  (concat content
          "\n\n"
          "\\begin{orglpsecidx}\n"
          (section-name-list)
          "\\end{orglpsecidx}\n"
          "\\orglpsectoc\n"))

(defun section-name-list ()
  (let* ((data (mapcar (lambda (ref)
                         (let ((caption (gethash ref orglpx-src-reference-caption-index))
                               (siblings (gethash ref orglpx-src-sibling-index)))
                           (list (or (and caption
                                          (org-export-data-with-backend
                                           caption 'orglpx-latex orglpx-latex-typeset-info))
                                     (and ref
                                          (format "{\\ttfamily %s}"
                                                  (org-export-data-with-backend
                                                   ref 'orglpx-latex orglpx-latex-typeset-info))))
                                 (src-block-definitions (car siblings))
                                 (src-block-referrers (car siblings)))))
                       orglpx-src-references))
         (index (sort data (lambda (a b) (string< (car a) (car b))))))
    (mapconcat (lambda (index-entry)
                 (format "\\noindent\\lpsimpref{%s \\ %s}%s"
                         (car index-entry)
                         (mapconcat 'numref/label (cadr index-entry) ", ")
                         (if (caddr index-entry)
                             (format " utilizada en %s"
                                     (mapconcat 'numref/label (caddr index-entry) ", "))
                           "")))
               index
               "\\\\\n")))

(setq orglpx-latex-content-filter 'content-filter)

;;; Links to sections
(defvar orglpx-link-follow nil)
(defvar orglpx-link-complete nil)
(defvar orglpx-link-face nil)
(defvar orglpx-link-help-echo nil)

(defun orglpx-link-export (path description backend)
  (let ((src (car (gethash path orglpx-src-sibling-index))))
    (cond ((eq backend 'latex)
           (when src
             (if description
                 (format "\\hyperlink{orglp-sec:%s}{%s}"
                         (plist-get src :label) description)
               (format "\\hyperlink{orglp-sec:%s}{%s}"
                       (plist-get src :label)
                       (plist-get src :label))))))))

(org-link-set-parameters "orglpx"
                         :follow (lambda (path)
                                   (and orglpx-link-follow
                                        (funcall orglpx-link-follow path)))
                         :complete (lambda (&optional arg)
                                     (and orglpx-link-complete
                                          (funcall orglpx-link-complete arg)))
                         :face (lambda (path)
                                 (and orglpx-link-face
                                      (funcall orglpx-link-face path)))
                         :help-echo (lambda (win obj pos)
                                      (and orglpx-link-help-echo
                                           (funcall orglpx-link-help-echo win obj pos)))
                         :export 'orglpx-link-export)

(defun orglpx-tangle-file (file)
  (let ((src (gethash file orglpx-src-file-index)))
    (when src
      (save-excursion
        (goto-char (plist-get src :beg))
        (org-babel-tangle '(4)))
      src)))

(provide 'orglpx-config)
