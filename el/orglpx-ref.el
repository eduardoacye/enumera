;;; -*- lexical-binding: t; -*-

(load (expand-file-name "../el/utils.el"))

(dolist (lib (list "dash" "f" "s" "htmlize" "hydra" "lv"
                   "parsebib" "helm-core" "helm" "async"
                   "helm-bibtex" "bibtex-completion" "biblio"
                   "biblio-core"
                   "org-ref"))
  (add-library-load-path lib))

(setq org-latex-prefer-user-labels t)
(bibtex-set-dialect 'biblatex)

(require 'org-ref)

(provide 'orglpx-ref)
