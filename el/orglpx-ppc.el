;;; orglpx-ppc.el --- Pretty print code -*- lexical-binding: t -*-

(require 'orglpx-src)

(defun put-text-property-here (prop val)
  (put-text-property (point) (1+ (point)) prop val))

(defun line-above-p ()
  (not (= (point-min) (point-at-bol))))

(defvar orglpx-ppc-inline-p nil)

(defun orglpx-ppc-maybe-tabbed-newline ()
  (if orglpx-ppc-inline-p
      'plain-newline
    'tabbed-newline))

(defconst orglpx-ppc-space-type 'orglpx-ppc-space-type)
(defconst orglpx-ppc-tab-type 'orglpx-ppc-tab-type)
(defconst orglpx-ppc-env-type 'orglpx-ppc-env-type)
(defconst orglpx-ppc-env-place 'orglpx-ppc-env-place)

(defvar orglpx-ppc-phantom-space-alist nil
  "Alist mapping characters to their TeX analog for phantom macro.")

(defvar orglpx-ppc-indent-chars nil
  "List of characters that can introduce an indentation point.")

(defvar orglpx-ppc-supported-faces
  '((font-lock-warning-face)
    (font-lock-function-name-face)
    (font-lock-variable-name-face)
    (font-lock-keyword-face)
    (font-lock-comment-face)
    (font-lock-comment-delimiter-face)
    (font-lock-type-face)
    (font-lock-constant-face)
    (font-lock-builtin-face)
    (font-lock-preprocessor-face)
    (font-lock-string-face)
    (font-lock-doc-face)))

(defvar orglpx-ppc-tab-analog-default "")

(defvar orglpx-ppc-tab-analog-alist
  '((set-tab . "\\=")
    (move-tab . "\\>")
    (tabbed-newline . "\\\\")))

(defvar orglpx-ppc-space-analog-default " ")

(defvar orglpx-ppc-space-analog-alist nil)

(defvar orglpx-ppc-char-analog-alist nil)

(defvar orglpx-ppc-string-replace-alist nil)

(defvar orglpx-ppc-env-format-default "%s")

(defvar orglpx-ppc-env-tex-math-delim "$")

(defvar orglpx-ppc-env-format-alist
  '((warning           . "\\swarn{%s}")
    (function-name     . "\\sfuncname{%s}")
    (variable-name     . "\\svarname{%s}")
    (keyword           . "\\skeyword{%s}")
    (comment           . "\\scomment{%s}")
    (comment-delimiter . "\\scommentdelim{%s}")
    (type              . "\\stype{%s}")
    (constant          . "\\sconst{%s}")
    (builtin           . "\\sbuiltin{%s}")
    (preprocessor      . "\\spreproc{%s}")
    (string            . "\\sstring{%s}")
    (doc               . "\\sdoc{%s}")
    (tex-math          . "$%s$")
    (noweb-ref         . "\\snoweb{%s}")))

(defun phantom-space-p (space)
  (and (consp space)
       (eq (car space) 'phantom)
       (characterp (cadr space))
       (not (cddr space))))

(defun face-env (face)
  "Convert FACE to its `orglpx-ppc-env-type' analog."
  (cond ((not face) nil)
        ((consp face)
         (face-env (car face)))
        ((or (symbolp face) (facep face))
         (let ((entry (assoc face orglpx-ppc-supported-faces)))
           (if entry
               (let ((name (symbol-name (or (cdr entry) face))))
                 (when (string-match "font-lock-\\(.+\\)-face" name)
                   (let ((str (match-string 1 name)))
                     (when str (intern str)))))
             (warn "Falling back from face %S" face)
             (face-env (face-attribute face :inherit)))))))

;;; TODO:
;;; - orglp-lang-keywords-alist
;;; - orglp-src-keywords
;;; - orglp-lang-keywords

(defun orglpx-ppc-code (src)
  (let ((code (plist-get src :code))
        (lang (plist-get src :lang)))
    (with-temp-buffer
      (let ((mode (org-src-get-lang-mode lang)))
        (orglpx-ppc-typeset-src-insert-code-in-mode code mode)
        (orglpx-ppc-typeset-src-instrument-buffer)
        (with-output-to-string
          (orglpx-ppc-typeset-src-translate))))))

(defun orglpx-ppc-typeset-src-insert-code-in-mode (code mode)
  "Activate MODE and insert CODE in current buffer.
Ensures that both syntax properties and font-lock text properties
are present."
  (when (functionp mode)
    (funcall mode))
  (insert code)
  (let ((beg (point-min))
        (end (point-max)))
    (untabify beg end)
    (goto-char beg)
    (syntax-propertize end)
    (font-lock-ensure beg end)
    (dolist (replac orglpx-ppc-string-replace-alist)
      (goto-char (point-min))
      (replace-string (car replac) (cdr replac)))
    (goto-char (point-min))))

(defun orglpx-ppc-typeset-src-instrument-buffer ()
  "Add texmac text properties to current buffer."
  (goto-char (point-min))
  (while (not (eobp))
    (orglpx-ppc-typeset-src-instrument-line)
    (forward-line))
  (when (progn
          (beginning-of-line)
          (and (looking-at-p "[[:space:]]*$") (line-above-p)))
    (forward-line -1)
    (end-of-line)
    (put-text-property-here orglpx-ppc-tab-type 'plain-newline))
  (orglpx-ppc-typeset-src-patch-noweb-refs)
  (orglpx-ppc-typeset-src-patch-tex-maths)
  (unless orglpx-ppc-inline-p
    (goto-char (point-min))
    (while (not (eobp))
      (orglpx-ppc-typeset-src-peephole-adjust)
      (forward-line))))

(defun orglpx-ppc-typeset-src-instrument-line ()
  "Instrument the current line with orglpx-ppc text properties.
The line is read depending on the point in the current buffer."
  (orglpx-ppc-typeset-src-classify-spaces)
  (orglpx-ppc-typeset-src-classify-lexenvs))

(defun orglpx-ppc-typeset-src-classify-spaces ()
  "Identify spaces before and after the first graphic char and handle the newline in current line."
  (let ((bol (point-at-bol))
        (eol (point-at-eol)))
    (back-to-indentation)
    (put-text-property bol (point) orglpx-ppc-space-type 'init-space)
    (while (< (point) eol)
      (skip-syntax-forward "^\s" eol)
      (unless (eolp)
        (put-text-property-here orglpx-ppc-space-type 'inner-space)
        (forward-char)))
    (cond ((char-after eol)
           ;; more lines follow
           (put-text-property-here orglpx-ppc-tab-type (orglpx-ppc-maybe-tabbed-newline)))
          ((progn
             (beginning-of-line)
             (looking-at-p "[[:space:]]*$"))
           ;; last line is blank, fix previous newline
           (delete-region bol eol)
           (when (line-above-p)
             (forward-line -1)
             (end-of-line)
             (put-text-property-here orglpx-ppc-tab-type 'plain-newline)))
          (t
           ;; last line contains graphic chars
           (goto-char eol)
           (insert "\n")
           (goto-char eol)
           (put-text-property-here orglpx-ppc-tab-type 'plain-newline)))
    (goto-char eol)))

(defun orglpx-ppc-typeset-src-classify-lexenvs ()
  "Identify lexical environments in current line."
  (let ((bol (point-at-bol))
        (eol (point-at-eol))
        next)
    (goto-char bol)
    (while (not (eolp))
      (setf next (next-single-property-change (point) 'face nil eol))
      (let ((env (face-env (get-char-property (point) 'face))))
        (when env
          (put-text-property (point) next orglpx-ppc-env-type env)
          (if (= next (1+ (point)))
              (put-text-property-here orglpx-ppc-env-place 'single)
            (put-text-property-here orglpx-ppc-env-place 'start)
            (put-text-property (1+ (point)) (1- next) orglpx-ppc-env-place 'middle)
            (put-text-property (1- next) next orglpx-ppc-env-place 'end))))
      (goto-char next))))

(defun orglpx-ppc-typeset-src-patch-noweb-refs ()
  "Identify noweb references in buffer and mark them for texmac."
  (goto-char (point-min))
  (while (re-search-forward (org-babel-noweb-wrap) nil t)
    (let ((ref-beg (match-beginning 0))
          (ref-end (match-end 0))
          (name-beg (match-beginning 1))
          (name-end (match-end 1)))
      (remove-text-properties
       ref-beg ref-end
       (list orglpx-ppc-env-type t orglpx-ppc-env-place t))
      (put-text-property ref-beg name-beg orglpx-ppc-env-type 'noweb-ref-open)
      (put-text-property name-beg name-end orglpx-ppc-env-type 'noweb-ref-name)
      (put-text-property name-end ref-end orglpx-ppc-env-type 'noweb-ref-close))))

(defun orglpx-ppc-typeset-src-patch-tex-maths ()
  (goto-char (point-min))
  (while (search-forward orglpx-ppc-env-tex-math-delim nil t)
    (let ((open-beg (match-beginning 0))
          (open-end (match-end 0)))
      (if (search-forward orglpx-ppc-env-tex-math-delim nil t)
          (let ((close-beg (match-beginning 0))
                (close-end (match-end 0)))
            (remove-text-properties
             open-beg close-end
             (list orglpx-ppc-env-type t orglpx-ppc-env-place t))
            (put-text-property open-beg open-end orglpx-ppc-env-type 'tex-math-open)
            (put-text-property open-end close-beg orglpx-ppc-env-type 'tex-math)
            (put-text-property close-beg close-end orglpx-ppc-env-type 'tex-math-close)
            (put-text-property (1- open-beg) open-beg orglpx-ppc-env-place 'end)
            (when (eq (get-text-property close-end orglpx-ppc-env-place) 'middle)
              (put-text-property close-end (1+ close-end) orglpx-ppc-env-place 'start)))
        (error "Malformed tex math environment")))))

(defun orglpx-ppc-typeset-src-peephole-adjust ()
  "Adjust the orglpx-ppc code properties in the current and previous lines."
  (let ((bol (point-at-bol))
        (eol (point-at-eol)))
    (forward-line -1)
    (let ((bol* (point-at-bol))
          (eol* (point-at-eol)))
      (goto-char bol)
      (let ((continuep (line-above-p)))
        (while continuep
          (let* ((phere (point))
                 (offset (- phere bol))
                 (pabove (+ bol* offset)))
            (cond ((or (>= phere eol) (>= pabove eol*))
                   (setf continuep nil))
                  ((eq (get-char-property phere orglpx-ppc-space-type) 'init-space)
                   (unless (member (get-char-property pabove orglpx-ppc-env-type) '(string math))
                     (let ((char-above (char-after pabove))
                           (space-above (get-char-property pabove orglpx-ppc-space-type))
                           (tab-above (get-char-property pabove orglpx-ppc-tab-type)))
                       (cond ((phantom-space-p space-above)
                              (put-text-property-here orglpx-ppc-space-type space-above))
                             ((assoc char-above orglpx-ppc-phantom-space-alist)
                              (put-text-property-here orglpx-ppc-space-type `(phantom ,char-above))))
                       (when (member tab-above '(set-tab move-tab))
                         (put-text-property-here orglpx-ppc-tab-type 'move-tab))))
                   (forward-char))
                  ((bolp)
                   (setf continuep nil))
                  ;; nuevo codigo
                  (;; (not (get-char-property phere orglpx-ppc-tab-type))
                   (get-char-property pabove orglpx-ppc-tab-type)
                   (when (member (get-char-property pabove orglpx-ppc-tab-type)
                                 '(set-tab move-tab))
                     (put-text-property-here orglpx-ppc-tab-type 'move-tab))
                   (forward-char)
                   (setf continuep nil))
                  ((let ((space-above (get-char-property pabove orglpx-ppc-space-type)))
                     (or (member space-above '(init-space init-plain-space))
                         (phantom-space-p space-above)))
                   (forward-char)
                   (setf continuep nil))
                  (;; (and (eq (char-after (1- pabove)) ?\s)
                   ;;      (not (get-char-property (1- pabove) orglpx-ppc-env-type)))
                   (and (eql (char-after (1- pabove)) ?\s)
                        (not (get-char-property (1- pabove) orglpx-ppc-env-type)))
                   (put-text-property pabove (1+ pabove) orglpx-ppc-tab-type 'set-tab)
                   (put-text-property-here orglpx-ppc-tab-type 'move-tab)
                   (forward-char)
                   (setf continuep nil))
                  (t
                   (forward-char)
                   (setf continuep nil))))))
      (let ((pabove (+ bol* (- (point) bol))))
        (save-excursion
          (goto-char pabove)
          (while (< (point) eol*)
            (when (eq (get-char-property (point) orglpx-ppc-tab-type) 'set-tab)
              (put-text-property-here orglpx-ppc-tab-type nil))
            (forward-char))))
      (goto-char (1+ bol))
      (let ((prev-char-for-tab nil))
        (while (< (point) eol)
          (cond ((and (member (get-char-property (point) orglpx-ppc-env-type)
                              '(string))
                      (member (get-char-property (point) orglpx-ppc-env-place)
                              '(middle end single)))
                 (setf prev-char-for-tab nil))
                ((member (char-after (point)) orglpx-ppc-indent-chars)
                 (unless (or (get-char-property (point) orglpx-ppc-tab-type)
                             (eq prev-char-for-tab (char-after (point))))
                   (put-text-property-here orglpx-ppc-tab-type 'set-tab))
                 (setf prev-char-for-tab (char-after (point))))
                (t
                 (setf prev-char-for-tab nil)))
          (forward-char)))
      (goto-char eol)
      (while (and (> (point) bol)
                  (not (eq (get-char-property (point) orglpx-ppc-tab-type)
                           'move-tab)))
        (forward-char -1))
      (when (eq (get-char-property (point) orglpx-ppc-tab-type) 'move-tab)
        (while (> (point) bol)
          (forward-char -1)
          (let ((space (get-char-property (point) orglpx-ppc-space-type)))
            (when (or (eq space 'init-space) (phantom-space-p space))
              (put-text-property-here orglpx-ppc-space-type 'init-plain-space)))))
      (goto-char eol))))

(defun orglpx-ppc-typeset-src-translate ()
  (goto-char (point-min))
  (let ((phere (point)))
    (while (< phere (point-max))
      (let ((char (char-after phere))
            (space (get-char-property phere orglpx-ppc-space-type))
            (tab (get-char-property phere orglpx-ppc-tab-type))
            (env (get-char-property phere orglpx-ppc-env-type))
            (place (get-char-property phere orglpx-ppc-env-place)))
        (cond ((eq char ?\n)
               (orglpx-ppc-typeset-tab tab)
               (princ "%\n")
               (setf phere (1+ phere)))
              (env
               (orglpx-ppc-typeset-tab tab)
               (cond ((eq env 'tex-math-open)
                      (pcase-let ((`(,math . ,next) (orglpx-ppc-parse-math phere)))
                        (orglpx-ppc-typeset-env math 'tex-math)
                        (setf phere next)))
                     ((eq env 'noweb-ref-open)
                      (pcase-let ((`(,name . ,next) (orglpx-ppc-parse-noweb phere)))
                        (orglpx-ppc-typeset-env name 'noweb-ref)
                        (setf phere next)))
                     ((eq place 'start)
                      (pcase-let ((`(,content . ,next) (orglpx-ppc-parse-env env phere)))
                        (orglpx-ppc-typeset-env (orglpx-ppc-env-string content) env)
                        (setf phere next)))
                     ((eq place 'single)
                      (orglpx-ppc-typeset-env (orglpx-ppc-env-string (string char)) env)
                      (setf phere (1+ phere)))
                     (t
                      (if (eq char ?\s)
                          (orglpx-ppc-typeset-space space)
                        (orglpx-ppc-typeset-char char))
                      (setf phere (1+ phere)))))
              (t
               (orglpx-ppc-typeset-tab tab)
               (if (eq char ?\s)
                   (orglpx-ppc-typeset-space space)
                 (orglpx-ppc-typeset-char char))
               (setf phere (1+ phere))))))))

(defun orglpx-ppc-typeset-tab (tab)
  (let ((analog (assoc tab orglpx-ppc-tab-analog-alist)))
    (princ (if analog (cdr analog)
             orglpx-ppc-tab-analog-default))))

(defun orglpx-ppc-typeset-space (space)
  (if (phantom-space-p space)
      (let ((analog (assoc (cadr space) orglpx-ppc-phantom-space-alist)))
        (princ (if analog (format "\\phantom{%s}" (cdr analog))
                 orglpx-ppc-space-analog-default)))
    (let ((analog (assoc space orglpx-ppc-space-analog-alist)))
      (princ (if analog (cdr analog)
               orglpx-ppc-space-analog-default)))))

(defun orglpx-ppc-typeset-char (char)
  (let ((analog (assoc char orglpx-ppc-char-analog-alist)))
    (princ (if analog (cdr analog) (string char)))))

(defun orglpx-ppc-parse-env (env phere)
  (let* ((mbeg (next-single-property-change phere orglpx-ppc-env-place))
         (ebeg (next-single-property-change mbeg orglpx-ppc-env-place)))
    (if (eq 'end (get-char-property mbeg orglpx-ppc-env-place))
        (cons (buffer-substring-no-properties phere ebeg)
              ebeg)
      (let ((next (next-single-property-change ebeg orglpx-ppc-env-place)))
        (cons (buffer-substring-no-properties phere next)
              next)))))

(defun orglpx-ppc-env-string (content)
  (with-output-to-string (seq-do 'orglpx-ppc-typeset-char content)))

(defvar orglpx-ppc-code-xref nil)

(defun orglpx-ppc-typeset-env (content env)
  (cond ((eq env 'noweb-ref)
         (let* ((ref content)
                (representative (car (gethash ref orglpx-src-sibling-index)))
                (flabel (plist-get representative :label))
                (fcaption (plist-get representative :caption))
                (caption (if fcaption
                             (org-export-data-with-backend
                              fcaption 'orglpx-latex orglpx-latex-typeset-info)
                           (format "{\\ttfamily %s}"
                                   (org-export-data-with-backend
                                    ref 'orglpx-latex orglpx-latex-typeset-info)))))
           (princ (if orglpx-ppc-code-xref
                      (funcall orglpx-ppc-code-xref ref flabel caption)
                    (format "\\lpsimpref{%s}" caption)))))
        (t
         (let ((fmt (assoc env orglpx-ppc-env-format-alist)))
           (princ (format (if fmt (cdr fmt) orglpx-ppc-env-format-default) content))))))

(defun orglpx-ppc-parse-noweb (phere)
  (let* ((nbeg (next-single-property-change phere orglpx-ppc-env-type))
         (cbeg (next-single-property-change nbeg orglpx-ppc-env-type))
         (next (next-single-property-change cbeg orglpx-ppc-env-type)))
    (cons (buffer-substring-no-properties nbeg cbeg)
          next)))

(defun orglpx-ppc-parse-math (phere)
  (let* ((beg (next-single-property-change phere orglpx-ppc-env-type))
         (end (next-single-property-change beg orglpx-ppc-env-type))
         (next (next-single-property-change end orglpx-ppc-env-type)))
    (cons (buffer-substring-no-properties beg end)
          next)))

(provide 'orglpx-ppc)
