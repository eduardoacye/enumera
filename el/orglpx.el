;;; orglpx.el --- LP/Org -*- lexical-binding: t -*-

(require 'org)
(require 'org-element)
(require 'org-src)
(require 'ox)
(require 'seq)
(require 'map)
(require 'cl-lib)

(defvar orglpx-parse-function nil
  "Function to be called on export with parsed data.")

(defun orglpx-parse (data backend info)
  (when orglpx-parse-function
    (funcall orglpx-parse-function data backend info))
  data)

(defvar orglpx-latex-typeset-info nil)

(defvar orglpx-latex-hyperref-template
  (concat "\\hypersetup{\n"
          "  pdfauthor={%a},\n"
          "  pdftitle={%t},\n"
          "  pdfkeywords={%k},\n"
          "  pdfsubject={%d},\n"
          "  pdfcreator={%c},\n"
          "  pdflang={%L},\n"
          "  colorlinks=true,\n"
          "  linkcolor=[rgb]{0.56,0.0,0.4588},\n"
          "  linktocpage=true"
          "}\n"))

(org-export-define-derived-backend 'orglpx-latex 'latex
  :translate-alist '((src-block . orglpx-latex-src-block)
                     (inline-src-block . orglpx-latex-inline-src-block)
                     (headline . orglpx-latex-headline)
                     (template . orglpx-latex-template))
  :filters-alist '((:filter-parse-tree orglpx-parse
                                       org-latex-math-block-tree-filter
				       org-latex-matrices-tree-filter
				       org-latex-image-link-filter))
  :options-alist
  '((:latex-hyperref-template nil nil orglpx-latex-hyperref-template t)))

(defun orglpx-export-to-latex (&optional async subtreep visible-only body-only ext-plist)
  (interactive)
  (let ((outfile (org-export-output-file-name ".tex" subtreep)))
    (org-export-to-file 'orglpx-latex outfile
      async subtreep visible-only body-only ext-plist)))

(defvar orglpx-latex-packages nil
  "List of packages to be appended before `org-latex-packages-alist' on export.")

(defvar orglpx-latex-content-filter nil)

(defun orglpx-latex-template (contents info)
  "Wrapper for `org-latex-template' to append `orglpx-latex-packages' to `org-latex-packages-alist'."
  (let ((org-latex-packages-alist (append orglpx-latex-packages org-latex-packages-alist))
        (orglpx-latex-typeset-info info))
    (org-latex-template (if orglpx-latex-content-filter
                            (funcall orglpx-latex-content-filter contents)
                          contents)
                        info)))

(defvar orglpx-latex-inline-src-block-transcoder nil
  "Function for transcoding inline source blocks to LaTeX.")

(defun orglpx-latex-inline-src-block (block contents info)
  "Transcode inline source code BLOCK into LaTeX.
INFO is a plist holding contextual information."
  (if orglpx-latex-inline-src-block-transcoder
      (let ((orglpx-latex-typeset-info info))
        (funcall orglpx-latex-inline-src-block-transcoder
                 block contents info))
    (org-latex-inline-src-block block contents info)))

(defvar orglpx-latex-src-block-transcoder nil
  "Function for transcoding source blocks to LaTeX.")

(defun orglpx-latex-src-block (block contents info)
  "Transcode source code BLOCK into LaTeX.
INFO is a plist holding contextual information."
  (if orglpx-latex-src-block-transcoder
      (let ((orglpx-latex-typeset-info info))
        (funcall orglpx-latex-src-block-transcoder
                 block contents info))
    (org-latex-src-block block contents info)))

(defvar orglpx-latex-headline-transcoder nil
  "Function for transcoding headlines to LaTeX.")

(defun orglpx-latex-headline (headline contents info)
  "Transcode a HEADLINE element from Org to LaTeX.
CONTENTS holds the contents of the headline.  INFO is a plist
holding contextual information."
  (if orglpx-latex-headline-transcoder
      (let ((orglpx-latex-typeset-info info))
        (funcall orglpx-latex-headline-transcoder
                 headline contents info))
    (org-latex-headline headline contents info)))

(provide 'orglpx)
