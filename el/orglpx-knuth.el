;;; orglpx-knuth.el --- Headline and source management for Org mode ala knuth -*- lexical-binding: t -*-

(require 'orglpx-src)

(defvar orglpx-knuth-headline-count 0
  "Number of parsed headlines.")

(defvar orglpx-knuth-headlines nil
  "List of headline data entries.")

(defvar orglpx-knuth-headline-index nil
  "Hash table mapping IDs to their headline entry.")

(defvar orglpx-knuth-headline-path nil
  "Hash table mapping headline ID to a list of parent headline IDs.")

(defvar orglpx-knuth-source-path nil
  "Hash table mapping source code ID to a list of parent headline IDs.")

(defvar orglpx-knuth-headline-source nil
  "Hash table mapping headline IDs to source code IDs.")

(defun orglpx-knuth--init-state ()
  (setq orglpx-knuth-headline-count 0
        orglpx-knuth-headlines nil
        orglpx-knuth-headline-index (make-hash-table :test 'eql)
        orglpx-knuth-headline-path (make-hash-table :test 'eql)
        orglpx-knuth-source-path (make-hash-table :test 'eql)
        orglpx-knuth-headline-source (make-hash-table :test 'eql)))

;;; must run after `orglpx-src-set-ids' and `orglpx-src-parse' on the same data argument.
(defun orglpx-knuth-parse-headlines (data)
  (orglpx-knuth--init-state)
  (setq orglpx-knuth-headlines
        (org-element-map data
            '(headline src-block inline-src-block)
          'orglpx-knuth--parse-element)))

(defun orglpx-knuth--parse-element (element)
  (let ((type (org-element-type element)))
    (cond ((eq type 'headline)
           (orglpx-knuth--parse-headline element))
          ((eq type 'src-block)
           (orglpx-knuth--parse-src-block element)
           nil)
          ((eq type 'inline-src-block)
           (orglpx-knuth--parse-inline-src-block element)
           nil))))

(defun orglpx-knuth--parse-headline (element)
  (setq orglpx-knuth-headline-count (1+ orglpx-knuth-headline-count))
  (let ((id orglpx-knuth-headline-count)
        (beg (org-element-property :begin element))
        (level (org-element-property :level element))
        (title (org-element-property :title element))
        (parent (org-element-property :parent element)))
    (org-element-put-property element :hdl-id id)
    (let ((entry (list :id id :beg beg :level level :title title)))
      (puthash id entry orglpx-knuth-headline-index)
      (let ((parent-path (orglpx-knuth--headline-path parent)))
        (puthash id (cons id (orglpx-knuth--headline-path parent))
                 orglpx-knuth-headline-path)
        entry)
      entry)))

(defun orglpx-knuth--headline-path (element)
  (if (eq 'headline (org-element-type element))
      (let ((id (org-element-property :hdl-id element)))
        (unless id
          (warn "Headline missing ID at point %s" (org-element-property :begin element)))
        (and id (gethash id orglpx-knuth-headline-path)))
    (let ((parent (org-element-property :parent element)))
      (when parent
        (orglpx-knuth--headline-path parent)))))

(defun orglpx-knuth--parse-src-block (element)
  (let ((id (org-element-property :src-id element))
        (parent (org-element-property :parent element)))
    (let ((path (orglpx-knuth--headline-path parent))
          (src-entry (gethash id orglpx-src-data-index)))
      (let ((hdl-id (car path)))
        (when (and src-entry hdl-id)
          (plist-put src-entry :label hdl-id)
          (puthash id path orglpx-knuth-source-path)
          (when (gethash hdl-id orglpx-knuth-headline-source)
            (warn "Headline contains multiple source code blocks at point %s"
                  (org-element-property :begin element)))
          (puthash hdl-id id orglpx-knuth-headline-source))))))

(defun orglpx-knuth--parse-inline-src-block (element)
  (let ((id (org-element-property :src-id element))
        (parent (org-element-property :parent element)))
    (let ((path (orglpx-knuth--headline-path parent))
          (src-entry (gethash id orglpx-src-data-index)))
      (let ((hdl-id (car path)))
        (when (and src-entry hdl-id)
          (plist-put src-entry :label hdl-id)
          (puthash id path orglpx-knuth-source-path))))))

(provide 'orglpx-knuth)
