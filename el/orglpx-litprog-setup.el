(require 'orglpx-config)

(setq-default lisp-extra-function-names
              '("simplex/factorial" "simplex/iterative" "simplex/fixnum" "simplex/combined"
                "simplex-from-table-2" "simplex-from-table-3" "simplex"
                "simplex-inv"
                "rank" "unrank" "rank*" "unrank*" "rank2*" "unrank2*" "rank/dup" "unrank/dup"
                "component-sum-offset" "component-sum-count"

                "tuplas:rank" "tuplas:unrank"
                "tuplas:rank*" "tuplas:unrank*"
                "tuplas:component-sum-offset" "tuplas:component-sum-count"
                "cardinality" "enumeratesp" "position-of" "object-at"
                "empty-environment" "find-enumeration" "ensure-enumeration"
                "contextualize-redefinition" "context-cardinality"
                "context-enumeratesp" "context-rank" "context-unrank"
                "symbolicate"

                "imp-cardinality" "imp-enumeratesp" "imp-rank" "imp-unrank"
                "size-nat" "size-loc" "naturalp" "size-arith"
                "size-bool" "size-prog" "count-nat" "count-loc"
                "count-arith-arith" "count-arith"
                "count-bool-bool" "count-bool"
                "count-prog" "count-loc-arith" "count-bool-prog"
                "count-prog-prog" "count-bool-prog-prog"
                "sunrank-nat" "sunrank-loc"
                "sunrank-arith" "sunrank-arith-arith"
                "sunrank-bool" "sunrank-bool-bool"
                "sunrank-prog" "sunrank-loc-arith" "sunrank-bool-prog"
                "sunrank-prog-prog" "sunrank-bool-prog-prog"
                "imp-sunrank"
                "random-arith" "random-bool" "random-prog"
                "prog-generator"
                "separate" "indent" "newline"
                "imp-write"
                "ignore-whitespace" "read-number" "read-constituent"
                "read-token" "read-tokens"
                "read-nat" "read-loc" "read-binop" "read-arith"
                "read-bool" "read-prog"
                "imp-read"))

(setq-default lisp-extra-macro-names
              '("defsystem" "@" "defenum" "elet" "defcenum" "defun/memo"))

(setq-default lisp-types
              '("evar" "environment" "closure" "recursive" "context"
                "evoid" "enat" "enatr" "econs" "elist" "eproduct"
                "esum" "esum*" "eunion" "ekleene"
                "enumeration-condition" "enumeration-error"
                "enumeration-warning"
                "malformed-object-error" "malformed-position-error"
                "environment-error" "redefinition-warning"))

(require 'orglpx-ppc-lisp)

(provide 'orglpx-litprog-setup)
