(setq-default orglp-lisp-extra-function-names
              (append '("naturalp" "tuplap" "count-diagonal" "combinations"
                        "factorial" "tupla<=" "lex-sort" "relex-sort"
                        "colex-sort" "corelex-sort" "count-below"
                        "simplex"
                        "tupla-seps" "seps-tupla"
                        "adjust-buffer"
                        "rank" "unrank"
                        "comp-pos")
                      orglp-lisp-extra-function-names))

(setq-default orglp-lisp-extra-macro-names
              (append nil
                      orglp-lisp-extra-macro-names))

(setq-default org-image-actual-width nil)

(provide 'tuplas-config)
