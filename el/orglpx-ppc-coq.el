;;; orglpx-ppc-coq.el --- Pretty print LaTeX + Coq -*- lexical-binding: t -*-

(load (expand-file-name "site-lisp/PG/generic/proof-site"
                        user-emacs-directory))

(require 'proof-syntax)

(setq-default coq-use-pg nil)

(add-hook 'coq-mode-hook
          (lambda ()
            (setq orglpx-ppc-phantom-space-alist
                  '((?\( . "$($")
                    (?\[ . "$[$")))
            (setq orglpx-ppc-indent-chars
                  '(?\[ ?\( ?\#))
            (setq orglpx-ppc-space-analog-alist
                  '((init-space . "\\phantom{i..}")
                    (inner-space . "\\ ")))
            (setq orglpx-ppc-char-analog-alist
                  '((?+ . "$+$")
                    (?- . "$-$")
                    (?* . "$\\ast$")
                    (?$ . "\\$")
                    (?# . "{\\upshape\\#}")
                    (?& . "\\&")
                    (?% . "\\%")
                    (?_ . "\\raise0.28ex\\hbox{\\_}")
                    (?^ . "\\^{}")
                    (?~ . "\\~{}")
                    (?\( . "$($")
                    (?\) . "$)$")
                    (?{ . "$\\{$")
                    (?} . "$\\}$")
                    (?\[ . "$[$")
                    (?\] . "$]$")
                    (?\\ . "$\\backslash$")
                    (?| . "$\\:\\vert\\:$")
                    (?' . "{\\rmfamily\\mdseries\\upshape\\textquotesingle}")
                    (?` . "{\\rmfamily\\mdseries\\upshape\\textasciigrave}")
                    (?\" . "{\\rmfamily\\mdseries\\upshape\\textquotedbl}")
                    (?: . "{\\rmfamily\\mdseries\\upshape:}")
                    (?∀ . "$\\forall$")
                    (?∃ . "$\\exists$")
                    (?! . "{\\rmfamily\\mdseries\\upshape!}")
                    (?↔ . "$\\longleftrightarrow$")
                    (?→ . "$\\rightarrow$")
                    (?← . "$\\leftarrow$")
                    (?∧ . "$\\land$")
                    (?∨ . "$\\lor$")
                    (?≤ . "$\\leq$")
                    (?≥ . "$\\geq$")
                    (?× . "$\\times$")
                    (?📖 . "$(\\ast$")
                    (?📕 . "$\\ast)$")))
            (setq orglpx-ppc-string-replace-alist
                  '(("forall" . "∀")
                    ("exists" . "∃")
                    ("<->" . "↔")
                    ("->" . "→")
                    ("<-" . "←")
                    ("/\\" . "∧")
                    ("\\/" . "∨")
                    ("<=" . "≤")
                    (">=" . "≥")
                    ("(*" . "📖")
                    ("*)" . "📕")
                    (" * " . " × ")))
            (setq orglpx-ppc-supported-faces
                  (append '((proof-tactics-name-face . font-lock-type-face)
                            (proof-tacticals-name-face . font-lock-type-face)
                            (coq-solve-tactics-face . font-lock-type-face))
                          orglpx-ppc-supported-faces))))

(defvar coq-extra-function-names
  '("rank_tup_func" "rank_tup_is_total" "rank_tup_pos_bound"
    "unrank_tup_func" "unrank_tup_is_total" "unrank_tup_pos_bound"
    "rank_unrank_tup_are_inverses"
    "arank_tup" "aunrank_tup"
    "arank_tup_is_total" "arank_tup_pos_bound"
    "aunrank_tup_is_total" "aunrank_tup_pos_bound"
    "arank_unrank_tup_are_inverses"
    "pair_equal_spec"
    "arank_tup_eqargs" "arank_unrank_intro" "lt_strong_ind"

    "enum_rel" "enumerable_type"

    "EIN_Trivial"
    "enum_imp_nat" "imp_nat_rank" "imp_nat_unrank" "imp_nat_enumerable"

    "X" "EIL_Bypass"
    "enum_imp_loc" "EIL_Trivial" "EIL_HTrivial" "imp_loc_rank" "imp_loc_unrank" "imp_loc_enumerable"

    "AN" "AX" "AA" "AS" "AM" "EIA_N" "EIA_X" "EIA_A" "EIA_S" "EIA_M"
    "enum_imp_arith" "imp_arith_rank" "imp_arith_unrank" "imp_arith_enumerable"

    "BT" "BF" "BE" "BL" "BN" "BO" "BA" "EIB_T" "EIB_F" "EIB_E" "EIB_L" "EIB_N" "EIB_O" "EIB_A"
    "enum_imp_bool" "imp_bool_rank" "imp_bool_unrank" "imp_bool_enumerable"

    "PS" "PA" "PC" "PW" "PI" "EIP_S" "EIP_A" "EIP_C" "EIP_W" "EIP_I"
    "enum_imp_prog" "imp_prog_rank" "imp_prog_unrank" "imp_prog_enumerable"

    "le_S_n" "le_trans" "le_n" "mod" "div_exact" "mod_eq" "mod_upper_bound"))

(defvar coq-extra-keyword-names
  '("From"))

(defvar coq-extra-type-names
  '("nat" "Loc" "Arith" "Bool" "Prog"))

(font-lock-add-keywords
 'coq-mode
 `((,(concat "\\_<\\(" (regexp-opt coq-extra-function-names) "\\)\\_>")
    1 font-lock-function-name-face)
   (,(concat "\\_<\\(" (regexp-opt coq-extra-keyword-names) "\\)\\_>")
    1 font-lock-keyword-face)
   (,(concat "\\_<\\(" (regexp-opt coq-extra-type-names) "\\)\\_>")
    1 font-lock-type-face)
   (,(concat "\\("
             (rx (and (or (and symbol-start
                               (or (and (? (any "-+")) (+ digit)
                                        (? (or (and (any "eE") (? (any "-+")) (+ digit))
                                               (and "." (? (and (+ digit)
                                                                (? (and (any "eE") (? (any "-+")) (+ digit))))))
                                               (and "/" (+ digit)))))
                                   (and "." (+ digit) (? (and (any "eE") (? (any "-+")) (+ digit))))))
                          (and "#" symbol-start
                               (or (and (any "bB") (? (any "-+")) (+ (any "01")))
                                   (and (any "oO") (? (any "-+")) (+ (any "0-7")))
                                   (and (any "xX") (? (any "-+")) (+ hex-digit)))))
                      symbol-end))
             "\\)")
    1 font-lock-constant-face))
 t)

(provide 'orglpx-ppc-coq)
