;;; orglpx-src.el --- Source block management for Org mode -*- lexical-binding: t -*-

(require 'org)
(require 'org-element)
(require 'org-src)
(require 'ox)
(require 'seq)
(require 'map)
(require 'cl-lib)

(defvar orglpx-src-data nil
  "List of source data entries of source blocks.")

(defvar orglpx-src-data-index nil
  "Hash table mapping source entry ID to its source data entry.")

(defvar orglpx-src-data-beg-index nil
  "Hash table mapping source entry begin position to its source data entry.")

(defvar orglpx-src-references nil
  "List of noweb references.")

(defvar orglpx-src-reference-caption-index nil
  "Hash table mapping noweb references to their corresponding captions.")

(defvar orglpx-src-file-index nil
  "Hash table mapping file names to their corresponding source data entry.")

(defvar orglpx-src-sibling-index nil
  "Hash table mapping noweb references to a list of their corresponding source data entries.")

(defvar orglpx-src-language-index nil
  "Hash table mapping languages to a list of their corresponding source data entries ordered by ID.")

(defvar orglpx-src-refers-to-index nil
  "Hash table mapping source entry ID to a list of source entries IDs that are referenced by the corresponding block.")

(defvar orglpx-src-refered-by-index nil
  "Hash table mapping source entry ID to a list of source entries IDs that reference the corresponding block.")

(defun orglpx-src--init-state ()
  (setq orglpx-src-data nil
        orglpx-src-data-index (make-hash-table :test 'eql)
        orglpx-src-data-beg-index (make-hash-table :test 'eql)
        orglpx-src-references nil
        orglpx-src-reference-caption-index (make-hash-table :test 'equal)
        orglpx-src-file-index (make-hash-table :test 'equal)
        orglpx-src-sibling-index (make-hash-table :test 'equal)
        orglpx-src-language-index (make-hash-table :test 'equal)
        orglpx-src-refers-to-index (make-hash-table :test 'eql)
        orglpx-src-refered-by-index (make-hash-table :test 'eql)))

(defun orglpx-src-set-ids (data)
  (let ((src-id 0))
    (org-element-map data
        '(src-block inline-src-block)
      (lambda (element)
        (setq src-id (1+ src-id))
        (org-element-put-property element :src-id src-id)))))

(defun orglpx-src-parse (data)
  (orglpx-src--init-state)
  (setq orglpx-src-data
        (org-element-map data
            '(src-block inline-src-block)
          'orglpx-src--parse-block))
  (orglpx-src--after-parse))

(defun orglpx-src--parse-refs (code)
  (let ((refs nil) (start 0))
    (while (string-match (org-babel-noweb-wrap) code start)
      (push (substring code (match-beginning 1) (match-end 1)) refs)
      (setq start (match-end 0)))
    (nreverse refs)))

(defun orglpx-src--data-entry (element)
  (when (and element (memq (org-element-type element) '(src-block inline-src-block)))
    (let ((id (org-element-property :src-id element))
          (lang (org-element-property :language element))
          (beg (org-element-property :begin element))
          (end (org-element-property :end element))
          (post-blank (org-element-property :post-blank element))
          (code (org-element-property :value element))
          (caption (org-export-get-caption element))
          (babel (org-babel-get-src-block-info 'light element)))
      (let ((args (nth 2 babel))
            (name (nth 4 babel)))
        (let ((noweb-ref (cdr (assq :noweb-ref args)))
              (tangle (cdr (assq :tangle args)))
              (package (cdr (assq :package args))))
          (when (member tangle '("no" "yes"))
            (setq tangle nil))
          (let ((ref (or name noweb-ref tangle))
                (file tangle))
            (list :id id :beg beg :end (- end post-blank)
                  :inlinep (eq (org-element-type element) 'inline-src-block)
                  :ref ref :file file :package package
                  :lang lang :code code
                  :caption caption)))))))

(defun orglpx-src--parse-block (element)
  (let ((entry (orglpx-src--data-entry element)))
    (let ((id (plist-get entry :id))
          (beg (plist-get entry :beg))
          (ref (plist-get entry :ref))
          (file (plist-get entry :file))
          (lang (plist-get entry :lang))
          (caption (plist-get entry :caption)))
      (puthash id entry orglpx-src-data-index)
      (puthash beg entry orglpx-src-data-beg-index)
      (when ref
        (push ref orglpx-src-references))
      (when file
        (when (gethash file orglpx-src-file-index)
          (warn "Duplicate source file %s at point %s" file beg))
        (puthash file entry orglpx-src-file-index))
      (when caption
        (unless ref
          (warn "Source block with caption cannot be referenced at %s" beg))
        (when (gethash ref orglpx-src-reference-caption-index)
          (warn "Duplicate caption for reference %s at point %s" ref beg))
        (puthash ref caption orglpx-src-reference-caption-index))
      (push entry (gethash ref orglpx-src-sibling-index))
      (push entry (gethash lang orglpx-src-language-index))
      (when (and file (not (equal file ref)))
        (warn "Source block contains a reference (%s) and a tangle destination (%s) at point %s"
              ref file beg))
      entry)))

(defun orglpx-src--after-parse ()
  ;; Sort references
  (setq orglpx-src-references (delete-dups (nreverse orglpx-src-references)))
  ;; Sort siblings
  (seq-do (lambda (ref)
            (puthash ref
                     (nreverse (gethash ref orglpx-src-sibling-index))
                     orglpx-src-sibling-index))
          orglpx-src-references)
  (puthash nil
           (nreverse (gethash nil orglpx-src-sibling-index))
           orglpx-src-sibling-index)
  ;; Sort code references
  (dolist (entry orglpx-src-data)
    (let ((id (plist-get entry :id))
          (ref-targets (orglpx-src--parse-refs (plist-get entry :code))))
      (dolist (ref-target ref-targets)
        (dolist (target-entry (gethash ref-target orglpx-src-sibling-index))
          (let ((target-id (plist-get target-entry :id)))
            (push target-id (gethash id orglpx-src-refers-to-index))
            (push id (gethash target-id orglpx-src-refered-by-index)))))))
  (dolist (entry orglpx-src-data)
    (let* ((id (plist-get entry :id))
           (refs-to (gethash id orglpx-src-refers-to-index))
           (refd-by (gethash id orglpx-src-refered-by-index)))
      (when refs-to
        (puthash id (sort (delete-dups refs-to) '<) orglpx-src-refers-to-index))
      (when refd-by
        (puthash id (sort (delete-dups refd-by) '<) orglpx-src-refered-by-index)))))

(provide 'orglpx-src)
