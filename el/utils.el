(defun add-library-load-path (libname)
  (let ((matches nil))
    (dolist (path (directory-files (expand-file-name "~/.emacs.d/elpa/")))
      (when (string-match (concat "^" libname "-[0-9]+") path)
        (push path matches)))
    (let ((match (car (last (sort matches 'string<)))))
      (if match
          (add-to-list 'load-path (expand-file-name (concat "~/.emacs.d/elpa/" match "/")))
        (error "Missing library %s" libname)))))
