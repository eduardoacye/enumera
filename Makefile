TEX_AUXTRASH:=*.aux *.toc *.bbl *.bcf *.blg *.fdb_latexmk *.fls *.log *.out *.xml *.lpsec *.sty

METAPOST_SRC:=$(wildcard fig/*.mp)

GNUPLOT_SRC:=$(wildcard plot/*.gnu)
GNUPLOT_TEX:=$(patsubst %.gnu,%.tex,$(GNUPLOT_SRC))
GNUPLOT_PDF:=$(patsubst %.gnu,%.pdf,$(GNUPLOT_SRC))

IMAGE_PNG:=$(wildcard img/*.png)

TESIS_SRC:=$(wildcard tesis/*.tex) tesis/bibliografia.bib

ORG_DEPS:=setup.org el/orglpx-litprog-setup.el

all: tesis.pdf tuplas.pdf enumera.pdf imp.pdf coq-imp.pdf

coq-check: coq-imp.pdf
	@cd src/vimp && coq_makefile -f _CoqProject -o Makefile
	@cd src/vimp && make clean
	@cd src/vimp && make

tesis.pdf: tesis.tex $(TESIS_SRC) $(GNUPLOT_PDF) $(GNUPLOT_TEX) $(METAPOST_SRC) $(IMAGE_PNG)
	@latexmk -lualatex \
		 -auxdir=./tesis \
		 -outdir=./tesis \
		 -g \
		 tesis.tex
	@mv tesis/tesis.pdf tesis.pdf

tuplas.pdf: tuplas/tuplas.tex
	@cd tuplas && latexmk -lualatex -g tuplas.tex
	@mv tuplas/tuplas.pdf tuplas.pdf
	@emacs  --batch \
		--eval "(require 'org)" \
		--eval '(org-babel-tangle-file "tuplas.org")'

enumera.pdf: enumera/enumera.tex
	@cd enumera && latexmk -lualatex -g enumera.tex
	@mv enumera/enumera.pdf enumera.pdf
	@emacs  --batch \
		--eval "(require 'org)" \
		--eval '(org-babel-tangle-file "enumera.org")'

imp.pdf: imp/imp.tex
	@cd imp && latexmk -lualatex -g imp.tex
	@mv imp/imp.pdf imp.pdf
	@emacs  --batch \
		--eval "(require 'org)" \
		--eval '(org-babel-tangle-file "imp.org")'

coq-imp.pdf: coq-imp/coq-imp.tex
	@cd coq-imp && latexmk -lualatex -g coq-imp.tex
	@mv coq-imp/coq-imp.pdf coq-imp.pdf
	@emacs  --batch \
		--eval "(require 'org)" \
		--eval '(org-babel-tangle-file "coq-imp.org")'
	@cd src/vimp && coq_makefile -f _CoqProject -o Makefile
	@cd src/vimp && make clean
	@cd src/vimp && make

tuplas/tuplas.tex: tuplas.org $(ORG_DEPS) $(GNUPLOT_PDF) $(GNUPLOT_TEX) $(METAPOST_SRC)
	@mkdir -p tuplas
	@cp tex/orglpxmac.sty tuplas/orglpxmac.sty
	@emacs  tuplas.org \
		--batch \
		--funcall toggle-debug-on-error \
		--eval "(add-to-list 'load-path (expand-file-name \"./el/\"))" \
		--eval "(require 'orglpx-litprog-setup)" \
		--eval "(orglpx-export-to-latex)"

enumera/enumera.tex: enumera.org $(ORG_DEPS) $(GNUPLOT_PDF) $(GNUPLOT_TEX) $(METAPOST_SRC)
	@mkdir -p enumera
	@cp tex/orglpxmac.sty enumera/orglpxmac.sty
	@emacs  enumera.org \
		--batch \
		--funcall toggle-debug-on-error \
		--eval "(add-to-list 'load-path (expand-file-name \"./el/\"))" \
		--eval "(require 'orglpx-litprog-setup)" \
		--eval "(orglpx-export-to-latex)"

imp/imp.tex: imp.org $(ORG_DEPS) $(GNUPLOT_PDF) $(GNUPLOT_TEX) $(METAPOST_SRC)
	@mkdir -p imp
	@cp tex/orglpxmac.sty imp/orglpxmac.sty
	@emacs  imp.org \
		--batch \
		--funcall toggle-debug-on-error \
		--eval "(add-to-list 'load-path (expand-file-name \"./el/\"))" \
		--eval "(require 'orglpx-litprog-setup)" \
		--eval "(orglpx-export-to-latex)"

coq-imp/coq-imp.tex: coq-imp.org $(ORG_DEPS) $(GNUPLOT_PDF) $(GNUPLOT_TEX) $(METAPOST_SRC)
	@mkdir -p coq-imp
	@cp tex/orglpxmac.sty coq-imp/orglpxmac.sty
	@emacs  coq-imp.org \
		--batch \
		--funcall toggle-debug-on-error \
		--eval "(add-to-list 'load-path (expand-file-name \"./el/\"))" \
		--eval "(require 'orglpx-litprog-setup)" \
		--eval "(require 'orglpx-ppc-coq)" \
		--eval "(orglpx-export-to-latex)"

plot/%.pdf plot/%.tex: plot/%.gnu
	@cd plot && gnuplot ../$<

clean:
	@rm -rf $(GNUPLOT_TEX) $(GNUPLOT_PDF)
	@cd tesis && rm -rf $(TEX_AUXTRASH)
	@rm -rf tuplas
	@rm -rf enumera
	@rm -rf coq-imp
	@rm -rf imp
