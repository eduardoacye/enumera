Definition rank_tup_func := nat -> nat -> nat.

Definition rank_tup_is_total (f : rank_tup_func) :=
  forall a b, exists n, f a b = n.

Definition rank_tup_pos_bound (f : rank_tup_func) :=
  forall a b n, f a b = n -> a <= n /\ b <= n.
Definition unrank_tup_func := nat -> (nat * nat).

Definition unrank_tup_is_total (g : unrank_tup_func) :=
  forall n, exists a b, g n = (a, b).

Definition unrank_tup_pos_bound (g : unrank_tup_func) :=
  forall n a b, g n = (a, b) -> a <= n /\ b <= n.
Definition rank_unrank_tup_are_inverses
           (f : rank_tup_func)
           (g : unrank_tup_func) :=
  forall a b n, f a b = n <-> g n = (a, b).
Parameter (arank_tup : rank_tup_func)
          (aunrank_tup : unrank_tup_func).

Axiom arank_tup_is_total : (rank_tup_is_total arank_tup).
Axiom arank_tup_pos_bound : (rank_tup_pos_bound arank_tup).
Axiom aunrank_tup_is_total : (unrank_tup_is_total aunrank_tup).
Axiom aunrank_tup_pos_bound : (unrank_tup_pos_bound aunrank_tup).
Axiom arank_unrank_tup_are_inverses :
  (rank_unrank_tup_are_inverses arank_tup aunrank_tup).
