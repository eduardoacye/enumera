From Coq Require Import Lia.
From Coq Require Import Arith.Arith.

From Enums Require Import Enum_Defs.
From Enums Require Import Enum_Props.
From Enums Require Import IMP_Defs.
From Enums Require Import Mod_Props.

Lemma imp_nat_rank : forall n, exists! k, enum_imp_nat n k.
Proof.
  intro. exists n. split.
  - apply EIN_Trivial.
  - intro k. intro H. inversion H. reflexivity.
Qed.
Lemma imp_nat_unrank : forall k, exists! n, enum_imp_nat n k.
Proof.
  intro. exists k. split.
  - apply EIN_Trivial.
  - intro n. intro H. inversion H. reflexivity.
Qed.
Theorem imp_nat_enumerable : (enumerable_type enum_imp_nat).
Proof. split. apply imp_nat_rank. apply imp_nat_unrank. Qed.
Lemma EIL_Trivial : forall n, enum_imp_loc (X n) n.
Proof. intro. apply EIL_Bypass. apply EIN_Trivial. Qed.

Lemma EIL_HTrivial : forall n1 n2, enum_imp_loc (X n1) n2 -> n1 = n2.
Proof. intros n1 n2 H. inversion H. inversion H1. reflexivity. Qed.
Lemma imp_loc_rank : forall x, exists! k, enum_imp_loc x k.
Proof.
  intro x. destruct x. exists n.
  split.
  - apply EIL_Trivial.
  - apply EIL_HTrivial.
Qed.
Lemma imp_loc_unrank : forall k, exists! x, enum_imp_loc x k.
Proof.
  intro k. exists (X k).
  split.
  - apply EIL_Trivial.
  - intros [n] H. apply EIL_HTrivial in H. subst. reflexivity.
Qed.
Theorem imp_loc_enumerable : (enumerable_type enum_imp_loc).
Proof. split. apply imp_loc_rank. apply imp_loc_unrank. Qed.
Lemma imp_arith_rank : forall a, exists! k, enum_imp_arith a k.
Proof.
  intros a.
  induction a.
  - exists (5 * n).
    split.
    + apply EIA_N. apply EIN_Trivial.
    + intros n' H. inversion H. inversion H1. reflexivity.
  - destruct x.
    exists (S (5 * n)).
    split.
    + apply EIA_X. apply EIL_Trivial.
    + intros n' H. inversion H.
      apply EIL_HTrivial in H1. rewrite H1.
      reflexivity.
  - destruct IHa1 as [k1 [Hk1ex Hk1un]].
    destruct IHa2 as [k2 [Hk2ex Hk2un]].
    destruct (arank_tup_is_total k1 k2) as [k' HRank].
    exists (S (S (5 * k'))).
    split.
    + apply (EIA_A a1 a2 k1 k2 Hk1ex Hk2ex k' HRank).
    + intros k H. inversion H.
      destruct (Hk1un k0 H0).
      destruct (Hk2un k3 H3).
      rewrite H5 in HRank. rewrite HRank. reflexivity.
  - destruct IHa1 as [k1 [Hk1ex Hk1un]].
    destruct IHa2 as [k2 [Hk2ex Hk2un]].
    destruct (arank_tup_is_total k1 k2) as [k' HRank].
    exists (S (S (S (5 * k')))).
    split.
    + apply (EIA_S a1 a2 k1 k2 Hk1ex Hk2ex k' HRank).
    + intros k H. inversion H.
      destruct (Hk1un k0 H0).
      destruct (Hk2un k3 H3).
      rewrite H5 in HRank. rewrite HRank. reflexivity.
  - destruct IHa1 as [k1 [Hk1ex Hk1un]].
    destruct IHa2 as [k2 [Hk2ex Hk2un]].
    destruct (arank_tup_is_total k1 k2) as [k' HRank].
    exists (S (S (S (S (5 * k'))))).
    split.
    + apply (EIA_M a1 a2 k1 k2 Hk1ex Hk2ex k' HRank).
    + intros k H. inversion H.
      destruct (Hk1un k0 H0).
      destruct (Hk2un k3 H3).
      rewrite H5 in HRank. rewrite HRank. reflexivity.
Qed.
Lemma imp_arith_unrank : forall k, exists! a, enum_imp_arith a k.
Proof.
  intro.
  induction k using lt_strong_ind.
  rename H into IH.
  destruct (nat_mult5 k) as
      [k' [Ek0 | [Ek1 | [Ek2 | [Ek3 | Ek4]]]]].
  - rewrite Ek0 in *.
    exists (AN k').
    split.
    + apply EIA_N. apply EIN_Trivial.
    + intros a H. inversion H; try(contradict H0; lia).
      inversion H2. assert (k0 = k'). lia. subst. reflexivity.
  - rewrite Ek1 in *.
    exists (AX (X k')).
    split.
    + apply EIA_X. apply EIL_Trivial.
    + intros a H. inversion H; try(contradict H0; lia).
      inversion H2. inversion H3. assert (k0 = k'). lia. subst. reflexivity.
  - rewrite Ek2 in *.
    destruct (arank_unrank_intro k') as 
        [k1 [k2 [HUnrank HRank]]].
    destruct (aunrank_tup_pos_bound k' k1 k2 HUnrank) as
        [Bound1 Bound2].
    assert (k1 < S (S (5 * k'))) as Bound1'; try(lia).
    assert (k2 < S (S (5 * k'))) as Bound2'; try(lia).
    destruct (IH k1 Bound1') as [a1 [Ha1ex Ha1un]].
    destruct (IH k2 Bound2') as [a2 [Ha2ex Ha2un]].
    exists (AA a1 a2).
    split.
    + apply (EIA_A a1 a2 k1 k2 Ha1ex Ha2ex k' HRank).
    + intros a H. inversion H; try(contradict H0; lia).
      assert (k4 = k'); try(lia).
      rewrite H5 in *.
      destruct (arank_tup_eqargs k1 k2 k0 k3 k' HRank H4).
      rewrite <- H6 in *. rewrite <- H7 in *.
      rewrite <- (Ha1un a0 H2). rewrite <- (Ha2un a3 H3).
      reflexivity.
  - rewrite Ek3 in *.
    destruct (arank_unrank_intro k') as 
        [k1 [k2 [HUnrank HRank]]].
    destruct (aunrank_tup_pos_bound k' k1 k2 HUnrank) as 
        [Bound1 Bound2].
    assert (k1 < S (S (S (5 * k')))) as Bound1'; try(lia).
    assert (k2 < S (S (S (5 * k')))) as Bound2'; try(lia).
    destruct (IH k1 Bound1') as [a1 [Ha1ex Ha1un]].
    destruct (IH k2 Bound2') as [a2 [Ha2ex Ha2un]].
    exists (AS a1 a2).
    split.
    + apply (EIA_S a1 a2 k1 k2 Ha1ex Ha2ex k' HRank).
    + intros a H. inversion H; try(contradict H0; lia).
      assert (k4 = k'); try(lia).
      rewrite H5 in *.
      destruct (arank_tup_eqargs k1 k2 k0 k3 k' HRank H4).
      rewrite <- H6 in *. rewrite <- H7 in *.
      rewrite <- (Ha1un a0 H2). rewrite <- (Ha2un a3 H3).
      reflexivity.
  - rewrite Ek4 in *.
    destruct (arank_unrank_intro k') as
        [k1 [k2 [HUnrank HRank]]].
    destruct (aunrank_tup_pos_bound k' k1 k2 HUnrank) as 
        [Bound1 Bound2].
    assert (k1 < S (S (S (S (5 * k'))))) as Bound1'; try(lia).
    assert (k2 < S (S (S (S (5 * k'))))) as Bound2'; try(lia).
    destruct (IH k1 Bound1') as [a1 [Ha1ex Ha1un]].
    destruct (IH k2 Bound2') as [a2 [Ha2ex Ha2un]].
    exists (AM a1 a2).
    split.
    + apply (EIA_M a1 a2 k1 k2 Ha1ex Ha2ex k' HRank).
    + intros a H. inversion H; try(contradict H0; lia).
      assert (k4 = k'); try(lia).
      rewrite H5 in *.
      destruct (arank_tup_eqargs k1 k2 k0 k3 k' HRank H4).
      rewrite <- H6 in *. rewrite <- H7 in *.
      rewrite <- (Ha1un a0 H2). rewrite <- (Ha2un a3 H3).
      reflexivity.
Qed.
Theorem imp_arith_enumerable : (enumerable_type enum_imp_arith).
Proof. split. apply imp_arith_rank. apply imp_arith_unrank. Qed.
Lemma imp_bool_rank : forall b, exists! k, enum_imp_bool b k.
Proof.
  intros b. induction b.
  - exists 0. split.
    + apply EIB_T.
    + intros k H. inversion H. reflexivity.
  - exists 1. split.
    + apply EIB_F.
    + intros k H. inversion H. reflexivity.
  - destruct (imp_arith_rank a1) as [k1 [Hk1ex Hk1un]].
    destruct (imp_arith_rank a2) as [k2 [Hk2ex Hk2un]].
    destruct (arank_tup_is_total k1 k2) as [k' HRank].
    exists (S (S (5 * k'))).
    split.
    + apply (EIB_E a1 a2 k1 k2 Hk1ex Hk2ex k' HRank).
    + intros k H. inversion H.
      destruct (Hk1un k0 H0).
      destruct (Hk2un k3 H3).
      rewrite H5 in HRank. rewrite HRank. reflexivity.
  - destruct (imp_arith_rank a1) as [k1 [Hk1ex Hk1un]].
    destruct (imp_arith_rank a2) as [k2 [Hk2ex Hk2un]].
    destruct (arank_tup_is_total k1 k2) as [k' HRank].
    exists (S (S (S (5 * k')))).
    split.
    + apply (EIB_L a1 a2 k1 k2 Hk1ex Hk2ex k' HRank).
    + intros k H. inversion H.
      destruct (Hk1un k0 H0).
      destruct (Hk2un k3 H3).
      rewrite H5 in HRank. rewrite HRank. reflexivity.
  - destruct IHb1 as [k1 [IHb1ex IHb1un]].
    destruct IHb2 as [k2 [IHb2ex IHb2un]].
    destruct (arank_tup_is_total k1 k2) as [k' HRank].
    exists (S (S (S (S (5 * k'))))).
    split.
    + apply (EIB_A b1 b2 k1 k2 IHb1ex IHb2ex k' HRank).
    + intros k H. inversion H.
      destruct (IHb1un k0 H0).
      destruct (IHb2un k3 H3).
      rewrite H5 in HRank. rewrite HRank. reflexivity.
  - destruct IHb1 as [k1 [IHb1ex IHb1un]].
    destruct IHb2 as [k2 [IHb2ex IHb2un]].
    destruct (arank_tup_is_total k1 k2) as [k' HRank].
    exists (S (S (S (S (S (5 * k')))))).
    split.
    + apply (EIB_O b1 b2 k1 k2 IHb1ex IHb2ex k' HRank).
    + intros k H. inversion H.
      destruct (IHb1un k0 H0).
      destruct (IHb2un k3 H3).
      rewrite H5 in HRank. rewrite HRank. reflexivity.
  - destruct IHb as [k' [IHbexists IHbunique]].
    exists (S (S (S (S (S (S (5 * k'))))))).
    split.
    + apply EIB_N. assumption.
    + intros k H. inversion H.
      rewrite <- (IHbunique k0 H1).
      reflexivity.
Qed.
Lemma imp_bool_unrank : forall k, exists! b, enum_imp_bool b k.
Proof.
  intro.
  induction k using lt_strong_ind.
  rename H into IH.
  destruct k.
  { exists BT. split.
    - apply EIB_T.
    - intros b H. inversion H. reflexivity. }
  destruct k.
  { exists BF. split.
    - apply EIB_F.
    - intros b H. inversion H. reflexivity. }
  destruct (nat_mult5 k) as
      [k' [Ek0 | [Ek1 | [Ek2 | [Ek3 | Ek4]]]]].
  - rewrite Ek0 in *.
    destruct (arank_unrank_intro k') as [k1 [k2 [HUnrank HRank]]].
    destruct (imp_arith_unrank k1) as [a1 [Ha1ex Ha1un]].
    destruct (imp_arith_unrank k2) as [a2 [Ha2ex Ha2un]].
    exists (BE a1 a2).
    split.
    + apply (EIB_E a1 a2 k1 k2 Ha1ex Ha2ex k' HRank).
    + intros b H. inversion H; try(contradict H0; lia).
      assert (k4 = k'); try(lia).
      rewrite H5 in *.
      destruct (arank_tup_eqargs k1 k2 k0 k3 k' HRank H4).
      rewrite <- H6 in *. rewrite <- H7 in *.
      rewrite <- (Ha1un a0 H2). rewrite <- (Ha2un a3 H3).
      reflexivity.
  - rewrite Ek1 in *.
    destruct (arank_unrank_intro k') as [k1 [k2 [HUnrank HRank]]].
    destruct (imp_arith_unrank k1) as [a1 [Ha1ex Ha1un]].
    destruct (imp_arith_unrank k2) as [a2 [Ha2ex Ha2un]].
    exists (BL a1 a2).
    split.
    + apply (EIB_L a1 a2 k1 k2 Ha1ex Ha2ex k' HRank).
    + intros b H. inversion H; try(contradict H0; lia).
      assert (k4 = k'); try(lia).
      rewrite H5 in *.
      destruct (arank_tup_eqargs k1 k2 k0 k3 k' HRank H4).
      rewrite <- H6 in *. rewrite <- H7 in *.
      rewrite <- (Ha1un a0 H2). rewrite <- (Ha2un a3 H3).
      reflexivity.
  - rewrite Ek2 in *.
    destruct (arank_unrank_intro k') as 
        [k1 [k2 [HUnrank HRank]]].
    destruct (aunrank_tup_pos_bound k' k1 k2 HUnrank) as
        [Bound1 Bound2].
    assert (k1 < S (S (S (S (5 * k'))))) as Bound1'; try(lia).
    assert (k2 < S (S (S (S (5 * k'))))) as Bound2'; try(lia).
    destruct (IH k1 Bound1') as [b1 [Hb1ex Hb1un]].
    destruct (IH k2 Bound2') as [b2 [Hb2ex Hb2un]].
    exists (BA b1 b2).
    split.
    + apply (EIB_A b1 b2 k1 k2 Hb1ex Hb2ex k' HRank).
    + intros b H. inversion H; try(contradict H0; lia).
      assert (k4 = k'); try(lia).
      rewrite H5 in *.
      destruct (arank_tup_eqargs k1 k2 k0 k3 k' HRank H4).
      rewrite <- H6 in *. rewrite <- H7 in *.
      rewrite <- (Hb1un b0 H2). rewrite <- (Hb2un b3 H3).
      reflexivity.
  - rewrite Ek3 in *.
    destruct (arank_unrank_intro k') as 
        [k1 [k2 [HUnrank HRank]]].
    destruct (aunrank_tup_pos_bound k' k1 k2 HUnrank) as 
        [Bound1 Bound2].
    assert (k1 < S (S (S (S (S (5 * k')))))) as Bound1'; try(lia).
    assert (k2 < S (S (S (S (S (5 * k')))))) as Bound2'; try(lia).
    destruct (IH k1 Bound1') as [b1 [Hb1ex Hb1un]].
    destruct (IH k2 Bound2') as [b2 [Hb2ex Hb2un]].
    exists (BO b1 b2).
    split.
    + apply (EIB_O b1 b2 k1 k2 Hb1ex Hb2ex k' HRank).
    + intros b H. inversion H; try(contradict H0; lia).
      assert (k4 = k'); try(lia).
      rewrite H5 in *.
      destruct (arank_tup_eqargs k1 k2 k0 k3 k' HRank H4).
      rewrite <- H6 in *. rewrite <- H7 in *.
      rewrite <- (Hb1un b0 H2). rewrite <- (Hb2un b3 H3).
      reflexivity.
  - rewrite Ek4 in *.
    assert (k' < S (S (S (S (S (S (5 * k'))))))) as Bound; try(lia).
    destruct (IH k' Bound) as [b1 [Hb1ex Hb1un]].
    exists (BN b1).
    split.
    + apply EIB_N. assumption.
    + intros b H. inversion H; try(contradict H0; lia).
      assert (k0 = k'); try(lia).
      rewrite H3 in *.
      rewrite (Hb1un b0 H2).
      reflexivity.
Qed.
Theorem imp_bool_enumerable : (enumerable_type enum_imp_bool).
Proof. split. apply imp_bool_rank. apply imp_bool_unrank. Qed.
Lemma imp_prog_rank : forall p, exists! k, enum_imp_prog p k.
Proof.
  intros p. induction p.
  - exists 0. split.
    + apply EIP_S.
    + intros k H. inversion H. reflexivity.
  - destruct (imp_loc_rank x) as [k1 [Hk1ex Hk1un]].
    destruct (imp_arith_rank a) as [k2 [Hk2ex Hk2un]].
    destruct (arank_tup_is_total k1 k2) as [k' HRank].
    exists (S (4 * k')).
    split.
    + apply (EIP_A x a k1 k2 Hk1ex Hk2ex k' HRank).
    + intros k H. inversion H.
      destruct (Hk1un k0 H0).
      destruct (Hk2un k3 H3).
      rewrite H5 in HRank. rewrite HRank. reflexivity.
  - destruct (imp_bool_rank b) as [k1 [Hk1ex Hk1un]].
    destruct IHp as [k2 [Hk2ex Hk2un]].
    destruct (arank_tup_is_total k1 k2) as [k' HRank].
    exists (S (S (4 * k'))).
    split.
    + apply (EIP_W b p k1 k2 Hk1ex Hk2ex k' HRank).
    + intros k H. inversion H.
      destruct (Hk1un k0 H0).
      destruct (Hk2un k3 H3).
      rewrite H5 in HRank. rewrite HRank. reflexivity.
  - destruct IHp1 as [k1 [Hk1ex Hk1un]].
    destruct IHp2 as [k2 [Hk2ex Hk2un]].
    destruct (arank_tup_is_total k1 k2) as [k' HRank].
    exists (S (S (S (4 * k')))).
    split.
    + apply (EIP_C p1 p2 k1 k2 Hk1ex Hk2ex k' HRank).
    + intros k H. inversion H.
      destruct (Hk1un k0 H0).
      destruct (Hk2un k3 H3).
      rewrite H5 in HRank. rewrite HRank. reflexivity.
  - destruct (imp_bool_rank b) as [k1 [Hk1ex Hk1un]].
    destruct IHp1 as [k2 [Hk2ex Hk2un]].
    destruct IHp2 as [k3 [Hk3ex Hk3un]].
    destruct (arank_tup_is_total k2 k3) as [kr HR1].
    destruct (arank_tup_is_total k1 kr) as [k' HR2].
    exists (S (S (S (S (4 * k'))))).
    split.
    + apply (EIP_I b p1 p2 k1 k2 k3 Hk1ex Hk2ex Hk3ex k' kr HR1 HR2).
    + intros k H. inversion H.
      destruct (Hk1un k0 H0).
      destruct (Hk2un k4 H4).
      destruct (Hk3un k5 H5).
      rewrite H7 in HR1.
      rewrite HR1 in H8.
      rewrite H8 in HR2.
      rewrite HR2.
      reflexivity.
Qed.
Lemma imp_prog_unrank : forall k, exists! p, enum_imp_prog p k.
Proof.
  intro.
  induction k using lt_strong_ind.
  rename H into IH.
  destruct k.
  { exists PS. split.
    - apply EIP_S.
    - intros p H. inversion H. reflexivity. }
  destruct (nat_mult4 k) as [k' [Ek0 | [Ek1 | [Ek2 | Ek3]]]].
  - rewrite Ek0 in *.
    destruct (arank_unrank_intro k') as [k1 [k2 [HUnrank HRank]]].
    destruct (imp_loc_unrank k1) as [x [Hxexists Hxunique]].
    destruct (imp_arith_unrank k2) as [a [Haexists Haunique]].
    exists (PA x a).
    split.
    + apply (EIP_A x a k1 k2 Hxexists Haexists k' HRank).
    + intros p H. inversion H; try(contradict H0; lia).
      assert (k4 = k'); try(lia).
      rewrite H5 in *.
      destruct (arank_tup_eqargs k1 k2 k0 k3 k' HRank H4).
      rewrite <- H6 in *. rewrite <- H7 in *.
      rewrite <- (Hxunique x0 H2). rewrite <- (Haunique a0 H3).
      reflexivity.
  - rewrite Ek1 in *.
    destruct (arank_unrank_intro k') as
        [k1 [k2 [HUnrank HRank]]].
    destruct (aunrank_tup_pos_bound k' k1 k2 HUnrank) as
        [Bound1 Bound2].
    destruct (imp_bool_unrank k1) as [b [Hbexists Hbunique]].
    assert (k2 < S (S (4 * k'))) as Bound2'; try(lia).
    destruct (IH k2 Bound2') as [p1 [Hp1ex Hp1un]].
    exists (PW b p1).
    split.
    + apply (EIP_W b p1 k1 k2 Hbexists Hp1ex k' HRank).
    + intros p H. inversion H; try(contradict H0; lia).
      assert (k4 = k'); try(lia).
      rewrite H5 in *.
      destruct (arank_tup_eqargs k1 k2 k0 k3 k' HRank H4).
      rewrite <- H6 in *. rewrite <- H7 in *.
      rewrite <- (Hbunique b0 H2). rewrite <- (Hp1un p0 H3).
      reflexivity.
  - rewrite Ek2 in *.
    destruct (arank_unrank_intro k') as
        [k1 [k2 [HUnrank HRank]]].
    destruct (aunrank_tup_pos_bound k' k1 k2 HUnrank) as
        [Bound1 Bound2].
    assert (k1 < S (S (S (4 * k')))) as Bound1'; try(lia).
    assert (k2 < S (S (S (4 * k')))) as Bound2'; try(lia).
    destruct (IH k1 Bound1') as [p1 [Hp1ex Hp1un]].
    destruct (IH k2 Bound2') as [p2 [Hp2ex Hp2un]].
    exists (PC p1 p2).
    split.
    + apply (EIP_C p1 p2 k1 k2 Hp1ex Hp2ex k' HRank).
    + intros p H. inversion H; try(contradict H0; lia).
      assert (k4 = k'); try(lia).
      rewrite H5 in *.
      destruct (arank_tup_eqargs k1 k2 k0 k3 k' HRank H4).
      rewrite <- H6 in *. rewrite <- H7 in *.
      rewrite <- (Hp1un p0 H2). rewrite <- (Hp2un p3 H3).
      reflexivity.
  - rewrite Ek3 in *.
    destruct (arank_unrank_intro k') as 
        [k1 [kr [HUnrank1 HR1]]].
    destruct (arank_unrank_intro kr) as
        [k2 [k3 [HUnrank2 HR2]]].
    destruct (aunrank_tup_pos_bound k' k1 kr HUnrank1) as
        [Bound1 Boundr].
    destruct (aunrank_tup_pos_bound kr k2 k3 HUnrank2) as
        [Bound2 Bound3].
    destruct (imp_bool_unrank k1) as [b [Hbexists Hbunique]].
    assert (k2 < S (S (S (S (4 * k'))))) as Bound2'; try(lia).
    assert (k3 < S (S (S (S (4 * k'))))) as Bound3'; try(lia).
    destruct (IH k2 Bound2') as [p1 [Hp1ex Hp1un]].
    destruct (IH k3 Bound3') as [p2 [Hp2ex Hp2un]].
    exists (PI b p1 p2).
    split.
    + apply (EIP_I b p1 p2 k1 k2 k3 Hbexists Hp1ex Hp2ex k' kr HR2 HR1).
    + intros p H. inversion H; try(contradict H0; lia).
      assert (k6 = k'); try(lia).
      rewrite H7 in *.
      destruct (arank_tup_eqargs k1 kr k0 k'0 k' HR1 H6).
      rewrite <- H8 in *. rewrite <- H9 in *.
      destruct (arank_tup_eqargs k2 k3 k4 k5 kr HR2 H5).
      rewrite <- H10 in *. rewrite <- H11 in *.
      rewrite <- (Hbunique b0 H2).
      rewrite <- (Hp1un p0 H3).
      rewrite <- (Hp2un p3 H4).
      reflexivity.
Qed.
Theorem imp_prog_enumerable : (enumerable_type enum_imp_prog).
Proof. split. apply imp_prog_rank. apply imp_prog_unrank. Qed.
