From Coq Require Import Setoid.
From Coq Require Import Arith.Arith.

From Enums Require Import Enum_Defs.

Lemma arank_tup_eqargs : forall a b c d n,
    (arank_tup a b = n) ->
    (arank_tup c d = n) ->
    (a = c) /\ (b = d).
Proof.
  do 5 intro.
  intros H1 H2.
  apply (arank_unrank_tup_are_inverses a b n) in H1.
  apply (arank_unrank_tup_are_inverses c d n) in H2.
  rewrite H1 in H2.
  apply pair_equal_spec in H2.
  apply H2.
Qed.
Lemma arank_unrank_intro : forall k, exists k1 k2,
      aunrank_tup k = (k1, k2) /\ arank_tup k1 k2 = k.
Proof.
  intros.
  destruct (aunrank_tup k) as (k1, k2) eqn:Unrank.
  exists k1. exists k2. split.
  - reflexivity.
  - rewrite (arank_unrank_tup_are_inverses k1 k2 k). assumption.
Qed.
Lemma lt_strong_ind :
  forall n (P : nat -> Prop),
    (forall n, (forall m, m < n -> P m) -> P n) -> P n.
Proof.
  intros.
  assert (forall k, k <= n -> P k).
  { induction n as [| n IHn].
    - intros. apply H. intros.
      inversion H0. rewrite H2 in H1. inversion H1.
    - intros. apply H. intros.
      apply IHn. unfold lt in H1.
      apply le_S_n.
      apply (le_trans (S m) k (S n) H1 H0). }
  apply H0. apply le_n.
Qed.
