From Coq Require Import Arith.Arith.
From Coq Require Import Lia.
Import Nat.

Lemma nat_mult4 : forall n, exists n',
      (n = 4 * n') \/ (n = S (4 * n')) \/
      (n = S (S (4 * n'))) \/ (n = S (S (S (4 * n')))).
Proof.
  intros. exists (n / 4).
  destruct (n mod 4) eqn:H.
  (* n mod 4 = 0 *)
  apply <- (div_exact n 4) in H. { left. apply H. } lia.
  (* n mod 4 = 1 *)
  destruct n0.
  rewrite (mod_eq n 4) in H. lia. lia.
  (* n mod 4 = 2 *)
  destruct n0.
  rewrite (mod_eq n 4) in H. lia. lia.
  (* n mod 4 = 3 *)
  destruct n0.
  rewrite (mod_eq n 4) in H. lia. lia.
  (* n mod 4 < 4 *)
  assert (Hf := mod_upper_bound n 4). lia.
Qed.
Lemma nat_mult5 : forall n, exists n',
      (n = 5 * n') \/ (n = S (5 * n')) \/
      (n = S (S (5 * n'))) \/ (n = S (S (S (5 * n')))) \/
      (n = S (S (S (S (5 * n'))))).
Proof.
  intros. exists (n / 5).
  destruct (n mod 5) eqn:H.
  (* n mod 5 = 0 *)
  apply <- (div_exact n 5) in H. { left. apply H. } lia.
  (* n mod 5 = 1 *)
  destruct n0.
  rewrite (mod_eq n 5) in H. lia. lia.
  (* n mod 5 = 2 *)
  destruct n0.
  rewrite (mod_eq n 5) in H. lia. lia.
  (* n mod 5 = 3 *)
  destruct n0.
  rewrite (mod_eq n 5) in H. lia. lia.
  (* n mod 5 = 4 *)
  destruct n0.
  rewrite (mod_eq n 5) in H. lia. lia.
  (* n mod 5 < 5 *)
  assert (Hf := mod_upper_bound n 5). lia.
Qed.
