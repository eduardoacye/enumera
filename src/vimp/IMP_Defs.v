From Enums Require Import Enum_Defs.

Definition enum_rel (T : Type) := T -> nat -> Prop.
Definition enumerable_type {T : Type} (R : enum_rel T) :=
  (forall x, exists! k, R x k) /\ (forall k, exists! x, R x k).
Inductive enum_imp_nat : (enum_rel nat) :=
| EIN_Trivial (n : nat) : enum_imp_nat n n.
Inductive Loc : Type :=
| X (n : nat).
Inductive enum_imp_loc : (enum_rel Loc) :=
| EIL_Bypass (n k : nat) (H : enum_imp_nat n k) :
    enum_imp_loc (X n) k.
Inductive Arith : Type :=
| AN (n : nat)
| AX (x : Loc)
| AA (a1 a2 : Arith)
| AS (a1 a2 : Arith)
| AM (a1 a2 : Arith).
Inductive enum_imp_arith : (enum_rel Arith) :=
| EIA_N (n k : nat) (H : enum_imp_nat n k) :
    enum_imp_arith (AN n) (5 * k)
| EIA_X (x : Loc) (k : nat) (H : enum_imp_loc x k) :
    enum_imp_arith (AX x) (S (5 * k))
| EIA_A (a1 a2 : Arith) (k1 k2 : nat)
        (H1 : enum_imp_arith a1 k1)
        (H2 : enum_imp_arith a2 k2)
        (k : nat) (H3 : arank_tup k1 k2 = k) :
    enum_imp_arith (AA a1 a2) (S (S (5 * k)))
| EIA_S (a1 a2 : Arith) (k1 k2 : nat)
        (H1 : enum_imp_arith a1 k1)
        (H2 : enum_imp_arith a2 k2)
        (k : nat) (H3 : arank_tup k1 k2 = k) :
    enum_imp_arith (AS a1 a2) (S (S (S (5 * k))))
| EIA_M (a1 a2 : Arith) (k1 k2 : nat)
        (H1 : enum_imp_arith a1 k1)
        (H2 : enum_imp_arith a2 k2)
        (k : nat) (H3 : arank_tup k1 k2 = k) :
    enum_imp_arith (AM a1 a2) (S (S (S (S (5 * k))))).
Inductive Bool : Type :=
| BT | BF
| BE (a1 a2 : Arith)
| BL (a1 a2 : Arith)
| BA (b1 b2 : Bool)
| BO (b1 b2 : Bool)
| BN (b : Bool).
Inductive enum_imp_bool : (enum_rel Bool) :=
| EIB_T : enum_imp_bool BT 0
| EIB_F : enum_imp_bool BF 1
| EIB_E (a1 a2 : Arith) (k1 k2 : nat)
        (H1 : enum_imp_arith a1 k1)
        (H2 : enum_imp_arith a2 k2)
        (k : nat) (H3 : arank_tup k1 k2 = k) :
    enum_imp_bool (BE a1 a2) (S (S (5 * k)))
| EIB_L (a1 a2 : Arith) (k1 k2 : nat)
        (H1 : enum_imp_arith a1 k1)
        (H2 : enum_imp_arith a2 k2)
        (k : nat) (H3 : arank_tup k1 k2 = k) :
    enum_imp_bool (BL a1 a2) (S (S (S (5 * k))))
| EIB_A (b1 b2 : Bool) (k1 k2 : nat)
        (H1 : enum_imp_bool b1 k1)
        (H2 : enum_imp_bool b2 k2)
        (k : nat) (H3 : arank_tup k1 k2 = k) :
    enum_imp_bool (BA b1 b2) (S (S (S (S (5 * k)))))
| EIB_O (b1 b2 : Bool) (k1 k2 : nat)
        (H1 : enum_imp_bool b1 k1)
        (H2 : enum_imp_bool b2 k2)
        (k : nat) (H3 : arank_tup k1 k2 = k) :
    enum_imp_bool (BO b1 b2) (S (S (S (S (S (5 * k))))))
| EIB_N (b : Bool) (k : nat) (H : enum_imp_bool b k) :
    enum_imp_bool (BN b) (S (S (S (S (S (S (5 * k))))))).
Inductive Prog : Type :=
| PS
| PA (x : Loc) (a : Arith)
| PW (b : Bool) (p : Prog)
| PC (p1 p2 : Prog)
| PI (b : Bool) (p1 p2 : Prog).
Inductive enum_imp_prog : (enum_rel Prog) :=
| EIP_S : enum_imp_prog PS 0
| EIP_A (x : Loc) (a : Arith) (k1 k2 : nat)
        (H1 : enum_imp_loc x k1)
        (H2 : enum_imp_arith a k2)
        (k : nat) (H3 : arank_tup k1 k2 = k) :
    enum_imp_prog (PA x a) (S (4 * k))
| EIP_W (b : Bool) (p : Prog) (k1 k2 : nat)
        (H1 : enum_imp_bool b k1)
        (H2 : enum_imp_prog p k2)
        (k : nat) (H3 : arank_tup k1 k2 = k) :
    enum_imp_prog (PW b p) (S (S (4 * k)))
| EIP_C (p1 p2 : Prog) (k1 k2 : nat)
        (H1 : enum_imp_prog p1 k1)
        (H2 : enum_imp_prog p2 k2)
        (k : nat) (H3 : arank_tup k1 k2 = k) :
    enum_imp_prog (PC p1 p2) (S (S (S (4 * k))))
| EIP_I (b : Bool) (p1 p2 : Prog) (k0 k1 k2 : nat)
        (H1 : enum_imp_bool b k0)
        (H2 : enum_imp_prog p1 k1)
        (H3 : enum_imp_prog p2 k2)
        (k k' : nat)
        (H4 : arank_tup k1 k2 = k')
        (H5 : arank_tup k0 k' = k) :
    enum_imp_prog (PI b p1 p2) (S (S (S (S (4 * k))))).
