(defpackage :enumera
  (:use :cl)
  (:export
   #:cardinality #:enumeratesp #:position-of #:object-at
   #:rank #:unrank
   #:enumeration-condition #:enumeration-error #:enumeration-warning
   #:malformed-object-error #:malformed-position-error
   #:environment-error #:redefinition-warning
   #:@ #:find-enumeration #:defenum #:elet #:defcenum
   #:closure #:recursive
   #:evoid #:enat #:enatr
   #:econs #:elist #:eproduct
   #:esum #:esum* #:eunion
   #:ekleene
   #:compiled-dependencies #:compiled-cardinality #:compiled-enumeratesp
   #:compiled-rank #:compiled-unrank))

(in-package :enumera)

(defgeneric cardinality (enumeration))
(defgeneric enumeratesp (enumeration object))
(defgeneric position-of (enumeration object))
(defgeneric object-at (enumeration position))
(defgeneric compiled-dependencies (enumeration)
  (:method (e) nil))

(defgeneric compiled-cardinality (enumeration context)
  (:method (e ctx) `(,(cardinality e))))

(defgeneric compiled-enumeratesp (enumeration context object-var)
  (:method (e ctx xv) `((eql ,e ,xv))))

(defgeneric compiled-rank (enumeration context object-var)
  (:method (e ctx xv) `((declare (ignore ,xv)) 0)))

(defgeneric compiled-unrank (enumeration context position-var)
  (:method (e ctx kv) `((declare (ignore ,kv)) ,e)))
(defmethod cardinality (e) 1)
(defmethod enumeratesp (e x) (eql e x))
(defmethod position-of (e x) 0)
(defmethod object-at (e k) e)
(define-condition enumeration-condition (condition)
  ((enumeration :initarg :enumeration
                :reader relevant-enumeration)))

(define-condition enumeration-error
    (error enumeration-condition)
  ())

(define-condition enumeration-warning
    (style-warning enumeration-condition)
  ())
(define-condition malformed-object-error (enumeration-error)
  ((object :initarg :object
           :reader relevant-object)))
(define-condition malformed-position-error (enumeration-error)
  ((position :initarg :position
             :reader relevant-position)))
(define-condition environment-error (enumeration-error)
  ((environment :initarg :environment
                :reader relevant-environment)))
(define-condition redefinition-warning (enumeration-warning)
  ((redefinition :initarg :redefinition
                 :accessor relevant-redefinition)))
(defclass evar () ((name :initarg :name :reader evar-name)))

(defmacro @ (name)
  (check-type name (and symbol (not keyword)))
  `(make-instance 'evar :name ',name))
(defclass environment ()
  ((parent :initarg :parent :reader environment-parent)
   (names :accessor environment-names :initform nil)
   (enums :accessor environment-enums :initform nil)))
(defun empty-environment (&optional parent)
  (make-instance 'environment :parent parent))
(defmethod environment-lookup (env name)
  (values nil nil))

(defmethod environment-lookup ((env environment) name)
  (loop for x in (environment-names env)
        for y in (environment-enums env)
        when (eq name x)
          return (values y t)
        finally
           (return (let ((parent (environment-parent env)))
                     (environment-lookup parent name)))))
(defmethod environment-register ((env environment) name enum)
  (loop for xs on (environment-names env)
        for ys on (environment-enums env)
        when (eq name (car xs))
          do (setf (car ys) enum)
          and return t
        finally
           (push name (environment-names env))
           (push enum (environment-enums env))
           (return nil)))
(defvar *env* (empty-environment))
(defun find-enumeration (name)
  (multiple-value-bind (enum foundp)
      (environment-lookup *env* name)
    (unless foundp
      (error 'environment-error
             :enumeration name
             :environment *env*))
    enum))
(defun ensure-enumeration (name enum)
  (when (environment-register *env* name enum)
    (warn 'redefinition-warning
          :enumeration name
          :redefinition 'ensure-enumeration)
    name))
(defmethod cardinality ((e evar))
  (cardinality (find-enumeration (evar-name e))))

(defmethod enumeratesp ((e evar) x)
  (enumeratesp (find-enumeration (evar-name e)) x))

(defmethod position-of ((e evar) x)
  (position-of (find-enumeration (evar-name e)) x))

(defmethod object-at ((e evar) k)
  (object-at (find-enumeration (evar-name e)) k))
(defmethod compiled-dependencies ((e evar))
  (let ((enum (find-enumeration (evar-name e))))
    (adjoin enum (compiled-dependencies enum))))

(defmethod compiled-enumeratesp ((e evar) ctx xv)
  (let ((enum (find-enumeration (evar-name e))))
    `((,(context-enumeratesp ctx enum) ,xv))))

(defmethod compiled-rank ((e evar) ctx xv)
  (let ((enum (find-enumeration (evar-name e))))
    `((,(context-rank ctx enum) ,xv))))

(defmethod compiled-unrank ((e evar) ctx kv)
  (let ((enum (find-enumeration (evar-name e))))
    `((,(context-unrank ctx enum) ,kv))))
(defclass context ()
  ((name-table :initform (make-hash-table :test 'eq))
   (cname-table :initform (make-hash-table :test 'eq))
   (ename-table :initform (make-hash-table :test 'eq))
   (rname-table :initform (make-hash-table :test 'eq))
   (uname-table :initform (make-hash-table :test 'eq))))
(defmethod enumeration-name ((ctx context) e)
  (gensym (format nil "~A" (class-name (class-of e)))))

(defmethod cardinality-fname ((ctx context) e)
  (gensym (format nil "~A-CARDINALITY" (class-name (class-of e)))))

(defmethod enumeratesp-fname ((ctx context) e)
  (gensym (format nil "~A-ENUMERATESP" (class-name (class-of e)))))

(defmethod rank-fname ((ctx context) e)
  (gensym (format nil "~A-RANK" (class-name (class-of e)))))

(defmethod unrank-fname ((ctx context) e)
  (gensym (format nil "~A-UNRANK" (class-name (class-of e)))))
(defmethod ensure-context-enumeration ((ctx context) e)
  (with-slots (name-table cname-table ename-table rname-table uname-table)
      ctx
    (let ((name-id (gethash e name-table)))
      (if name-id name-id
          (let ((name-id (enumeration-name ctx e)))
            (setf (gethash e name-table) name-id)
            (setf (gethash name-id cname-table)
                  (cardinality-fname ctx e))
            (setf (gethash name-id ename-table)
                  (enumeratesp-fname ctx e))
            (setf (gethash name-id rname-table)
                  (rank-fname ctx e))
            (setf (gethash name-id uname-table)
                  (unrank-fname ctx e))
            name-id)))))
(defun context-cardinality (ctx e)
  (gethash (ensure-context-enumeration ctx e)
           (slot-value ctx 'cname-table)))

(defun context-enumeratesp (ctx e)
  (gethash (ensure-context-enumeration ctx e)
           (slot-value ctx 'ename-table)))

(defun context-rank (ctx e)
  (gethash (ensure-context-enumeration ctx e)
           (slot-value ctx 'rname-table)))

(defun context-unrank (ctx e)
  (gethash (ensure-context-enumeration ctx e)
           (slot-value ctx 'uname-table)))
(defclass closure ()
  ((environment :initform *env*
                :reader environment)))
(defmethod cardinality :around ((e closure))
  (let ((*env* (environment e)))
    (call-next-method)))

(defmethod enumeratesp :around ((e closure) x)
  (let ((*env* (environment e)))
    (call-next-method)))

(defmethod position-of :around ((e closure) x)
  (let ((*env* (environment e)))
    (call-next-method)))

(defmethod object-at :around ((e closure) k)
  (let ((*env* (environment e)))
    (call-next-method)))
(defun contextualize-redefinition (redefinition)
  #'(lambda (condition)
      (setf (relevant-redefinition condition)
            redefinition)))

(defmacro defenum (name &body enum)
  (check-type enum (not null))
  (check-type (cdr enum) null)
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (handler-bind ((redefinition-warning
                      (contextualize-redefinition 'defenum)))
       (ensure-enumeration ',name ,(car enum)))))
(defmacro elet (bindings &body body)
  (let ((extensions
          (loop for (name enum) in bindings
                collect `(ensure-enumeration ',name ,name))))
    `(handler-bind ((redefinition-warning
                      (contextualize-redefinition 'elet)))
       (let ((*env* (empty-environment *env*)))
         (let ,bindings
           ,@extensions
           ,@body)))))
(defmethod compiled-dependencies :around ((e closure))
  (let ((*env* (environment e)))
    (call-next-method)))

(defmethod compiled-cardinality :around ((e closure) ctx)
  (let ((*env* (environment e)))
    (call-next-method)))

(defmethod compiled-enumeratesp :around ((e closure) ctx xv)
  (let ((*env* (environment e)))
    (call-next-method)))

(defmethod compiled-rank :around ((e closure) ctx xv)
  (let ((*env* (environment e)))
    (call-next-method)))

(defmethod compiled-unrank :around ((e closure) ctx kv)
  (let ((*env* (environment e)))
    (call-next-method)))
(defclass recursive ()
  ((stored-cardinality)
   (inside-self-p :initform nil)))
(defmethod cardinality :around ((e recursive))
 (with-slots (stored-cardinality inside-self-p) e
   (cond
     ((slot-boundp e 'stored-cardinality) stored-cardinality)
     (inside-self-p nil)
     (t (unwind-protect
             (setf inside-self-p t
                   stored-cardinality (call-next-method))
          (setf inside-self-p nil))))))
(defmethod enumeratesp :before ((e recursive) x)
  (unless (slot-boundp e 'stored-cardinality)
    (cardinality e)))
(defmethod position-of :before ((e recursive) x)
  (unless (slot-boundp e 'stored-cardinality)
    (cardinality e)))
(defmethod object-at :before ((e recursive) k)
  (unless (slot-boundp e 'stored-cardinality)
    (cardinality e)))
(defmethod compiled-dependencies :around ((e recursive))
  (unless (slot-value e 'inside-self-p)
    (unwind-protect
         (progn (setf (slot-value e 'inside-self-p) t)
                (call-next-method))
      (setf (slot-value e 'inside-self-p) nil))))

(defmethod compiled-enumeratesp :before ((e recursive) ctx xv)
  (unless (slot-boundp e 'stored-cardinality)
    (cardinality e)))

(defmethod compiled-rank :before ((e recursive) ctx xv)
  (unless (slot-boundp e 'stored-cardinality)
    (cardinality e)))

(defmethod compiled-unrank :before ((e recursive) ctx kv)
  (unless (slot-boundp e 'stored-cardinality)
    (cardinality e)))
(let ((the-void (make-instance 'closure)))
  (defmethod cardinality ((e (eql the-void))) 0)
  (defmethod enumeratesp ((e (eql the-void)) x) nil)
  
  (defenum evoid the-void)
  (defmethod compiled-enumeratesp ((e (eql the-void)) ctx xv)
    `((declare (ignore ,xv))
      nil))
  
  (defmethod compiled-rank ((e (eql the-void)) ctx xv)
    `((declare (ignore ,xv))))
  
  (defmethod compiled-unrank ((e (eql the-void)) ctx kv)
    `((declare (ignore ,kv)))))
(let ((the-nats (make-instance 'closure)))
  (defmethod cardinality ((e (eql the-nats))) nil)
  (defmethod enumeratesp ((e (eql the-nats)) x)
    (and (integerp x) (not (minusp x))))
  
  (defmethod position-of ((e (eql the-nats)) x) x)
  (defmethod object-at ((e (eql the-nats)) k) k)
  
  (defenum enat the-nats)
  (defmethod compiled-enumeratesp ((e (eql the-nats)) ctx xv)
    `((and (integerp ,xv) (not (minusp ,xv)))))
  
  (defmethod compiled-rank ((e (eql the-nats)) ctx xv)
    `(,xv))
  
  (defmethod compiled-unrank ((e (eql the-nats)) ctx kv)
    `(,kv)))
(defclass enatr ()
  ((from :initarg :from)
   (to :initarg :to)))

(defun enatr (from &optional to)
  (check-type from (integer 0))
  (check-type to (or (integer 0) null))
  (make-instance 'enatr :from from :to to))
(defmethod cardinality ((e enatr))
  (with-slots (from to) e
    (cond ((eql to nil) nil)
          ((<= from to) (1+ (- to from)))
          (t (1+ (- from to))))))
(defmethod enumeratesp ((e enatr) x)
  (and (integerp x) (not (minusp x))
       (with-slots (from to) e
         (cond ((eql to nil) (<= from x))
               ((<= from to) (<= from x to))
               (t (<= to x from))))))
(defmethod position-of ((e enatr) x)
  (with-slots (from to) e
    (cond ((eql to nil) (- x from))
          ((<= from to) (- x from))
          (t (- from x)))))
(defmethod object-at ((e enatr) k)
  (with-slots (from to) e
    (cond ((eql to nil) (+ k from))
          ((<= from to) (+ k from))
          (t (- from k)))))
(defmethod compiled-enumeratesp ((e enatr) ctx xv)
  `((and (integerp ,xv) (not (minusp ,xv))
         ,(with-slots (from to) e
            (cond ((eql to nil) `(<= ,from ,xv))
                  ((<= from to) `(<= ,from ,xv ,to))
                  (t `(<= ,to ,xv ,from)))))))

(defmethod compiled-rank ((e enatr) ctx xv)
  (with-slots (from to) e
    (cond ((eql to nil) `((- ,xv ,from)))
          ((<= from to) `((- ,xv ,from)))
          (t `((- ,from ,xv))))))

(defmethod compiled-unrank ((e enatr) ctx kv)
  (with-slots (from to) e
    (cond ((eql to nil)
           `((+ ,kv ,from)))
          ((<= from to)
           `((+ ,kv ,from)))
          (t
           `((- ,from ,kv))))))
(defclass econs (recursive closure)
  ((ecar :initarg :ecar)
   (ecdr :initarg :ecdr)))

(defun econs (ecar ecdr)
  (make-instance 'econs :ecar ecar :ecdr ecdr))
(defun elist (&rest es)
  (reduce #'econs es
          :initial-value nil
          :from-end t))
(defmethod cardinality ((e econs))
  (with-slots (ecar ecdr) e
    (let ((ccar (cardinality ecar))
          (ccdr (cardinality ecdr)))
      (cond
        ((or (eql ccar 0) (eql ccdr 0)) 0)
        ((or (not ccar) (not ccdr)) nil)
        (t (* ccar ccdr))))))
(defmethod enumeratesp ((e econs) x)
  (with-slots (ecar ecdr) e
    (and (consp x)
         (enumeratesp ecar (car x))
         (enumeratesp ecdr (cdr x)))))
(defmethod position-of ((e econs) x)
  (with-slots (ecar ecdr) e
    (let ((ccar (cardinality ecar))
          (ccdr (cardinality ecdr)))
      (cond (ccar (+ (* ccar (position-of ecdr (cdr x)))
                     (position-of ecar (car x))))
            (ccdr (+ (* ccdr (position-of ecar (car x)))
                     (position-of ecdr (cdr x))))
            (t (tuplas:rank 2 (list (position-of ecar (car x))
                                    (position-of ecdr (cdr x)))))))))
(defmethod object-at ((e econs) k)
  (with-slots (ecar ecdr) e
    (let ((ccar (cardinality ecar))
          (ccdr (cardinality ecdr)))
      (cond (ccar (multiple-value-bind (kcdr kcar) (floor k ccar)
                    (cons (object-at ecar kcar)
                          (object-at ecdr kcdr))))
            (ccdr (multiple-value-bind (kcar kcdr) (floor k ccdr)
                    (cons (object-at ecar kcar)
                          (object-at ecdr kcdr))))
            (t (destructuring-bind (kcar kcdr) (tuplas:unrank 2 k)
                 (cons (object-at ecar kcar)
                       (object-at ecdr kcdr))))))))
(defclass eproduct (recursive closure)
  ((operands :initarg :operands)
   (noperands)
   (boperands)
   (uoperands)
   (bproducts)
   (udimension)))

(defun eproduct (&rest operands)
  (make-instance 'eproduct :operands operands))
(defmethod cardinality ((e eproduct))
  (loop with bproducts = 1
        for op in (slot-value e 'operands)
        for c = (cardinality op)
        sum 1 into noperands
        if c
          collect op into boperands
          and do (setf bproducts (* bproducts c))
        else
          collect op into uoperands
          and sum 1 into udimension
        finally
           (setf (slot-value e 'boperands) boperands
                 (slot-value e 'uoperands) uoperands
                 (slot-value e 'noperands) noperands
                 (slot-value e 'bproducts) bproducts
                 (slot-value e 'udimension) udimension)
           (return (cond ((zerop bproducts) 0)
                         ((zerop udimension) bproducts)))))
(defmethod enumeratesp ((e eproduct) xs)
  (when (listp xs)
    (loop for op in (slot-value e 'operands)
          for x in xs
          sum 1 into n
          unless (enumeratesp op x)
            return nil
          finally
             (return (= n (slot-value e 'noperands))))))
(defmethod position-of ((e eproduct) xs)
  (with-slots (operands bproducts udimension) e
    (loop with bfactor = 1
          for op in operands
          for x in xs
          for c = (cardinality op)
          if c
            sum (* bfactor (position-of op x)) into brank
            and do (setf bfactor (* bfactor c))
          else
            collect (position-of op x) into ys
          finally
             (return (+ brank
                        (* bproducts
                           (tuplas:rank udimension ys)))))))
(defmethod object-at ((e eproduct) k)
  (with-slots (operands boperands uoperands bproducts udimension) e
    (multiple-value-bind (uk bk) (floor k bproducts)
      (loop with ys = (tuplas:unrank udimension uk)
            with q = bk and r = 0
            with bop = boperands
            with uop = uoperands
            for op in operands
            collect (if (eq op (car bop))
                        (progn
                          (pop bop)
                          (setf (values q r) (floor q (cardinality op)))
                          (object-at op r))
                        (progn
                          (pop uop)
                          (object-at op (pop ys))))))))
(defmethod compiled-dependencies ((e econs))
  (with-slots (ecar ecdr) e
    (let ((ecardeps (compiled-dependencies ecar))
          (ecdrdeps (compiled-dependencies ecdr)))
      (adjoin ecar (adjoin ecdr (union ecardeps ecdrdeps))))))

(defmethod compiled-enumeratesp ((e econs) ctx xv)
  (with-slots (ecar ecdr) e
    (let ((ccar (cardinality ecar))
          (ccdr (cardinality ecdr)))
      (if (or (eql ccar 0) (eql ccdr 0))
          '(nil)
          `((and (consp ,xv)
                 (,(context-enumeratesp ctx ecar) (car ,xv))
                 (,(context-enumeratesp ctx ecdr) (cdr ,xv))))))))
(defmethod compiled-rank ((e econs) ctx xv)
  (with-slots (ecar ecdr) e
    (let ((ccar (cardinality ecar))
          (ccdr (cardinality ecdr)))
      (cond
        (ccar
         `((+ (* ,ccar (,(context-rank ctx ecdr) (cdr ,xv)))
              (,(context-rank ctx ecar) (car ,xv)))))
        (ccdr
         `((+ (* ,ccdr (,(context-rank ctx ecar) (car ,xv)))
              (,(context-rank ctx ecdr) (cdr ,xv)))))
        (t
         `((tuplas:rank 2 (list (,(context-rank ctx ecar) (car ,xv))
                                (,(context-rank ctx ecdr) (cdr ,xv))))))))))

(defmethod compiled-unrank ((e econs) ctx kv)
  (with-slots (ecar ecdr) e
    (let ((ccar (cardinality ecar)) (kcarv (gensym "KCAR"))
          (ccdr (cardinality ecdr)) (kcdrv (gensym "KCDR")))
      (cond (ccar
             `((multiple-value-bind (,kcdrv ,kcarv) (floor ,kv ,ccar)
                 (cons (,(context-unrank ctx ecar) ,kcarv)
                       (,(context-unrank ctx ecdr) ,kcdrv)))))
            (ccdr
             `((multiple-value-bind (,kcarv ,kcdrv) (floor ,kv ,ccdr)
                 (cons (,(context-unrank ctx ecar) ,kcarv)
                       (,(context-unrank ctx ecdr) ,kcdrv)))))
            (t
             `((destructuring-bind (,kcarv ,kcdrv) (tuplas:unrank 2 ,kv)
                 (cons (,(context-unrank ctx ecar) ,kcarv)
                       (,(context-unrank ctx ecdr) ,kcdrv)))))))))
(defmethod compiled-dependencies ((e eproduct))
  (loop with deps = nil
        for op in (slot-value e 'operands)
        for opd = (compiled-dependencies op)
        do (setf deps (adjoin op (union opd deps)))
        finally (return deps)))

(defmethod compiled-enumeratesp ((e eproduct) ctx xsv)
  (labels ((build-form (ops xv)
             (if (null ops)
                 `(null ,xsv)
                 (let ((op (car ops)))
                   `(and (consp ,xsv)
                         (let* ((,xv (car ,xsv)) (,xsv (cdr ,xsv)))
                           (and (,(context-enumeratesp ctx op) ,xv)
                                ,(build-form (cdr ops) xv))))))))
    `(,(build-form (slot-value e 'operands) (gensym "X")))))
(defmethod compiled-rank ((e eproduct) ctx xsv)
  (with-slots (operands bproducts udimension) e
    (labels ((build-form (bks bcs uks)
               (if (null bks)
                   `(tuplas:rank ,udimension ,(cons 'list uks))
                   `(+ ,(car bks)
                       (* ,(car bcs)
                          ,(build-form (cdr bks) (cdr bcs) uks))))))
      (loop for op in operands
            for c = (cardinality op)
            for r = (context-rank ctx op)
            for x = (gensym "X")
            for k = (gensym "K")
            collect r into rs
            collect x into xs
            collect k into ks
            if c
              collect k into bks
              and collect c into bcs
            else
              collect k into uks
            finally
               (return
                 `((destructuring-bind ,xs ,xsv
                     (let ,(loop for k in ks
                                 for r in rs
                                 for x in xs
                                 collect `(,k (,r ,x)))
                       ,(build-form bks bcs uks)))))))))
(defmethod compiled-unrank ((e eproduct) ctx kv)
  (with-slots (operands boperands uoperands bproducts udimension) e
    (let ((ukv (gensym "UK"))
          (bkv (gensym "BK"))
          (rv (gensym "R")))
      (loop for op in operands
            for c = (cardinality op)
            for k = (gensym "K")
            if c
              collect `(progn (setf (values ,bkv ,rv) (floor ,bkv ,c))
                              (,(context-unrank ctx op) ,rv))
                into listargs
            else
              collect `(,(context-unrank ctx op) ,k)
                into listargs
                and collect k into uks
            finally
               (return
                 `((let ((,rv 0))
                     (multiple-value-bind (,ukv ,bkv)
                         (floor ,kv ,bproducts)
                       (destructuring-bind ,uks
                           (tuplas:unrank ,udimension ,ukv)
                         (list ,@listargs))))))))))
(defclass esum (recursive closure)
  ((left :initarg :left)
   (right :initarg :right)))

(defun esum (e1 e2)
  (make-instance 'esum :left e1 :right e2))
(defun esum* (&rest es)
  (reduce #'esum es
          :initial-value (@ evoid)
          :from-end t))
(defmethod cardinality ((e esum))
  (with-slots (left right) e
    (let ((c1 (cardinality left))
          (c2 (cardinality right)))
      (and c1 c2 (+ c1 c2)))))
(defmethod enumeratesp ((e esum) x)
  (with-slots (left right) e
    (or (enumeratesp left x)
        (enumeratesp right x))))
(defmethod position-of ((e esum) x)
  (with-slots (left right) e
    (let ((c1 (cardinality left))
          (c2 (cardinality right)))
      (cond (c1
             (if (enumeratesp left x)
                 (position-of left x)
                 (+ c1 (position-of right x))))
            (c2
             (if (enumeratesp right x)
                 (position-of right x)
                 (+ c2 (position-of left x))))
            (t
             (if (enumeratesp left x)
                 (* 2 (position-of left x))
                 (1+ (* 2 (position-of right x)))))))))
(defmethod object-at ((e esum) k)
  (with-slots (left right) e
    (let ((c1 (cardinality left))
          (c2 (cardinality right)))
      (cond (c1
             (if (< k c1)
                 (object-at left k)
                 (object-at right (- k c1))))
            (c2
             (if (< k c2)
                 (object-at right k)
                 (object-at left (- k c2))))
            (t
             (multiple-value-bind (q r)
                 (floor k 2)
               (if (zerop r)
                   (object-at left q)
                   (object-at right q))))))))
(defclass eunion (recursive closure)
  ((operands :initarg :operands)
   (boperands)
   (uoperands)
   (bprefixes)
   (uprefix)
   (udivisor)))

(defun eunion (&rest operands)
  (make-instance 'eunion :operands operands))
(defmethod cardinality ((e eunion))
  (loop for op in (slot-value e 'operands)
        for c = (cardinality op)
        if c
          collect op into boperands
          and sum c into uprefix
          and collect uprefix into bprefixes
        else
          collect op into uoperands
          and sum 1 into udivisor
        finally
           (setf (slot-value e 'boperands) boperands
                 (slot-value e 'uoperands) uoperands
                 (slot-value e 'bprefixes) (cons 0 bprefixes)
                 (slot-value e 'uprefix) uprefix
                 (slot-value e 'udivisor) udivisor)
           (return (and (null uoperands) uprefix))))
(defmethod enumeratesp ((e eunion) x)
  (loop for op in (slot-value e 'operands)
        when (enumeratesp op x) return t))
(defmethod position-of ((e eunion) x)
  (with-slots (boperands bprefixes uoperands uprefix udivisor) e
    (loop for op in boperands
          for pf in bprefixes
          when (enumeratesp op x)
            return (+ pf (position-of op x))
          finally
             (return
               (loop for op in uoperands
                     for i from 0
                     when (enumeratesp op x)
                       return (let ((k (position-of op x)))
                                (+ uprefix i (* udivisor k))))))))

(defmethod object-at ((e eunion) k)
  (with-slots (boperands bprefixes uoperands uprefix udivisor) e
    (loop for op in boperands
          for pf in bprefixes
          for bound in (cdr bprefixes)
          when (< k bound)
            return (object-at op (- k pf))
          finally
             (return
               (multiple-value-bind (k* i)
                   (floor (- k uprefix) udivisor)
                 (object-at (elt uoperands i) k*))))))
(defmethod compiled-dependencies ((e esum))
  (with-slots (left right) e
    (let ((leftdeps (compiled-dependencies left))
          (rightdeps (compiled-dependencies right)))
      (adjoin left (adjoin right (union leftdeps rightdeps))))))

(defmethod compiled-enumeratesp ((e esum) ctx xv)
  (with-slots (left right) e
    `((or (,(context-enumeratesp ctx left) ,xv)
          (,(context-enumeratesp ctx right) ,xv)))))
(defmethod compiled-rank ((e esum) ctx xv)
  (with-slots (left right) e
    (let ((c1 (cardinality left))
          (c2 (cardinality right)))
      (cond (c1
             `((if (,(context-enumeratesp ctx left) ,xv)
                   (,(context-rank ctx left) ,xv)
                   (+ ,c1 (,(context-rank ctx right) ,xv)))))
            (c2
             `((if (,(context-enumeratesp ctx right) ,xv)
                   (,(context-rank ctx right) ,xv)
                   (+ ,c2 (,(context-rank ctx left) ,xv)))))
            (t
             `((if (,(context-enumeratesp ctx left) ,xv)
                   (* 2 (,(context-rank ctx left) ,xv))
                   (1+ (* 2 (,(context-rank ctx right) ,xv))))))))))
(defmethod compiled-unrank ((e esum) ctx kv)
  (with-slots (left right) e
    (let ((c1 (cardinality left))
          (c2 (cardinality right)))
      (cond (c1
             `((if (< ,kv ,c1)
                   (,(context-unrank ctx left) ,kv)
                   (,(context-unrank ctx right) (- ,kv ,c1)))))
            (c2
             `((if (< ,kv ,c2)
                   (,(context-unrank ctx right) ,kv)
                   (,(context-unrank ctx left) (- ,kv ,c2)))))
            (t
             (let ((qv (gensym "Q"))
                   (rv (gensym "R")))
               `((multiple-value-bind (,qv ,rv) (floor ,kv 2)
                   (if (zerop ,rv)
                       (,(context-unrank ctx left) ,qv)
                       (,(context-unrank ctx right) ,qv))))))))))
(defmethod compiled-dependencies ((e eunion))
  (with-slots (operands) e
    (loop with deps = nil
          for op in operands
          for opd = (compiled-dependencies op)
          do (setf deps (adjoin op (union opd deps)))
          finally (return deps))))

(defmethod compiled-enumeratesp ((e eunion) ctx xv)
  `((or ,@(loop for op in (slot-value e 'operands)
                collect `(,(context-enumeratesp ctx op) ,xv)))))
(defmethod compiled-rank ((e eunion) ctx xv)
  (with-slots (boperands bprefixes uoperands uprefix udivisor) e
    `((cond ,@(loop for op in boperands
                    for pf in bprefixes
                    collect `((,(context-enumeratesp ctx op) ,xv)
                              (+ ,pf (,(context-rank ctx op) ,xv))))
            ,@(loop for op in uoperands
                    for i from 0
                    collect `((,(context-enumeratesp ctx op) ,xv)
                              (+ ,uprefix ,i
                                 (* ,udivisor
                                    (,(context-rank ctx op) ,xv)))))
            (t 0)))))
(defmethod compiled-unrank ((e eunion) ctx kv)
  (with-slots (boperands bprefixes uoperands uprefix udivisor) e
    `((cond
        ,@(loop for op in boperands
                for pf in bprefixes
                for bound in (cdr bprefixes)
                collect `((< ,kv ,bound)
                          (,(context-unrank ctx op) (- ,kv ,pf))))
        ,@(unless (zerop udivisor)
            (let ((k*v (gensym "K*"))
                  (iv (gensym "I")))
              `((t
                 (multiple-value-bind (,k*v ,iv)
                     (floor (- ,kv ,uprefix) ,udivisor)
                   (case ,iv
                     ,@(loop
                         for op in uoperands
                         for mod from 0
                         for ufunc = (context-unrank ctx op)
                         collect `((,mod)
                                   (,ufunc ,k*v)))))))))))))
(defclass ekleene (recursive closure)
  ((operand :initarg :operand)))

(defun ekleene (operand)
  (make-instance 'ekleene :operand operand))
(defmethod cardinality ((e ekleene))
  (with-slots (operand) e
    (and (eql 0 (cardinality operand)) 1)))
(defmethod enumeratesp ((e ekleene) xs)
  (when (listp xs)
    (loop for x in xs
          unless (enumeratesp (slot-value e 'operand) x)
            return nil
          finally (return t))))
(defmethod position-of ((e ekleene) xs)
  (with-slots (operand) e
    (let ((c (cardinality operand)))
      (if c
          (loop for k = 0 then (1+ (+ (* c k) (position-of operand x)))
                for x in xs
                finally (return k))
          (if (null xs) 0
              (loop for x in xs
                    collect (position-of operand x) into ys
                    sum 1 into n
                    finally
                       (return
                         (let* ((k* (tuplas:rank n ys))
                                (n* (1- n)))
                           (1+ (tuplas:rank* 2 (list k* n*)))))))))))
(defmethod object-at ((e ekleene) k)
  (with-slots (operand) e
    (let ((c (cardinality operand)))
      (if c
          (loop with xs and r
                while (> k 0)
                do (setf (values k r) (floor (1- k) c))
                   (push (object-at operand r) xs)
                finally (return xs))
          (if (zerop k) nil
              (destructuring-bind (k* n)
                  (tuplas:unrank* 2 (1- k))
                (loop for y in (tuplas:unrank (1+ n) k*)
                      collect (object-at operand y))))))))
(defmethod compiled-dependencies ((e ekleene))
  (with-slots (operand) e
    (adjoin operand (compiled-dependencies operand))))

(defmethod compiled-enumeratesp ((e ekleene) ctx xsv)
  (with-slots (operand) e
    (let ((xv (gensym "X")))
      `((and (listp ,xsv)
             (loop for ,xv in ,xsv
                   unless (,(context-enumeratesp ctx operand) ,xv)
                     return nil
                   finally (return t)))))))

(defmethod compiled-rank ((e ekleene) ctx xsv)
  (with-slots (operand) e
    (let ((c (cardinality operand))
          (rf (context-rank ctx operand)))
      (if c
          (let ((kv (gensym "K"))
                (xv (gensym "X")))
            `((loop for ,kv = 0 then (1+ (+ (* ,c ,kv) (,rf ,xv)))
                    for ,xv in ,xsv
                    finally (return ,kv))))
          (let ((xv (gensym "X"))
                (ysv (gensym "YS"))
                (nv (gensym "N")))
            `((if (null ,xsv) 0
                  (loop for ,xv in ,xsv
                        collect (,rf ,xv) into ,ysv
                        sum 1 into ,nv
                        finally
                           (return
                             (1+ (tuplas:rank* 2 (list (tuplas:rank ,nv ,ysv)
                                                       (1- ,nv)))))))))))))

(defmethod compiled-unrank ((e ekleene) ctx kv)
  (with-slots (operand) e
    (let ((c (cardinality operand))
          (uf (context-unrank ctx operand)))
      (if c
          (let ((xsv (gensym "XS"))
                (rv (gensym "R")))
            `((loop with ,xsv and ,rv
                    while (> ,kv 0)
                    do (setf (values ,kv ,rv) (floor (1- ,kv) ,c))
                       (push (,uf ,rv) ,xsv)
                    finally (return ,xsv))))
          (let ((k*v (gensym "K*"))
                (nv (gensym "N"))
                (yv (gensym "Y")))
            `((if (zerop k) nil
                  (destructuring-bind (,k*v ,nv)
                      (tuplas:unrank* 2 (1- ,kv))
                    (loop for ,yv in (tuplas:unrank (1+ ,nv) ,k*v)
                          collect (,uf ,yv))))))))))
(defun rank (e x)
  (unless (enumeratesp e x)
    (error 'malformed-object-error
           :enumeration e
           :object x))
  (position-of e x))
(defun unrank (e k)
  (let ((c (cardinality e)))
    (unless (and (integerp k)
                 (not (minusp k))
                 (or (not c) (< k c)))
      (error 'malformed-position-error
             :enumeration e
             :position k)))
  (object-at e k))
(defvar *ctx* (make-instance 'context))
(defun symbolicate (&rest things)
  (let* ((length (reduce #'+ things
                         :key (lambda (x) (length (string x)))))
         (name (make-array length :element-type 'character)))
    (let ((index 0))
      (dolist (thing things (values (intern name)))
        (let* ((x (string thing))
               (len (length x)))
          (replace name x :start1 index)
          (incf index len))))))
(defmacro defcenum (basename enumeration
                    &body declarations
                    &environment env)
  (let ((e (eval (macroexpand enumeration env)))
        (ctx *ctx*)
        (cardinality-name (symbolicate basename "-CARDINALITY"))
        (enumeratesp-name (symbolicate basename "-ENUMERATESP"))
        (rank-name (symbolicate basename "-RANK"))
        (unrank-name (symbolicate basename "-UNRANK")))
    (let ((xv (gensym "X"))
          (kv (gensym "K"))
          (deps (adjoin e (compiled-dependencies e)))
          (c (cardinality e)))
      `(labels ,(loop for dep in deps
                      append `((,(context-enumeratesp ctx dep) (,xv)
                                ,@declarations
                                ,@(compiled-enumeratesp dep ctx xv))
                               (,(context-rank ctx dep) (,xv)
                                ,@declarations
                                ,@(compiled-rank dep ctx xv))
                               (,(context-unrank ctx dep) (,kv)
                                (declare (type (integer 0) ,kv))
                                ,@declarations
                                ,@(compiled-unrank dep ctx kv))))
         (declare (inline ,@(loop for dep in deps
                                  when (typep dep 'evar)
                                    append `(,(context-enumeratesp ctx dep)
                                             ,(context-rank ctx dep)
                                             ,(context-unrank ctx dep)))))
         (declare (ftype (function (t) (integer 0))
                         ,@(loop for dep in deps
                                 collect (context-rank ctx dep))))
         (declare (ftype (function ((integer 0)) t)
                         ,@(loop for dep in deps
                                 collect (context-unrank ctx dep))))
         (defun ,cardinality-name ()
           ,@declarations
           ,@(compiled-cardinality e ctx))
         
         (defun ,enumeratesp-name (,xv)
           ,@declarations
           (,(context-enumeratesp ctx e) ,xv))
         (defun ,rank-name (,xv)
           ,@declarations
           (unless (,(context-enumeratesp ctx e) ,xv)
             (error 'malformed-object-error
                    :enumeration ',(symbolicate "COMPILED-" basename)
                    :object ,xv))
           (,(context-rank ctx e) ,xv))
         
         (defun ,unrank-name (,kv)
           ,@declarations
           (unless (and (integerp ,kv) (<= 0 ,kv)
                        ,(if c `(< ,kv ,c) t))
             (error 'malformed-position-error
                    :enumeration ',(symbolicate "COMPILED-" basename)
                    :position ,kv))
           (,(context-unrank ctx e) ,kv))))))
