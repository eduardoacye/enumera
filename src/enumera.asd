(defpackage :enumera.system
  (:use :cl :asdf))

(in-package :enumera.system)

(defsystem :enumera
  :serial t
  :components
  ((:file "tuplas")
   (:file "enumera")))
