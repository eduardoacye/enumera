(defpackage :tuplas
  (:use :cl)
  (:export #:rank #:unrank
           #:component-sum-offset
           #:component-sum-count
           #:rank* #:unrank*))

(in-package :tuplas)

(defun simplex/factorial (n m)
  (/ (loop for prod = 1 then (* prod i)
           for i from m below (+ n m)
           finally (return prod))
     (loop for prod = 1 then (* prod i)
           for i from 1 upto n
           finally (return prod))))
(defun simplex/iterative (n m)
  (loop for prod = m then (floor (* prod (+ m i)) (1+ i))
        for i from 1 below n
        finally (return prod)))
(defun simplex/fixnum (n m)
  (declare (optimize (speed 3) (safety 0)))
  (declare (type fixnum n m))
  (loop for prod = m then (floor (the fixnum (* prod a)) b)
        for a fixnum from (1+ m) below (+ m n)
        for b fixnum from 2 upto n
        finally (return (the fixnum prod))))
(defun simplex/combined (n m)
  (if (< n 7)
      (if (< m (aref #(0 0 2147483648 2097152 72527 10204 2863) n))
          (simplex/fixnum n m)
          (simplex/iterative n m))
      (simplex/factorial n m)))
(defconstant +mbound-2+ 141421)
(defconstant +mbound-3+ 3914)
(defconstant +table-size+ (+ +mbound-2+ +mbound-3+))
(defvar *simplex-table*
  (let ((table (make-array +table-size+
                           :element-type 'fixnum
                           :initial-element 0)))
    (setf (aref table 1) 1)
    (loop for prev = 1 then curr
          for m from 2 below +mbound-2+
          for curr = (+ m prev)
          do (setf (aref table m) curr))
    (setf (aref table (1+ +mbound-2+)) 1)
    (loop for prev = 1 then curr
          for m from 2 below +mbound-3+
          for i = (+ +mbound-2+ m)
          for curr = (+ (aref table m) prev)
          do (setf (aref table i) curr))
    table))
(declaim (type (simple-array fixnum (#.+table-size+)) *simplex-table*))

(defun simplex-from-table-2 (m)
  (declare (optimize (speed 3) (safety 0)))
  (declare (type fixnum m))
  (aref *simplex-table* m))

(defun simplex-from-table-3 (m)
  (declare (optimize (speed 3) (safety 0)))
  (declare (type fixnum m))
  (aref *simplex-table* (+ m +mbound-2+)))
(defun simplex (n m)
  (declare (type (integer 0) n m))
  (cond
   ((or (eql m 0) (eql m 1)) m)
   ((eql n 0) 1)
   ((eql n 0) 1)
   ((and (= n 2) (< m +mbound-2+))
    (simplex-from-table-2 m))
   ((and (= n 3) (< m +mbound-3+))
    (simplex-from-table-3 m))
   (t
    (simplex/combined n m))))
(defun simplex-inv (n k)
  (declare (type (integer 0) n k))
  (flet ((find-bounds (a b)
           (loop for s = (simplex n b)
                 until (< k s) do
                   (setf a b)
                   (setf b (* 2 b))
                 finally (return (values a b))))
         (narrow-bounds (a b)
           (loop for diff = (- b a)
                 until (<= diff 1)
                 for c = (+ a (floor diff 2))
                 do (if (<= (simplex n c) k)
                        (setf a c)
                        (setf b c))
                 finally (return a))))
    (cond
      ((eql k 0) 0)
      ((eql n 0) 1)
      ((eql n 1) k)
      ((eql n 2)
       (multiple-value-call #'narrow-bounds
         (find-bounds 1 (1- +mbound-2+))))
      ((eql n 3)
       (multiple-value-call #'narrow-bounds
         (find-bounds 1 (1- +mbound-3+))))
      (t
       (multiple-value-call #'narrow-bounds
         (find-bounds 1 2))))))
(declaim (ftype (function ((integer 0) list) (integer 0)) rank))
(defun rank (n xs)
  (cond ((eql n 0) 0)
        ((eql n 1) (car xs))
        (t (loop for i from 1 upto (1+ n)
                 for x in xs
                 sum x into a
                 sum (simplex i a)))))
(declaim (ftype (function ((integer 0) (integer 0)) list) unrank))
(defun unrank (n k)
  (cond ((eql n 0) nil)
        ((eql n 1) (list k))
        (t (loop with xs = nil
                 with m = (simplex-inv n k)
                 with d = (- k (simplex n m))
                 for i from (1- n) downto 2
                 for a = (simplex-inv i d)
                 do (push (- m a) xs)
                    (setf m a)
                    (setf d (- d (simplex i a)))
                 finally
                    (push (- m d) xs)
                    (push d xs)
                    (return xs)))))
(defun component-sum-offset (n m)
  (simplex n m))
(defun component-sum-count (n m)
  (if (zerop n) 1
      (simplex (1- n) (1+ m))))
(defun rank/dup (n xs)
  (cond ((eql n 0) 0)
        ((eql n 1) (car xs))
        (t (rank 2 (list (car xs) (rank/dup (1- n) (cdr xs)))))))

(defun unrank/dup (n k)
  (cond ((eql n 0) nil)
        ((eql n 1) (list k))
        (t (destructuring-bind (k1 k*) (unrank 2 k)
             (cons k1 (unrank/dup (1- n) k*))))))
(defun rank2* (k1 k2)
  (+ (ash k1 (1+ k2)) (1- (expt 2 k2))))

(defun unrank2* (k)
  (loop with r
        do (setf (values k r) (floor k 2))
        until (zerop r)
        sum 1 into k2
        finally (return (values k k2))))
(defun rank* (n xs)
  (cond ((eql n 0) 0)
        ((eql n 1) (car xs))
        (t (rank2* (car xs) (rank* (1- n) (cdr xs))))))

(defun unrank* (n k)
  (cond ((eql n 0) nil)
        ((eql n 1) (list k))
        (t (multiple-value-bind (k1 k2) (unrank2* k)
             (cons k1 (unrank* (1- n) k2))))))
