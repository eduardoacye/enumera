(defpackage :imp.system
  (:use :cl :asdf))

(in-package :imp.system)

(defsystem :imp
  :depends-on ("enumera")
  :serial t
  :components
  ((:file "imp")))
