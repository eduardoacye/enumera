# -*- mode: gnuplot -*-

set terminal cairolatex pdf input size 5.5in, 3in

set output 'ekleene-dist1.tex'

set key at graph 0.2,1.0 opaque

set linetype 1 linewidth 4 linecolor rgb "#000000" dt solid
set linetype 2 linewidth 7 linecolor rgb "#64000000" dt solid
set linetype 3 linewidth 10 linecolor rgb "#AF000000" dt solid
set linetype 4 linewidth 5 linecolor rgb "#000000" dt 2
set linetype 5 linewidth 8 linecolor rgb "#000000" dt 3
set linetype 6 linewidth 6 linecolor rgb "#64000000" dt 4

# set linetype 1 linewidth 4 linecolor rgb "#000000" dashtype solid
# set linetype 2 linewidth 10 linecolor rgb "#77000000" dashtype solid
# set linetype 3 linewidth 4 linecolor rgb "#000000" dashtype "-"
# set linetype 4 linewidth 4 linecolor rgb "#000000" dashtype "."
# set linetype 5 linewidth 10 linecolor rgb "#aa000000" dashtype solid

# set linetype 1 linewidth 3 linecolor rgb "#5F4690" dashtype solid
# set linetype 2 linewidth 3 linecolor rgb "#1D6996" dashtype solid
# set linetype 3 linewidth 3 linecolor rgb "#38A6A5" dashtype solid
# set linetype 4 linewidth 3 linecolor rgb "#0F8554" dashtype solid
# set linetype 5 linewidth 3 linecolor rgb "#73AF48" dashtype solid
# set linetype 6 linewidth 3 linecolor rgb "#EDAD08" dashtype solid
set linetype 7 linewidth 3 linecolor rgb "#E17C05" dashtype solid
set linetype 8 linewidth 3 linecolor rgb "#CC503E" dashtype solid
set linetype 9 linewidth 3 linecolor rgb "#94346E" dashtype solid
set linetype 10 linewidth 3 linecolor rgb "#6F4070" dashtype solid
set linetype 11 linewidth 3 linecolor rgb "#994E95" dashtype solid
set linetype 12 linewidth 3 linecolor rgb "#666666" dashtype solid

set linetype 20 linewidth 1.0 linecolor rgb "gray" dashtype (1,2)
set linetype 21 linewidth 2.0 linecolor rgb "black" dashtype solid

set grid xtics ytics linetype 20
set border linetype 21

# set title 'Posiciones máximas de componentes en $\econs{E_1}{(\econs{E_2}{(\econs{E_3}{\K(\T{null})})})}$'
set xlabel 'Posición $k$'
set ylabel 'Cantidad de listas'
set yrange [0:*]

plot 'ekleene-dist1.dat' using 1:2 title '$1$' smooth csplines lt 1, \
     'ekleene-dist1.dat' using 1:3 title '$2$' smooth csplines lt 2, \
     'ekleene-dist1.dat' using 1:4 title '$3$' smooth csplines lt 3, \
     'ekleene-dist1.dat' using 1:5 title '$4$' smooth csplines lt 4, \
     'ekleene-dist1.dat' using 1:6 title '$5$' smooth csplines lt 5, \
     'ekleene-dist1.dat' using 1:7 title '$6$' smooth csplines lt 6
