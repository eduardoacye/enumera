# -*- mode: gnuplot -*-

set terminal cairolatex pdf input size 5.5in, 2.5in

set output 'imp-dens.tex'

unset key
set tics out

set linetype 1 lw 1 lc "#000000" ps .5 pt 7

set style boxplot nooutliers fraction 1.0

set logscale y

set xlabel 'Tamaño'
set ylabel 'Posición'
set xtics ('6' 1, '7' 2, '8' 3)
set ytics nomirror
set yrange [1:1000000000000000000000000000]

plot for [i=1:3] 'imp-dens.dat' u (i):i w boxplot lt 1 fs solid 0.2