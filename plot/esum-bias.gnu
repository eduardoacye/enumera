# -*- mode: gnuplot -*-

set terminal cairolatex pdf input size 5.5in, 2in

set output 'esum-bias.tex'

set key at graph 0.2,1.0 opaque

set linetype 1 linewidth 4 linecolor rgb "#000000" dashtype solid
set linetype 2 linewidth 10 linecolor rgb "#bb000000" dashtype solid
set linetype 3 linewidth 4 linecolor rgb "#000000" dashtype "-"

set linetype 20 linewidth 1.0 linecolor rgb "gray" dashtype (1,2)
set linetype 21 linewidth 2.0 linecolor rgb "black" dashtype solid

set grid xtics ytics linetype 20
set border linetype 21

# set multiplot layout 2, 1

# set title 'Posiciones máximas de componentes en $E_1+(E_2+(E_3+\emptyset))$'
set xlabel 'Posición $k$'
set ylabel '$\max k_i$'
plot 'esum-bias.dat' using 1:2 title '$E_1$' smooth csplines lt 1, \
     'esum-bias.dat' using 1:3 title '$E_2$' smooth csplines lt 2, \
     'esum-bias.dat' using 1:4 title '$E_3$' smooth csplines lt 3

# set title 'Posiciones máximas de componentes en $\eunion(E_1, E_2, E_3)$'
# plot 'esum-bias.dat' using 1:5 title '$E_1$' smooth csplines lt 1, \
#      'esum-bias.dat' using 1:6 title '$E_2$' smooth csplines lt 2, \
#      'esum-bias.dat' using 1:7 title '$E_3$' smooth csplines lt 3