# -*- mode: gnuplot -*-

set terminal cairolatex pdf input size 5.5in, 4.0in

set output 'econs-bias.tex'

set key at graph 0.2,1.0 opaque

set linetype 1 linewidth 4 linecolor rgb "#000000" dashtype solid
set linetype 2 linewidth 10 linecolor rgb "#bb000000" dashtype solid
set linetype 3 linewidth 4 linecolor rgb "#000000" dashtype "-"
#set linetype 1 linewidth 3 linecolor rgb "#5F4690" dashtype solid
#set linetype 2 linewidth 3 linecolor rgb "#1D6996" dashtype solid
#set linetype 3 linewidth 3 linecolor rgb "#38A6A5" dashtype solid
set linetype 4 linewidth 3 linecolor rgb "#0F8554" dashtype solid
set linetype 5 linewidth 3 linecolor rgb "#73AF48" dashtype solid
set linetype 6 linewidth 3 linecolor rgb "#EDAD08" dashtype solid
set linetype 7 linewidth 3 linecolor rgb "#E17C05" dashtype solid
set linetype 8 linewidth 3 linecolor rgb "#CC503E" dashtype solid
set linetype 9 linewidth 3 linecolor rgb "#94346E" dashtype solid
set linetype 10 linewidth 3 linecolor rgb "#6F4070" dashtype solid
set linetype 11 linewidth 3 linecolor rgb "#994E95" dashtype solid
set linetype 12 linewidth 3 linecolor rgb "#666666" dashtype solid

set linetype 20 linewidth 1.0 linecolor rgb "gray" dashtype (1,2)
set linetype 21 linewidth 2.0 linecolor rgb "black" dashtype solid

set grid xtics ytics linetype 20
set border linetype 21

set multiplot layout 2, 1

# set title 'Posiciones máximas de componentes en $\econs{E_1}{(\econs{E_2}{(\econs{E_3}{\K(\T{null})})})}$'
# set xlabel 'Posición $k$'
unset xlabel                    # eliminar si se quiere repetir etiqueta en eje x
set ylabel '$\max k_i$'
plot 'econs-bias.dat' using 1:2 title '$k_1$' smooth csplines lt 1, \
     'econs-bias.dat' using 1:3 title '$k_2$' smooth csplines lt 2, \
     'econs-bias.dat' using 1:4 title '$k_3$' smooth csplines lt 3

# set title 'Posiciones máximas de componentes en $\eprod(E_1, E_2, E_3)$'
set xlabel 'Posición $k$'
plot 'econs-bias.dat' using 1:5 title '$k_1$' smooth csplines lt 1, \
     'econs-bias.dat' using 1:6 title '$k_2$' smooth csplines lt 2, \
     'econs-bias.dat' using 1:7 title '$k_3$' smooth csplines lt 3
