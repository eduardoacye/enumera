# -*- mode: gnuplot -*-

set terminal cairolatex pdf input size 5.5in, 2.5in

set output '../plot/factorial-iterative-07.tex'

# set key at graph 0.2,1.0 opaque
unset key

set linetype 2 linewidth 4 linecolor rgb "#7F3C8D" dashtype solid
set linetype 3 linewidth 4 linecolor rgb "#11A579" dashtype solid
set linetype 4 linewidth 4 linecolor rgb "#3969AC" dashtype solid
set linetype 5 linewidth 4 linecolor rgb "#F2B701" dashtype solid
set linetype 6 linewidth 4 linecolor rgb "#E73F74" dashtype solid
set linetype 7 linewidth 4 linecolor rgb "#80BA5A" dashtype solid
set linetype 8 linewidth 4 linecolor rgb "#E68310" dashtype solid
set linetype 9 linewidth 4 linecolor rgb "#008695" dashtype solid
set linetype 10 linewidth 4 linecolor rgb "#666666" dashtype solid

set linetype 20 linewidth 1.0 linecolor rgb "gray" dashtype (1,2)
set linetype 21 linewidth 2.0 linecolor rgb "black" dashtype solid

set style fill transparent solid 0.2

set grid xtics ytics linetype 20
set border linetype 21

set title 'Tiempo de ejecución $S_7(m)$'
set xlabel '$m$'
set ylabel 'T (cpu)'
set yrange [0:1000]
set autoscale xfix
set ytics nomirror
set y2label '$T_1/{T_2}$'
set y2tics 0, 1
set y2range [0:4]

plot '../plot/factorial-iterative-07.dat' using 1:5 axis x1y2 notitle smooth csplines lt 7 with filledcurves y1=0, \
     '../plot/factorial-iterative-07.dat' using 1:2 title '\small factorial' smooth csplines lt 7 dt (5,5), \
     '../plot/factorial-iterative-07.dat' using 1:3 title '\small iterative' smooth csplines lt 7