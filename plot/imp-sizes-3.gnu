# -*- mode: gnuplot -*-

set terminal cairolatex pdf input size 5.5in, 2.1in

set output 'imp-sizes-3.tex'

unset key

set linetype 1 linewidth 1 linecolor "#bb000000" dashtype solid pt 1
set linetype 3 linewidth 4 linecolor rgb "#000000" dashtype solid

# set yrange [1:24]
set xrange [1:*]
set xtics rotate
set logscale y
set xlabel 'Tamaño'
set ylabel 'Cantidad de programas'

plot 'imp-sizes-3.dat' using 1:2 with lines lt 3