# -*- mode: gnuplot -*-

set terminal cairolatex pdf input size 5.5in, 3in

set output 'imp-sizes-2.tex'

unset key

set linetype 1 linewidth 1 linecolor "#bb000000" dashtype solid pt 1
set linetype 3 linewidth 4 linecolor rgb "#000000" dashtype solid

set yrange [1:24]
set xrange [1:*]
set xtics rotate

f(x) = 2.45433417+1.03892644*log(x)

plot 'imp-sizes-2.dat' using 1:2 lt 1, \
     f(x) lt 3