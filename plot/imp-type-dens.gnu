# -*- mode: gnuplot -*-

set terminal cairolatex pdf input size 5.5in, 2.5in

set output 'imp-type-dens.tex'

unset key
set tics out

set linetype 1 lw 1 lc "#000000" ps .5 pt 7

set style boxplot nooutliers fraction 1.0

set logscale y

set xlabel 'Tipo de programa'
set ylabel 'Posición'
# set xtics ('$\T{set}$' 1, '$\T{while}$' 2, '$\T{conc}$' 3, '$\T{if}$' 4)
set ytics nomirror
set yrange [1:100000000000000000000000000000]

# plot 'imp-type-dens.dat' u 1:2 w boxplot lt 1 fs solid 0.2
plot 'imp-type-dens.dat' u (1):1:(0.5):2 w boxplot lt 1 fs solid 0.2